'use strict';

export class Player {
  ID: number;
  teamID: number;

  name: string;
  startedLeg: boolean;
  startedSet: boolean;
  Account: number;
  hasThrown: boolean;
 
  constructor() {
    //Set the defaults
    this.startedLeg = false;
    this.startedSet = false;
    this.Account = 0;
    this.hasThrown = false;
  }

}
