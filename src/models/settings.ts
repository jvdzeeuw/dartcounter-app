'use strict';

import { Team } from '../models/team';

export class Settings {
  public key?: string;
  public allowDartbot?: boolean = false;
  public dartbotMin?: number;
  public dartbotMax?: number;
  public dartbotDefault?: number;
  public cpuTitle?: string;
  
  public allowTeams?: boolean = false;
  public allowCheckoutRate?: boolean = false;
  public checkoutRate?: boolean = false;
  
  public setsMode?: boolean = false;
  public vsCPU?: boolean = false;
  public isBestOf?: string;
  public isSets?: string;
  public showStartScores?: boolean = false;
  public selectedStartScore?: any;
  public startScore?: any;
  public showSequences?: boolean = false;
  public sequence?: any;
  public onePlayerPossible?: boolean = false;
  public unsavedKey: string;  
  public unfinishedKey: string;  
  public difficulties?: boolean = false;
  public difficulty?: any;
  
  //For choosing tactics or cricket
  public showGameModes?: boolean = false;
  public gameMode?: any;

  public showAmountOfTurns?: boolean = false;
  public amountOfTurns?: number;
  
  public goalAmount?: number;
  public customScore?: number;

  public teams?: Team[];
  public amountOfPlayers?: number;
  public players?: any[];
 
  constructor() {
    //Set the defaults
  }

}
