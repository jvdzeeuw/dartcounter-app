import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, ToastController, Config, Keyboard, IonicApp, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import { TranslateService } from '@ngx-translate/core';
import { AppRate } from '@ionic-native/app-rate';

import { Home } from '../pages';
import { PreferenceService } from '../providers';

@Component({
  templateUrl: './app.html',
})
export class DartCounterApp {
  @ViewChild(Nav) public nav: Nav;
  private showedAlert: boolean = false;
  private exitAppAlert: any;
  public rootPage: any;

  constructor(
    private oneSignal: OneSignal,
    public alertCtrl: AlertController,
    public keyboard: Keyboard,
    public ionicApp: IonicApp,
    public preferences: PreferenceService,
    public appRate: AppRate,
    public platform: Platform,
    public menu: MenuController,
    public splashScreen: SplashScreen,
    public config: Config,
    public translate: TranslateService,
    public status: StatusBar,
    public toast: ToastController) {

    this.rootPage = Home;
    const vm = this;

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.status.styleDefault();
      if (this.splashScreen) {
        setTimeout(() => {
          this.splashScreen.hide();
        }, 100);
      }

      document.addEventListener('deviceready', () => {

        //OneSignal configuration
        //startInit(appId, googleProjectNumber)
        vm.oneSignal.startInit('0e0c348a-2874-4db2-b0a3-86bafc1db732', '289956640860');

        vm.oneSignal.inFocusDisplaying(vm.oneSignal.OSInFocusDisplayOption.InAppAlert);

        vm.oneSignal.handleNotificationReceived().subscribe(() => {
          // do something when notification is received
        });

        vm.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
        });

        vm.oneSignal.endInit();

        //override the whole preferences object
        vm.appRate.preferences = {
          displayAppName: 'DartCounter',
          promptAgainForEachNewVersion: true,
          inAppReview: true,
          usesUntilPrompt: 3,
          storeAppURL: {
            ios: '999533915',
            android: 'market://details?id=com.dartcounter.mobile'
          },
          callbacks: {
            handleNegativeFeedback: function () {
              window.open('mailto:info@dartcounter.net', '_system');
            },
          }
        };

      }, false);

    });

    // Confirm exit
    this.platform.registerBackButtonAction(() => {
      if (this.keyboard.isOpen()) { // Handles the keyboard if open
        return this.keyboard.close();
      }

      const activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();

      //activePortal is the active overlay like a modal,toast,etc
      if (activePortal) {
        return activePortal.dismiss();
      }
      else if (this.menu.isOpen()) { // Close menu if open
        return this.menu.close();
      }


      if (this.nav.length() == 1) {
        if (!this.showedAlert) {
          this.confirmExitApp();
        } else {
          this.showedAlert = false;
          this.exitAppAlert.dismiss();
        }
      }
      else {
        this.nav.pop();
      }
    });
  }
  //End of constructor 

  confirmExitApp() {
    this.showedAlert = true;

    const lang = this.preferences.Preferences.lang;
    let question = 'Weet je het zeker?';
    let title = 'Afsluiten';
    let confirm = 'Ja';
    let cancel = 'Annuleren';

    if (lang == 'en') {
      question = 'Are you sure?';
      title = 'Quit';
      confirm = 'Yes';
      cancel = 'Cancel';
    } else if (lang == 'de') {
      question = 'Bist du sicher?';
      title = 'Verlassen';
      confirm = 'Ja';
      cancel = 'Annullieren';
    }

    this.exitAppAlert = this.alertCtrl.create({
      title: title,
      message: question,
      buttons: [
        {
          text: cancel,
          handler: () => {
            this.showedAlert = false;
            return;
          }
        },
        {
          text: confirm,
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.exitAppAlert.present();
  }
}
