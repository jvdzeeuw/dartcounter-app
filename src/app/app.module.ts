import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativeAudio } from '@ionic-native/native-audio';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { DatePicker } from '@ionic-native/date-picker';
import { Facebook } from '@ionic-native/facebook';
import { FirebaseAnalytics  } from '@ionic-native/firebase-analytics';
import { OneSignal } from '@ionic-native/onesignal';
import { Network } from '@ionic-native/network';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppRate } from '@ionic-native/app-rate';

import { DartCounterApp } from './app.component';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicImageViewerModule } from 'ionic-img-viewer';

// Import Bugsnag and the Angular integration
import bugsnag from '@bugsnag/js';
import { BugsnagErrorHandler } from '@bugsnag/plugin-angular';

//Import Components
import { 
  GameHeaderComponent, AppHeaderComponent, AppMenuComponent, IngameMenuComponent, AppHeaderPopoverComponent, AppHeaderPopoverController, IonNumericKeyboard,
  ProfileImageComponent, LoginForm, RegistrationForm, FacebookLogin
} from '../components';

//Import pages & dialogs
import { 
  Login, Games, Register, ProfileView, AccountView, SettingsView, AppSettingsView,
  MatchPage, DoublesTrainingPage, SinglesTrainingPage, BobsTraining,
  ScoreTrainingPage, TacticsPage, Friends, Statistics, Achievements,
  ItemList, ItemDetail, RemoteControl, MatchDetail, MatchList, TacticsList, 
  SinglesDetail, DoublesDetail, BobsDetail, ScoresDetail, ForgotPassword, Home, Dashboard,
  AccountSettingsView, TacticsDetail

} from '../pages';
import { 
  ThrowoutDialog, MatchStatisticsDialog, LinkPeopleDialog, AgreementDialog, PolicyDialog,
  TutorialDialog, SimpleStatisticsDialog, DartboardDialog, CompleteAccountDialog, PreferencesDialog,
  LoginRequiredDialog, UpgradeSubscriptionDialog, SaveOrDeleteDialog, SwipeForMenu, AcceptTermsDialog, ActionCodeDialog, UltimateSubscriptionDialog
} from '../dialogs';

//Import Providers
import { 
  Auth, MobileApiService, SmartAudioProvider, FileTransferService, PreferenceService, StorageProvider, PurchaseService,
  SocialMediaService
} from '../providers';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LinkSoundDialog } from '../dialogs/link-sound/link-sound';
import { LoginOrCreateDialog } from '../dialogs/login-or-create/login-or-create';
import { LinkAccountWarningDialog } from '../dialogs/link-account-warning/link-account-warning';
import { X01Average } from '../pages/statistics/alltime/x01average/x01average';
import { CheckoutRate } from '../pages/statistics/alltime/checkoutRate/checkoutRate';
import { Checkouts } from '../pages/statistics/alltime/checkouts/checkouts';
import { FirstNines } from '../pages/statistics/alltime/firstNine/firstNine';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

// configure Bugsnag asap
const bugsnagClient = bugsnag('b0a56b45a399315172000854192f2be1');
//bugsnagClient.notify(new Error('Test error'));

// create a factory which will return the bugsnag error handler
export function errorHandlerFactory() {
  return new BugsnagErrorHandler(bugsnagClient);
}

@NgModule({
  declarations: [
    DartCounterApp,
    IonNumericKeyboard,
    Games,
    Dashboard,
    Home,
    Login,
    Register,
    ForgotPassword,
    Friends,
    Statistics,
    RemoteControl,
    MatchPage,
    MatchList,
    MatchDetail,
    TacticsList,
    TacticsDetail,
    Achievements,
    TacticsPage,
    SettingsView,
    ItemList,
    ItemDetail,
    AppSettingsView,
    ProfileView,
    AccountView,
    AccountSettingsView,
    SinglesTrainingPage,
    SinglesDetail,
    DoublesTrainingPage,
    DoublesDetail,
    BobsTraining,
    BobsDetail,
    ScoreTrainingPage,
    ScoresDetail,
    MatchStatisticsDialog,
    SimpleStatisticsDialog,
    AgreementDialog, 
    PolicyDialog,
    LinkPeopleDialog,
    LinkSoundDialog,
    ThrowoutDialog,
    DartboardDialog,
    CompleteAccountDialog,
    PreferencesDialog,
    UpgradeSubscriptionDialog,
    LoginRequiredDialog,
    LoginOrCreateDialog,
    LinkAccountWarningDialog,
    ActionCodeDialog,
    UltimateSubscriptionDialog,
    TutorialDialog,
    SaveOrDeleteDialog,
    SwipeForMenu,
    AcceptTermsDialog,
    GameHeaderComponent,
    AppHeaderComponent,
    AppMenuComponent,
    IngameMenuComponent,
    AppHeaderPopoverComponent,
    ProfileImageComponent,
    LoginForm,
    FacebookLogin,
    RegistrationForm,
    X01Average,
    CheckoutRate,
    Checkouts,
    FirstNines
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicImageViewerModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(DartCounterApp, { 
      pageTransition: 'md-transition', 
      scrollAssist: false, 
      autoFocusAssist: false }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    DartCounterApp,
    GameHeaderComponent,
    AppHeaderComponent,
    AppHeaderPopoverComponent,
    MatchStatisticsDialog,
    SimpleStatisticsDialog,
    LinkPeopleDialog,
    LinkSoundDialog,
    ThrowoutDialog,
    AgreementDialog, 
    DartboardDialog,
    CompleteAccountDialog,
    PreferencesDialog,
    UpgradeSubscriptionDialog,
    LoginRequiredDialog,
    LoginOrCreateDialog,
    LinkAccountWarningDialog,
    UltimateSubscriptionDialog,
    ActionCodeDialog,
    PolicyDialog,
    TutorialDialog,
    SaveOrDeleteDialog,
    SwipeForMenu,
    AcceptTermsDialog,
    SinglesTrainingPage,
    SinglesDetail,
    DoublesTrainingPage,
    DoublesDetail,
    BobsTraining,
    BobsDetail,
    ScoreTrainingPage,
    ScoresDetail,
    ItemList,
    ItemDetail,
    MatchPage,
    MatchList,
    MatchDetail,
    TacticsList,
    TacticsDetail,
    TacticsPage,
    Achievements,
    Games,
    Dashboard,
    Home,
    Login,
    Friends,
    Statistics,
    RemoteControl,
    SettingsView,
    AppSettingsView,
    ProfileView,
    AccountView,
    AccountSettingsView,
    Register,
    ForgotPassword,
    X01Average,
    CheckoutRate,
    Checkouts,
    FirstNines
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    AppMenuComponent,
    IngameMenuComponent,
    NativeAudio,
    ThemeableBrowser,
    Facebook,
    OneSignal,
    FirebaseAnalytics,
    Network,
    SocialSharing,
    InAppPurchase2,
    AppHeaderPopoverController,
    { provide: ErrorHandler, useFactory: errorHandlerFactory },
    Auth, 
    SmartAudioProvider,
    PreferenceService,
    FileTransfer,
    File,
    FileTransferService,
    PurchaseService,
    SocialMediaService,
    MobileApiService,
    StorageProvider,
    ScreenOrientation,
    AppRate,
  ],
})

export class AppModule { }
