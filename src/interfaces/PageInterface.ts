export interface PageInterface {
  title: string;
  component: any;
  icon: string;
  index?: number;
  description?: string;        
  badgeCount?: number;
}
