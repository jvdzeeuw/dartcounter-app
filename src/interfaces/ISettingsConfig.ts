export interface ISettingsConfig {
  title: string;
  gameComponent: any;
  startScores: any[];
}
