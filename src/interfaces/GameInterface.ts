import { Player } from '../models/player';
import { Settings } from '../models/settings';

export interface GameInterface {
  settings: Settings;
  players: Player[];

  title: string;
  component: any;
  gameplay: any;
  icon: string;
  description?: string;
  unfinishedAmount?: number;
}
