import { Component, Input } from '@angular/core';

import { ModalController, MenuController, NavController, ToastController } from 'ionic-angular';

import { FacebookLoginResponse } from '@ionic-native/facebook';

import { AppMenuComponent } from '../../components';
import { Auth, MobileApiService, SocialMediaService } from '../../providers';
import { CompleteAccountDialog } from '../../dialogs';
import { Dashboard } from '../../pages';


@Component({
  selector: 'facebook-login',
  templateUrl: 'facebook-login.html'
})
export class FacebookLogin {
  @Input() pop: boolean = false;

  constructor(
    public auth: Auth,
    public modal: ModalController,
    public navCtrl: NavController,
    public toast: ToastController,
    public social: SocialMediaService,
    public appMenu: AppMenuComponent,
    public mobileAPI: MobileApiService,
    public menu: MenuController) {

  }

  ionViewWillEnter() {
  }

  FBLogin() {
    const vm = this;
    vm.social.FacebookLogin().then((res: FacebookLoginResponse) => {
      vm.social.GetAccount(res.authResponse.userID)
        .then((fbAccount) => {
          vm.mobileAPI.PostData({
            account: {
              Email: fbAccount.email,
            }
          }, 'backend', 'checkEmail').subscribe(res => {
            //profile with emailadress found without FB link
            if (res.ID) {
              vm.LinkToExistingAccount(fbAccount, res.ID);
            }
            else {
              vm.CreateFBAccount(fbAccount);
            }
          });
        }).catch(e => { });
    }).catch(e => {
      console.log('Error logging into Facebook', e);
    });
  }

  LinkToExistingAccount(fbAccount, AccountID) {
    const vm = this;

    vm.mobileAPI.PostData({
      account: {
        ID: AccountID,
        FacebookID: fbAccount.id,
        Birthday: null,
        Locale: fbAccount.locale,
        Email: fbAccount.email,
        Gender: fbAccount.gender,
      }
    }, 'backend', 'linkToExisting').subscribe(profile => {
      vm.auth.saveAuthentication(profile);

      //Account successfully linked to existing user: profile.Username
      vm.mobileAPI.translate.get('account linked to', { value: profile.Username }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });

      vm.appMenu.profile = profile;
      if (vm.pop) {
        vm.navCtrl.pop();
      }
      else {
        vm.navCtrl.setRoot(Dashboard);
      }
    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  CreateFBAccount(fbAccount) {
    const vm = this;

    vm.mobileAPI.PostData({
      account: {
        FirstName: fbAccount.first_name,
        LastName: fbAccount.last_name,
        FacebookID: fbAccount.id,
        Email: fbAccount.email,
        Birthday: null,
        Locale: fbAccount.locale,
        Gender: fbAccount.gender,
      }
    }, 'backend', 'fbregister').subscribe(profile => {
      vm.auth.saveAuthentication(profile);
      vm.appMenu.profile = profile;
      
      if (vm.pop) {
        vm.navCtrl.pop();
      }
      else {
        vm.navCtrl.setRoot(Dashboard);
      }

      if (!profile.Username) {
        const completeAccountModal = vm.modal.create(CompleteAccountDialog, { fbAccount: fbAccount });
        completeAccountModal.present();
      }

    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }
}
