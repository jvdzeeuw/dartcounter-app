import { Component, Input, Output, EventEmitter, SimpleChange} from '@angular/core';
import { Content } from 'ionic-angular';
import { PreferenceService } from '../../providers';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'ion-numeric-keyboard',
  templateUrl: 'ion-numeric-keyboard.html',
  host: {
    '(document: click)': 'closeOnOutsideClickEvent($event)',
    '(click)': 'trackHostClickEvent($event)',
  }
})
export class IonNumericKeyboard {
  // inputs
  @Input() options: IonNumericKeyboardOptions;
  @Input() visible: boolean = false;
  // outputs
  @Output() inkClick = new EventEmitter();
  @Output() inkClose = new EventEmitter();
  // internals
  private _hostClickEvent: any = null; // reference to the most recent host click event
  private _isFirstClickEventIgnored = false;
  constructor(private _sanitizer: DomSanitizer,
    public translate: TranslateService,
    public preferenseService: PreferenceService
  ) { }

  ngOnInit() {
    if (this.options === undefined || this.options === null) {
      console.error('[IonNumericKeyboard] options are not defined.');
    }
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    // watch visibility changes
    const c: SimpleChange = changes['visible'];
    if (this.options.contentComponent && c && !c.isFirstChange()) {
      if (!this.visible && this.visible === c.currentValue && c.previousValue !== true) {
        return; //don't fire events or resize content if keyboard already hidden
      }
      this.postToggleVisibility();
    }
  }

  trustContent(content: string): SafeHtml {
    return this._sanitizer.bypassSecurityTrustHtml(content);
  }

  onClick(key: any, source: string) {
    this.inkClick.emit({
      key: key,
      source: source
    });
  }

  postToggleVisibility() {
    this.options.contentComponent.resize();
    if (this.options.animated) {
      // resizing content several times to move up/down the content as the same time as the keyboard
      setTimeout(() => this.options.contentComponent.resize(), 100);
      setTimeout(() => this.options.contentComponent.resize(), 175);
      setTimeout(() => this.options.contentComponent.resize(), 250);
    }
    this._isFirstClickEventIgnored = false;
    if (!this.visible) {
      this.inkClose.emit({});
    }
  }

  closeOnOutsideClickEvent(globalClickEvent) {
    if (!globalClickEvent || !globalClickEvent.target) {
      return; // do not hide keyboard if there is no click target, no point going on
    }
    if (this.options && !this.options.hideOnOutsideClick) {
      return; // do not hide keyboard if option is disabled
    }
    if (!this.visible) {
      return; // do not hide keyboard if keyboard already hidden
    }
    if (this._hostClickEvent === globalClickEvent) {
      return; // do not hide keyboard if click event is inside the keyboard
    }
    if (!this._isFirstClickEventIgnored) {
      this._isFirstClickEventIgnored = true;
      return; // do not hide keyboard for the first outside click event
    }
    // loop through the available elements, looking for classes in the class list that might match
    for (let element = globalClickEvent.target; element; element = element.parentNode) {
      let classNames = element.className;
      // Unwrap SVGAnimatedString
      if (classNames && classNames.baseVal !== undefined) {
        classNames = classNames.baseVal;
      }
      // check for id's or classes, but only if they exist in the first place
      if (classNames && classNames.indexOf('ion-numeric-keyboard-source') > -1) {
        // now let's exit out as it is an element that has been defined as being ignored for clicking outside
        return; // do not hide keyboard if outside click event contains .ion-numeric-keyboard-source class
      }
    }
    // click outside detected
    // hiding the keyboard
    this.visible = false;
    this.postToggleVisibility();
  }

  trackHostClickEvent(newHostClickEvent) {
    this._hostClickEvent = newHostClickEvent;
  }
}

export interface IonNumericKeyboardOptions {
  hideOnOutsideClick?: boolean; // do not hide by default
  onlyThreeOptions?: boolean;
  hideBottom?: boolean;
  showExtraButton?: boolean;
  ExtraButtonText?: string;

  animated?: boolean; // do not animate by default
  leftControlKey?: {
    type: string,
    value: string
  }; // no left control key by default
  rightControlKey?: {
    type: string,
    value: string
  }; // no right control key by default
  topBarKeys?: Array<{
    keySource: string,
    keyContent: string,
    keyClass: string
  }>;
  contentComponent: Content;
} 
