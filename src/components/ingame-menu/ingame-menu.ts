import { Component, Input } from '@angular/core';
import { App, MenuController, ModalController } from 'ionic-angular';
import { Auth, PreferenceService, StorageProvider } from '../../providers';
import { Platform } from 'ionic-angular/platform/platform';
import { UpgradeSubscriptionDialog } from '../../dialogs';


@Component({
  selector: 'ingame-menu',
  templateUrl: 'ingame-menu.html'
})
export class IngameMenuComponent {
  @Input() content: any;

  public options: {
    showSound?: boolean,
    showKeySize?: boolean,
    showTacticsSize?: boolean,
    showAdvancedSound?: boolean,
  } = {};

  constructor(
    public app: App,
    public platform: Platform,
    public storage: StorageProvider,
    public modal: ModalController,
    public auth: Auth,
    public preferenceService: PreferenceService,
    public menu: MenuController) {
    this.options = {
      showKeySize: true,
      showSound: true,
      showAdvancedSound: true,
    };
  }

  SetOptions(options) {
    this.options = options;
  }

  OpenSubscriptionDialog() {
    const subscriptionDialog = this.modal.create(UpgradeSubscriptionDialog);
    subscriptionDialog.present();
  }

}
