import { Component } from '@angular/core';
import { App, Events, MenuController, ToastController, LoadingController, NavController, ModalController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { Dashboard } from '../../pages';
import { AppMenuComponent } from '../';
import { Auth, MobileApiService } from '../../providers';
import { CompleteAccountDialog } from '../../dialogs';


@Component({
  selector: 'registration-form',
  templateUrl: 'registration-form.html'
})
export class RegistrationForm {
  public FirstName;
  public LastName;
  public Username;
  public Password;
  public Email;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public toast: ToastController,
    public appMenu: AppMenuComponent,
    public modal: ModalController,
    public translate: TranslateService,
    public events: Events,
    public loading: LoadingController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public menu: MenuController) {

  }

  ionViewWillEnter() {
    const vm = this;

  }

  TryRegistration() {
    const vm = this;

    const mailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const userRegex = new RegExp('^[a-zA-Z0-9\-]+$');
    if (!mailRegex.test(vm.Email)) {
      vm.mobileAPI.translate.get('email_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else if (vm.Username.length < 5 || vm.Username.length > 20 || !userRegex.test(vm.Username)) {
      vm.mobileAPI.translate.get('username_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else if (vm.Password.length < 6) {
      vm.mobileAPI.translate.get('pass_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else {

      const loader = this.loading.create();
      loader.present();
      vm.mobileAPI.PostData({
        account: {
          FirstName: vm.FirstName,
          LastName: vm.LastName,
          Username: vm.Username,
          Password: vm.Password,
          Email: vm.Email,
        }
      }, 'general', 'register').subscribe(profile => {
        loader.dismiss();
        this.auth.saveAuthentication(profile);
        this.appMenu.profile = profile;
        vm.navCtrl.setRoot(Dashboard);

      }, (err) => {
        loader.dismiss();
        this.toast.create({
          cssClass: 'error',
          message: err,
          duration: 3000
        }).present();
      });
    }
  }

  CreateAccount(fbAccount) {
    const vm = this;

    const loader = this.loading.create();
    loader.present();
    vm.mobileAPI.PostData({
      account: {
        FirstName: fbAccount.first_name,
        LastName: fbAccount.last_name,
        FacebookID: fbAccount.id,
        Email: fbAccount.email,
        Birthday: null,
        Locale: fbAccount.locale,
        Gender: fbAccount.gender,
      }
    }, 'backend', 'fbregister').subscribe(profile => {
      loader.dismiss();
      vm.auth.saveAuthentication(profile);
      vm.appMenu.profile = profile;
      vm.navCtrl.setRoot(Dashboard);

      if (!profile.Username) {
        const completeAccountModal = vm.modal.create(CompleteAccountDialog, { fbAccount: fbAccount });
        completeAccountModal.present();
      }

    }, (err) => {
      loader.dismiss();
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }
}
