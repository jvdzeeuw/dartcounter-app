import { Component, Input } from '@angular/core';

@Component({
  selector: 'game-header',
  templateUrl: 'game-header.html'
})
export class GameHeaderComponent {
   // inputs
  @Input() color: string = 'dcOrange';
  @Input() title: string = null;

  constructor() {
    console.log(this);
  }


}
