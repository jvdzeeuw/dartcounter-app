import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
  ],
  imports: [
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
  ],
  exports: [
  ],
  entryComponents: [],
  providers: [],
})

export class ComponentsModule { }
