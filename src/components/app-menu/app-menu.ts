import { Component, Input } from '@angular/core';
import { App, Events, MenuController, ModalController } from 'ionic-angular';

import { Games, Home, Dashboard, Statistics, Friends, ProfileView, AccountView, AccountSettingsView, AppSettingsView } from '../../pages';
import { PageInterface } from '../../interfaces/PageInterface';
import { Auth, MobileApiService, PurchaseService, StorageProvider, PreferenceService } from '../../providers';
import { LoginRequiredDialog, UpgradeSubscriptionDialog, UltimateSubscriptionDialog } from '../../dialogs';
import { Platform } from 'ionic-angular/platform/platform';


@Component({
  selector: 'app-menu',
  templateUrl: 'app-menu.html'
})
export class AppMenuComponent {
  @Input() content: any;
  profile: any = {};
  pages: PageInterface[];
  unfinisheds: number;

  constructor(
    public app: App,
    public events: Events,
    public platform: Platform,
    public storage: StorageProvider,
    public purchases: PurchaseService,
    public modal: ModalController,
    public auth: Auth,
    public preferenceService: PreferenceService,
    public mobileAPI: MobileApiService,
    public menu: MenuController) {

    if (!this.auth.guest) {
      //Check authentication
      this.auth.checkAuthentication().then((res) => {
        this.profile = res;
      }, (err) => {
      });
    }

    this.pages = [
      <PageInterface>{
        title: 'search friends',
        component: Friends,
        icon: 'people',
      },
      <PageInterface>{
        title: 'statistics',
        component: Statistics,
        icon: 'stats',
      },
      // <PageInterface>{
      //   title: 'remote control',
      //   component: RemoteControl,
      //   icon: 'grid'
      // }
    ];
  }

  CheckUnfinisheds() {
    const vm = this;

    vm.unfinisheds = 0;
    for (const item in vm.storage.unfinisheds) {
      vm.unfinisheds += vm.storage.unfinisheds[item].length;
    }
    return vm.unfinisheds;
  }

  OpenPage(page: PageInterface) {
    const vm = this;
    if (vm.auth.guest) {
      const loginRequiredDialog = vm.modal.create(LoginRequiredDialog);
      loginRequiredDialog.present();
    }
    else {
      this.content.setRoot(Dashboard);
      this.content.push(page.component);
      this.menu.close();
    }
  }

  OpenSubscriptionDialog() {
    if (this.auth.userData.isPro == '1') {
      const subscriptionDialog = this.modal.create(UltimateSubscriptionDialog);
      subscriptionDialog.present();
    }
    else {
      const subscriptionDialog = this.modal.create(UpgradeSubscriptionDialog);
      subscriptionDialog.present();
    }
  }

  OpenProfile() {
    const vm = this;

    vm.profile.Self = true;
    this.app.getActiveNav().push(ProfileView, {
      profile: vm.auth.userData
    });
    this.menu.close();
  }

  OpenAccount() {
    const vm = this;

    this.app.getActiveNav().push(AccountView, {
      profile: vm.auth.userData
    });
    this.menu.close();
  }

  AccountSettings() {
    const vm = this;

    this.app.getActiveNav().push(AccountSettingsView, {
      account: vm.auth.userData
    });
    this.menu.close();
  }

  ShowHome() {
    this.menu.close();
    this.app.getActiveNav().setRoot(Dashboard);
  }

  NewGame() {
    this.menu.close();
    this.app.getActiveNav().push(Games);
  }

  OpenSettings() {
    this.app.getActiveNav().push(AppSettingsView, {});
    this.menu.close();
  }

  Logout() {
    this.auth.logout();
    this.menu.close();
    this.app.getActiveNav().setRoot(Home);
  }
}
