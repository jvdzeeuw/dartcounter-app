import { Component, Injectable } from '@angular/core';
import { ViewController, NavController, ActionSheetController, PopoverController } from 'ionic-angular';

//import { AboutPage } from '../../pages/about/about';
import { SocialSharing } from '@ionic-native/social-sharing';


@Component({
  selector: 'app-header-popover',
  template: `
    <ion-list>
      <button ion-item (click)="about()">About</button>
      <button ion-item (click)="share()">Share</button>
    </ion-list>
  `
})
export class AppHeaderPopoverComponent {
  constructor(
    private socialSharing: SocialSharing,
    public view: ViewController,
    public nav: NavController,
    public sheet: ActionSheetController,
    public popover: PopoverController,
  ) { }

  close() {
    return this.view.dismiss();
  }

  about() {
    // this.nav.push(AboutPage);
  }

  share() {
    const text = 'Firetask - Ionic 2 & Firebase 3 Full Application';
    const url = 'https://firetask.io';
    const actionSheet = this.sheet.create({
      title: 'Spread the World',
      buttons: [
        {
          text: 'Whats App',
          icon: 'whatsapp',
          role: 'whatsapp',
          handler: () => {
            this.socialSharing.shareViaWhatsApp(text, null /*Image*/, url)
              .then(() => {
                // alert("Success");
              },
              () => {
                // alert("failed")
              });
          }
        }, {
          text: 'Facebook',
          icon: 'facebook',
          role: 'facebook',
          handler: () => {
            this.socialSharing.shareViaFacebook(text, null /*Image*/, url)
              .then(() => {
                // alert("Success");
              },
              () => {
                // alert("failed face");
              });
          }
        },
        {
          text: 'Twitter',
          icon: 'twitter',
          role: 'twitter',
          handler: () => {
            this.socialSharing.shareViaTwitter(text, null /*Image*/, url)
              .then(() => {
                // alert("Success");
              },
              () => {
                // alert("failed")
              });
          }
        }, {
          text: 'Others...',
          icon: 'share',
          role: 'others',
          handler: () => {
            this.socialSharing.share(text, null/*Subject*/, null/*File*/, url)
              .then(() => {
                // alert("Success");
              },
              () => {
                // alert("failed")
              });
          }
        }
      ]
    });
    this.view.dismiss().then(() => setTimeout(() => actionSheet.present()));
  }
}

@Injectable()
export class AppHeaderPopoverController {

  constructor(public popover: PopoverController) {
  }

  present(event) {
    const popover = this.popover.create(AppHeaderPopoverComponent);
    popover.present({ ev: event });
    event.preventDefault();
  }
}
