import { Component, Input } from '@angular/core';


@Component({
  selector: 'profile-image',
  templateUrl: 'profile-image.html'
})
export class ProfileImageComponent {
  @Input() url: any;
  imageUrl: any;

  constructor() { }

  ngOnInit() {
    this.SetImageUrl(this.url);
  }

  ngOnChanges(changes: {[propertyName: string]: any}) {
    if (changes['url']) { 
      this.SetImageUrl(this.url);
    }
  }

  SetImageUrl(url){
    if (url){
      this.imageUrl = 'https://legacyapi.dartcounter.net/images/profile_images/' + url;
    }
    else{
      this.imageUrl = 'assets/images/user-default.jpg';
    }
  }

}
