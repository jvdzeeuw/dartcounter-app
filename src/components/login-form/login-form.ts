import { Component } from '@angular/core';
import { MenuController, ToastController, NavController } from 'ionic-angular';

import { Dashboard } from '../../pages';
import { Auth, MobileApiService } from '../../providers';
import { AppMenuComponent } from '../../components';


@Component({
  selector: 'login-form',
  templateUrl: 'login-form.html'
})
export class LoginForm {
  Username: string;
  Password: string;

  constructor(
    public auth: Auth,
    public appMenu: AppMenuComponent,
    public toast: ToastController,
    public navCtrl: NavController,
    public mobileAPI: MobileApiService,
    public menu: MenuController) {

  }

  ionViewWillEnter(){
    const vm = this;
    
  }

  TryLogin() {
    const vm = this;
    vm.mobileAPI.showLoader();

    const credentials = {
      Username: this.Username,
      Password: this.Password
    };

    return this.mobileAPI.PostData({ account: credentials }, 'backend', 'login').subscribe(profile => {
      this.auth.saveAuthentication(profile);
      this.appMenu.profile = profile;
      vm.mobileAPI.hideLoader();
      vm.mobileAPI.GetNotifications(this.auth.userData);
      this.navCtrl.setRoot(Dashboard);
    }, (err) => {
      vm.mobileAPI.hideLoader();
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });

  }
}
