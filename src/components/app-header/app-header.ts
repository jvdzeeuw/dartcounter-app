import { Component, Input } from '@angular/core';

import  { AppHeaderPopoverController } from '../app-header-popover/app-header-popover';

@Component({
  selector: 'app-header',
  templateUrl: 'app-header.html'
})
export class AppHeaderComponent {
  @Input() title: any;

  constructor(public appHeaderPopover: AppHeaderPopoverController) {}
    presentPopover(event) {
    this.appHeaderPopover.present(event);
  }
}
