//Defines which games we have
export type GameModes = 
'Match' | 
'Tactics' | 
'SinglesTraining' | 
'DoublesTraining' | 
'ScoreTraining' | 
'Bobs27';
