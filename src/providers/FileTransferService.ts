import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Injectable } from '@angular/core';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FileTransferService {
  public fileTransfer: FileTransferObject;

  constructor(
    private transfer: FileTransfer,
    private file: File
    ) {
      this.fileTransfer = this.transfer.create();
  }

  public download(sound: string): Promise<any> {
    const ft: FileTransferObject = this.transfer.create();
    const fn = this.file.dataDirectory + sound + '.mp3';
    return ft.download('https://legacyapi.dartcounter.net/sounds/names/' + sound + '.mp3', fn );
  }
}
