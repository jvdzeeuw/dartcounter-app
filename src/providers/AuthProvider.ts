import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MenuController } from 'ionic-angular';

// import { MobileApiService } from './';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Auth {
  public userData: any = null;
  public guest: boolean = false;
  public notifications: number = 0;
  public loader: any;

  constructor(
    public http: Http,
    public menu: MenuController,
    // public mobileAPI: MobileApiService,
    public storage: Storage) {

  }

  checkAuthentication() {
    return new Promise((resolve, reject) => {

      //Load token if exists
      this.storage.get('userData').then((value) => {

        if (value == null) {
          reject('UserData not found');
        }
        else {
          this.userData = value;
          resolve(value);
        }
      });
    });
  }

  createAccount(details) {
    return new Promise((resolve, reject) => {

    });
  }

  saveAuthentication(profile) {
    this.userData = profile;
    this.guest = false;    
    this.storage.set('userData', this.userData);
  }

  logout() {
    this.guest = false;
    this.userData = null;
    this.storage.remove('matchSettings');
    this.storage.remove('tacticsSettings');
    this.storage.remove('bobsSettings');
    this.storage.remove('singlesSettings');
    this.storage.remove('doublesSettings');
    this.storage.remove('scoresSettings');
    this.storage.remove('userData');
  }
}
