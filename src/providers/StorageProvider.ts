import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';

import 'rxjs/add/operator/map';
import { Http } from '@angular/http';


@Injectable()
export class StorageProvider {
  public unfinisheds: any[] = [];
  public unfinishedAmount: number = 0;

  constructor(
    public http: Http,
    public storage: Storage,
    public loadingCtrl: LoadingController) {

  }

  GetItemFromStorage(key): any {
    const vm = this;
    return new Promise((resolve, reject) => {

      //Load token if exists
      this.storage.get(key).then((value) => {
        if (value == null) {
          if (key.indexOf('unfinished') >= 0) {
            vm.unfinisheds[key] = [];
          }
          reject();
        }
        else if (key.indexOf('unfinished') >= 0) {
          vm.unfinisheds[key] = value;
        }
        resolve(value);
      });
    });
  }

  PutItemInStorage(key, storageItem) {
    this.storage.set(key, storageItem);
  }

  PutItemInStorageByID(key, storageItem, id) {
    const vm = this;

    vm.GetItemFromStorage(key).then(
      (items: any[]) => {
        const item = items.find(item => item.Game.ID == id);
        if (item) {
          items.splice(items.indexOf(item), 1);
        }
        items.push(storageItem);

        vm.PutItemInStorage(key, items);

        if (key.indexOf('unfinished')) {
          this.unfinisheds[key] = items;
        }
      }, () => {
        vm.PutItemInStorage(key, [storageItem]);
      });

    this.storage.set(key, storageItem);
  }

  RemoveItemFromStorage(key) {
    this.storage.remove(key);

    if (key.indexOf('unfinished')) {
      this.unfinisheds[key] = [];
    }
  }

  RemoveItemFromStorageListByID(key, id) {
    const vm = this;
    vm.GetItemFromStorage(key).then(
      (items: any[]) => {
        const item = items.find(item => item.Game.ID == id);
        if (item) {
          items.splice(items.indexOf(item), 1);
          vm.PutItemInStorage(key, items);

          if (key.indexOf('unfinished')) {
            this.unfinisheds[key] = items;
          }
        }
      }, () => { });
  }

}
