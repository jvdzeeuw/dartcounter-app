import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AppRate } from '@ionic-native/app-rate';

import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';
import { Http } from '@angular/http';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PreferenceService {
  public Preferences: any;

  constructor(
    public translate: TranslateService,
    public appRate: AppRate,
    public http: Http,
    public storage: Storage) {

    const vm = this;
    this.translate = translate;
    this.translate.setDefaultLang('en');

    vm.CheckPreferences().then((prefs: any) => {
      this.translate.use(prefs.lang);
      vm.Preferences = prefs;
      if (vm.Preferences.callYouRequire == null){
        vm.Preferences.callYouRequire = true;
        vm.Preferences.callDartbot = true;
        vm.UpdatePreferences();
      }
    }, (err) => {
      vm.SetDefaultPreferences();
    });
  }

  CheckPreferences() {
    return new Promise((resolve, reject) => {

      //Load token if exists
      this.storage.get('dcPreferences').then((value) => {

        if (value == null) {
          reject();
        }

        resolve(value);
      });
    });
  }

  SetDefaultPreferences() {
    const defaultPreferences = {
      allowCaller: true,
      callYouRequire: true,
      callDartbot: true,
      lang: 'en',
      keySize: 45
    };

    this.translate.use(defaultPreferences.lang);
    this.Preferences = defaultPreferences;
    this.storage.set('dcPreferences', defaultPreferences);
  }

  public UpdatePreferences() {
    this.storage.set('dcPreferences', this.Preferences);
  }

  LanguageChanged() {
    this.translate.use(this.Preferences.lang);

    this.UpdatePreferences();
  }

}
