import { Injectable } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SocialMediaService {
  public products: any = [];
  public proSubscription: any = null;

  constructor(
    public http: Http,
    private fb: Facebook,
    public storage: Storage) {

    const vm = this;

    //this.fb.browserInit(224780451025678);
  }

  FacebookLogin(): Promise<FacebookLoginResponse> {
    const vm = this;
    return this.fb.login(['public_profile', 'user_friends', 'email']);
  }

  GetAccount(userID) {
    return this.fb.api(userID + '/?fields=id,email,gender,locale,first_name,last_name', ['public_profile']);
  }

  GetFriends(userID){
    this.fb.api(userID + '/friends', ['user_friends'])
    .then((res) => {
      console.log('Result: ', res);
    }).catch(e =>
      console.error('Failed: ', e)
    );
  }

  GetProfilePicture(userID){
    this.fb.api(userID + '/picture', ['public_profile'])
    .then((res) => {
      console.log('Result: ', res);
    }).catch(e =>
      console.error('Failed: ', e)
    );
  }
}
