import { Injectable } from '@angular/core';
import { ToastController, Platform } from 'ionic-angular';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { Storage } from '@ionic/storage';
import { MobileApiService, Auth } from './';
import { Http } from '@angular/http';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PurchaseService {
  public products: any = [];
  public proSubscription: any = null;

  constructor(
    public http: Http,
    public toast: ToastController,
    public auth: Auth,
    public platform: Platform,
    public mobileAPI: MobileApiService,
    public store: InAppPurchase2,
    public storage: Storage) {

    const vm = this;

    this.platform.ready().then(() => {

      document.addEventListener('deviceready', () => {
        //logging level
        store.verbosity = store.INFO;

        store.register({
          id: 'annual_pro', // id without package name!
          alias: 'DartCounter PRO',
          type: store.PAID_SUBSCRIPTION
        });

        store.when('annual_pro').verified(function (p) {
          //globalService.toast('success', "Verified!", 3000);
          p.finish();
        });

        store.when('annual_pro').unverified(function (p) {
          vm.toast.create({
            cssClass: 'error',
            message: p,
            duration: 3000
          }).present();
        });

        store.when('annual_pro').approved(function (p) {
          // synchronous
          //app.unlockFeature();
          p.finish();
        });

        store.when('annual_pro').updated(function (p) {
          //updated to owned state
          vm.proSubscription = p;
        });

        // Log all errors
        store.error(function (error) {
          console.error('ERROR ' + error.code + ': ' + error.message);
          vm.toast.create({
            cssClass: 'error',
            message: error,
            duration: 3000
          }).present();
        });

        // When store is ready
        store.ready(() => {
          vm.proSubscription = store.get('annual_pro');
        });

        store.refresh();
      });
    });
  }

  SetPro(transaction) {
    const vm = this;

    if (this.auth.userData) {
      return this.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Transaction: transaction
      }, 'transaction');
    }
  }
}
