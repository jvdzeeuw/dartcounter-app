import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio';

/*
  Generated class for the SmartAudioProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SmartAudioProvider {

  audioType: string = 'html5';
  lastAudioKey: any = null;
  audioAsset: HTMLAudioElement;
  sounds: any = [];

  constructor(public nativeAudio: NativeAudio, platform: Platform) {
    if (platform.is('cordova')) {
      this.audioType = 'native';
    }
  }

  /**
   * This functions plays a predefined Audio asset by a given key (can be int or string)
   * Automatically stops the last sound when playing a new one (both on web / app)
   * 
   * @param {any} key 
   * @memberof SmartAudioProvider
   */
  play(key, asset) {
    const vm = this;

    if (vm.audioType === 'html5') {

      if (this.audioAsset) {
        this.audioAsset.pause();
      }
      this.audioAsset = new Audio(asset);
      this.audioAsset.play();

    } else {
      if (this.lastAudioKey) {
        //Stop and unload from buffer
        this.nativeAudio.stop(this.lastAudioKey);
      }

      if (key != this.lastAudioKey) {
        if (this.lastAudioKey) {
          this.nativeAudio.unload(this.lastAudioKey);
          this.lastAudioKey = null;
        }

        this.nativeAudio.preloadComplex(key, asset, 1, 1, 0).then(
          () => {
            vm.nativeAudio.play(key).then((_res) => {
              vm.lastAudioKey = key;
            }, (err) => {
              console.error(err);
            });
          }
        );
      }
      else {
        this.nativeAudio.play(this.lastAudioKey);
      }
    }

  }

  playHTML(asset) {
    if (this.audioAsset) {
      this.audioAsset.pause();
    }
    this.audioAsset = new Audio(asset);
    this.audioAsset.play();
  }

  playAndObserve(asset): HTMLAudioElement {
    if (this.audioAsset) {
      this.audioAsset.pause();
    }
    this.audioAsset = new Audio(asset);
    this.audioAsset.play();

    return this.audioAsset;
  }

  playAndObserveNative(key, asset, completeCallback = null) {
    const vm = this;

    if (this.lastAudioKey) {
      //Stop and unload from buffer
      this.nativeAudio.stop(this.lastAudioKey);
    }

    if (key != this.lastAudioKey) {
      if (this.lastAudioKey) {
        this.nativeAudio.unload(this.lastAudioKey);
        this.lastAudioKey = null;
      }

      this.nativeAudio.preloadComplex(key, asset, 1, 1, 0).then(
        () => {
          vm.nativeAudio.play(key, completeCallback).then((_res) => {
            vm.lastAudioKey = key;
          }, (err) => {
            console.error(err);
          });
        }
      );
    }
    else {
      this.nativeAudio.play(this.lastAudioKey);
    }
  }

  playAndPauseBeforeEnd(asset, beforeEnd: number): HTMLAudioElement {
    const vm = this;

    if (this.audioAsset) {
      this.audioAsset.pause();
    }
    this.audioAsset = new Audio(asset);

    this.audioAsset.onplaying = (() => {
      const duration = this.audioAsset.duration * 1000; //miliseconds
      setTimeout(() => {
        vm.audioAsset.pause();
      }, duration - beforeEnd);

      //reset the listener
      this.audioAsset.onplaying = (() => {});
    });

    this.audioAsset.play();

    return this.audioAsset;
  }

}
