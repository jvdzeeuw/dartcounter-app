import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { TranslateService } from '@ngx-translate/core';

/*
  Generated class for the MobileApiService provider.
*/
@Injectable()
export class MobileApiService {
  public loader: any;
  public notifications: number = 0;

  constructor(public http: Http, public translate: TranslateService,
    public loadingCtrl: LoadingController) {
  }

  GetData(body: any, ...params): Observable<any> {
    const url: string = this.CreateAPIUrl(params);
    return this.http.get(url, { params: body })
      .map(this.extractData)
      .catch(this.handleError);
  }

  GetDataR(body: any, ...params): Observable<any[]> {
    const url: string = this.CreateAPIUrl(params);
    return this.http.get(url, { params: body })
      .map(this.extractData)
      .catch(this.handleError);
  }

  PostData(body: any, ...params): Observable<any> {
    const url: string = this.CreateAPIUrl(params);
    return this.http.post(url, body)
      .map(this.extractData)
      .catch(this.handleError);
  }

  PostDataR(body: any, ...params): Observable<any[]> {
    const url: string = this.CreateAPIUrl(params);
    return this.http.post(url, body)
      .map(this.extractData)
      .catch(this.handleError);
  }

  GetNotifications(auth) {
    const vm = this;

    this.GetData({
      AccountID: auth.AccountID,
      UniqueKey: auth.UniqueKey,
    }, 'friends', 'notifications').subscribe(result => {
      vm.notifications = result.count;
    }, (err) => {
      console.log(err);
    });
  }

  private CreateAPIUrl(params: string[]): string {
    const apiUrl = 'https://legacyapi.dartcounter.net/' + this.translate.currentLang + '/mobile2/';
    if (params != null) {
      return apiUrl + params.join('/');
    }

    return apiUrl;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      if (!error.text()) {
        errMsg = `${error.status} - ${error.statusText || ''}`;
      }
      else {
        const body = error.json() || '';
        if (body.data) {
          errMsg = body.data;
        }
        else {
          if (typeof body != 'string') {
            JSON.stringify(body);
          }
          errMsg = body;
        }
      }
      // errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

  showLoader(_duration: number = null) {

    this.loader = this.loadingCtrl.create({
      spinner: 'hide',
      content: `
            <div class="loading-custom-spinner-container">
              <div class="cssload-container">
                <div class="cssload-lt"></div>
                <div class="cssload-rt"></div>
                <div class="cssload-lb"></div>
                <div class="cssload-rb"></div>
              </div>
            </div>`,
      duration: _duration
    });

    this.loader.present();
  }

  showCircleLoader(_duration: number = null) {

    this.loader = this.loadingCtrl.create({
      spinner: 'hide',
      showBackdrop: false,
      content: `
            <div class="loading-custom-spinner-container">
              <div class="sk-circle">
                <div class="sk-circle1 sk-child"></div>
                <div class="sk-circle2 sk-child"></div>
                <div class="sk-circle3 sk-child"></div>
                <div class="sk-circle4 sk-child"></div>
                <div class="sk-circle5 sk-child"></div>
                <div class="sk-circle6 sk-child"></div>
                <div class="sk-circle7 sk-child"></div>
                <div class="sk-circle8 sk-child"></div>
                <div class="sk-circle9 sk-child"></div>
                <div class="sk-circle10 sk-child"></div>
                <div class="sk-circle11 sk-child"></div>
                <div class="sk-circle12 sk-child"></div>
              </div>
            </div>`,
      duration: _duration
    });

    this.loader.present();
  }

  hideLoader() {
    this.loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
    this.loader = undefined;
  }
}
