import { Component } from '@angular/core';
import { ModalController, LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../providers';
import { AppMenuComponent } from '../../components';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})

export class AccountView {
  public title: string;
  public accountID: number = null;
  public authenticated: boolean;
  public profile: any;
  public thrownScores: Array<any> = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public appMenu: AppMenuComponent,
    public mobileAPI: MobileApiService,
    public modal: ModalController, 
    public navParams: NavParams) {

    const vm = this;

    vm.title = 'account menu';

    if (vm.navParams.get('profile')) {
      vm.profile = vm.navParams.get('profile');
    }

    //Check if authenticated
    vm.GetProfile();
  }

  GetProfile() {
    const vm = this;
    const body = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      ProfileID: this.auth.userData.AccountID
    };

    const loader = this.loading.create({
      showBackdrop: false
    });

    loader.present();
    return new Promise((resolve, reject) => {
      this.mobileAPI.GetData(body, 'profile').subscribe(profile => {
        vm.profile = profile;
        loader.dismiss();
        resolve(profile);
      }, (err) => {
        loader.dismiss();
        reject(err);
      });
    });
  }

  SaveAccount(){
    const vm = this;

    const loader = this.loading.create({
      showBackdrop: true
    });
    loader.present();

    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      account: {
        //FirstName, LastName, Nickname, Darts, Team, League, Division
        FirstName: vm.profile.FirstName,
        LastName: vm.profile.LastName,
        Nickname: vm.profile.Nickname,
        Darts: vm.profile.Darts,
        Team: vm.profile.Team,
        League: vm.profile.League,
        Division: vm.profile.Division,
      }
    }, 'account', 'update').subscribe(profile => {
      vm.auth.saveAuthentication(profile);
      loader.dismiss();      
    }, (err) => {
      loader.dismiss();            
    });
  }

}
