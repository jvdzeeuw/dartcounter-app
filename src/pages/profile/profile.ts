import { Component } from '@angular/core';
import moment from 'moment';

import { ModalController, LoadingController, ViewController, IonicPage, NavController, NavParams, ActionSheetController, PopoverController } from 'ionic-angular';

import { Auth, MobileApiService } from '../../providers';
import { AccountView } from '../';
 

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfileView {
  public title: string;
  public accountID: number = null;
  public authenticated: boolean;
  public loaded: boolean = false;
  public profile: any;
  public profileSegment: any;
  public incomingFriends: Array<any>;
  public thrownScores: Array<any> = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,
    public sheet: ActionSheetController,
    public popover: PopoverController,
    public modal: ModalController, public navParams: NavParams,
     ) {
    const vm = this;

    vm.title = 'page profile';
    vm.profileSegment = 'stats';

    if (vm.navParams.get('profile')) {
      vm.profile = vm.navParams.get('profile');
    }

    //Check if authenticated
    vm.auth.checkAuthentication()
      .then((account: any) => {
        if (vm.navParams.get('AccountID')) {
          vm.accountID = vm.navParams.get('AccountID');
        }
        else {
          vm.accountID = account.AccountID;
        }

        vm.GetProfile(vm.accountID);
      }).catch((err) => {

      });
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  refreshProfile(e) {
    const vm = this;

    const body = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      ProfileID: vm.accountID
    };

    return new Promise((resolve, reject) => {
      this.mobileAPI.GetData(body, 'profile').subscribe(profile => {
        profile.CreatedDate = moment(profile.CreatedDate).toISOString();
        vm.profile = profile;
        e.complete();

      }, (err) => {
        e.complete();
      });
    });
  }

  OpenAccount() {
    const vm = this;

    this.nav.push(AccountView, {
      profile: vm.profile
    });
  }

  GetProfile(profileID: number) {
    const vm = this;
    const body = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      ProfileID: profileID
    };

    vm.mobileAPI.showCircleLoader();
    return new Promise((resolve, reject) => {
      this.mobileAPI.GetData(body, 'profile').subscribe(profile => {
        profile.CreatedDate = moment(profile.CreatedDate).toISOString();
        vm.profile = profile;
        vm.mobileAPI.hideLoader();
        resolve(profile);
      }, (err) => {
        vm.mobileAPI.hideLoader();
        reject(err);
      });
    }).then((profile: any) => {
      vm.GetStatistics(profileID);
      if (profile.Self) {
        vm.GetIncomingFriends();
      }
    });
  }

  GetStatistics(profileID: number) {
    const vm = this;

    vm.mobileAPI.GetDataR({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      ProfileID: profileID
    }, 'statistics', 'scoreCount').subscribe(result => {
      vm.loaded = true;
      vm.thrownScores = result;
    }, (err) => {
      vm.loaded = true;
    });
  }

  GetIncomingFriends() {
    const vm = this;

    vm.mobileAPI.GetDataR({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    }, 'friends', 'incoming').subscribe(result => {
      vm.incomingFriends = result;
      vm.mobileAPI.GetNotifications(vm.auth.userData);
    }, (err) => {
      //Add to unsaved-collection

    });
  }

  AddFriend(accountID) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'add', accountID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      vm.profile.FriendStatus = 'pending';
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }

  ShowPendingOptions(accountID) {
    const vm = this;

    const actionSheet = this.sheet.create(<any>{
      enableBackdropDismiss: true,
      buttons: [
        {
          text: 'Cancel request',
          icon: 'close',
          handler: (bla) => {
            this.RemoveFriend(accountID);
          }
        }
      ]
    });

    actionSheet.present();
  }

  ShowFriendOptions(accountID) {
    const vm = this;

    const actionSheet = this.sheet.create(<any>{
      enableBackdropDismiss: true,
      buttons: [
        {
          text: 'Remove friend',
          icon: 'close',
          handler: () => {
            this.RemoveFriend(accountID);
          }
        }
      ]
    });

    actionSheet.present();
  }

  RemoveFriend(accountID) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'decline', accountID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      vm.profile.FriendStatus = null;
      if (vm.profile.Self) {
        vm.GetProfile(vm.accountID);
      }
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }

  AcceptFriend(accountID) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'accept', accountID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      if (vm.profile.Self) {
        vm.GetProfile(vm.accountID);
      }
      vm.profile.FriendStatus = 'confirmed';
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }

  OpenProfile(accountID, profile) {
    if (accountID != this.accountID) {
      this.nav.push(ProfileView, {
        profile: profile,
        AccountID: accountID
      });
    }
  }
}
