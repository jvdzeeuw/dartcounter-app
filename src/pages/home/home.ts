import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { Auth, MobileApiService, StorageProvider, PreferenceService } from '../../providers';
import { AppMenuComponent } from '../../components';
import { PreferencesDialog } from '../../dialogs';
import { Dashboard, Register, Login, ForgotPassword } from '../';
 


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class Home {

  constructor(public appMenu: AppMenuComponent, public navCtrl: NavController,
    public modal: ModalController,
    public network: Network,
    public storage: StorageProvider,
    public preferenceService: PreferenceService,
    public auth: Auth, public mobileAPI: MobileApiService,
     ) {
  }

  ionViewDidLoad() {
    const vm = this;
    this.appMenu.menu.enable(false);
    vm.mobileAPI.showLoader();

    //Check if already authenticated
    this.auth.checkAuthentication().then((res) => {
      vm.mobileAPI.hideLoader();
      vm.mobileAPI.GetNotifications(this.auth.userData);
      this.navCtrl.setRoot(Dashboard);

      vm.mobileAPI.PostData({
        AccountID: vm.auth.userData.AccountID,
        UniqueKey: vm.auth.userData.UniqueKey
      }, 'backend', 'AccountData').subscribe(account => {
        this.auth.saveAuthentication(account);
        this.appMenu.profile = account;
      }, (err) => { 
        this.appMenu.Logout();
      });

    }, (err) => {
      vm.mobileAPI.hideLoader();

      //IOS login CHECK
      let curUserData: any = localStorage.getItem('currentUserData');
      if (curUserData != null && curUserData != '') {
        curUserData = JSON.parse(localStorage.getItem('currentUserData'));
        localStorage.removeItem('currentUserData');

        vm.mobileAPI.PostData({
          AccountID: curUserData.AccountID,
          UniqueKey: curUserData.UniqueKey
        }, 'backend', 'AccountData').subscribe(account => {
          vm.navCtrl.setRoot(Dashboard);
          vm.auth.saveAuthentication(account);
          vm.appMenu.profile = account;
          vm.mobileAPI.GetNotifications(this.auth.userData);
        }, (err) => {
          vm.storage.GetItemFromStorage('firstTime').then(
            (firstTime) => { },
            () => {
              const prefDialog = vm.modal.create(PreferencesDialog);
              prefDialog.present();
              vm.storage.PutItemInStorage('firstTime', true);
            });
        });
        //END OF IOS login CHECK
      }

      vm.storage.GetItemFromStorage('firstTime').then(
        (firstTime) => { },
        () => {
          const prefDialog = vm.modal.create(PreferencesDialog);
          prefDialog.present();
          vm.storage.PutItemInStorage('firstTime', true);
        });

    });
  }

  GuestMode() {
    this.auth.guest = true;
    this.auth.userData = null;
    this.navCtrl.setRoot(Dashboard);
  }

  ForgotPassword() {
    this.navCtrl.push(ForgotPassword);
  }

  GoToLogin() {
    this.navCtrl.push(Login);
  }

  GoToRegistration() {
    this.navCtrl.push(Register);
  }

}
