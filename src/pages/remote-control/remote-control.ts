import { Component, ViewChild } from '@angular/core';
import { ModalController, ViewController, LoadingController, ToastController, Content, IonicPage, NavController, NavParams } from 'ionic-angular';
import { IonNumericKeyboardOptions } from '../../components';
import { ThrowoutDialog } from '../../dialogs';

import { Auth, MobileApiService } from '../../providers';
 

const _IMPOSSIBLETHROWS = [163, 166, 169, 172, 173, 175, 176, 178, 179];

@IonicPage()
@Component({
  selector: 'page-remote-control',
  templateUrl: 'remote-control.html',
})

export class RemoteControl {
  @ViewChild(Content) content: Content;
  title: string;
  score: any = '';
  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;

  constructor(public nav: NavController, public auth: Auth,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,
    public toast: ToastController,
    public loading: LoadingController,
    public modal: ModalController, public navParams: NavParams,
     ) {
    const vm = this;

    this.title = 'page remote';
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      showExtraButton: true,
      ExtraButtonText: 'throwout',
      rightControlKey: {
        type: 'icon',
        value: 'backspace'
      }
    };
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = this.score.substr(0, this.score.length - 1);
    }
    else if (event.source === 'NUMERIC_KEY') {
      const newScore = this.score + event.key;

      if (parseInt(newScore).toString().length <= 3) {
        this.score = parseInt(newScore).toString();
      }
    }
    else if (event.source === 'extra') {
      this.Throwout();
    }
  }

  SubmitScore(_score) {
    const vm = this;
    if (_score === '') { return; }

    _score = parseInt(_score);
    //Check if score is possible
    if ((_score <= 180 && _IMPOSSIBLETHROWS.indexOf(_score) < 0)
      && _score >= 0) {
      //Correct score
      const loader = this.loading.create({
        showBackdrop: false
      });
      loader.present();

      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        event: {
          event: 'score',
          amount: _score
        },
      }, 'general', 'remoteEvent').subscribe(result => {
        loader.dismiss();
      }, (err) => {
        loader.dismiss();
      });
    }
    else {//Show score error
      vm.mobileAPI.translate.get('invalid score', { value: vm.score }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    vm.score = '';
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  UndoScore(): void {
    const vm = this;

    const loader = this.loading.create({
      showBackdrop: false
    });
    loader.present();

    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      event: {
        event: 'undo',
        amount: null
      },
    }, 'general', 'remoteEvent').subscribe(result => {
      loader.dismiss();
    }, (err) => {
      loader.dismiss();
    });
  }

  Throwout() {
    const vm = this;

    //send throwout with amount of darts
    const throwoutModal = this.modal.create(ThrowoutDialog, { possibleDarts: [1, 2, 3] });
    throwoutModal.onDidDismiss(data => {
      if (data != null) {
        const loader = this.loading.create({
          showBackdrop: false
        });
        loader.present();

        vm.mobileAPI.PostData({
          AccountID: this.auth.userData.AccountID,
          UniqueKey: this.auth.userData.UniqueKey,
          event: {
            event: 'throwout',
            amount: data.dartsThrown
          },
        }, 'general', 'remoteEvent').subscribe(result => {
          loader.dismiss();
        }, (err) => {
          loader.dismiss();
        });
      }
    });

    throwoutModal.present();
    vm.score = '';
  }

}
