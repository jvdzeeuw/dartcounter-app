import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
 

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  title: string;

  constructor( ) {
    this.title = 'Login';
  }

  ionViewDidLoad() {
    const vm = this;
  }
}
