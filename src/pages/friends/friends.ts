import { Component } from '@angular/core';
import { ModalController, ActionSheetController, IonicPage, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService, StorageProvider } from '../../providers';
import { ProfileView } from '../../pages';
 

@IonicPage()
@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})

export class Friends {
  public profile: any;
  public searchString: string = '';
  public showCancel: boolean = false;
  public recentSearches: any[] = [];
  public people: Array<any> = new Array();
  public incomingFriends: Array<any>;

  constructor(public nav: NavController, public auth: Auth,
    public sheet: ActionSheetController,
    public storage: StorageProvider,
    public mobileAPI: MobileApiService,
    public modal: ModalController, public navParams: NavParams,
     ) {


  }

  ionViewCanEnter() {
    const vm = this;

    vm.storage.GetItemFromStorage('recentSearches').then(
      (items: any[]) => {
        vm.recentSearches = items;
      },
      () => {
        vm.recentSearches = [];
      });
  }

  onInput(ev) {
    const vm = this;
    vm.showCancel = vm.searchString.length > 0;

    if (vm.searchString.length >= 3) {
      const body = {
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,

      };

      return new Promise((resolve, reject) => {
        this.mobileAPI.GetDataR(body, 'profile', 'search', vm.searchString).subscribe(people => {
          vm.people = people;
          resolve(people);
        }, (err) => {
          reject(err);
        });
      });
    }
  }

  onClearInput(ev) {
    const vm = this;
    vm.searchString = '';
    vm.showCancel = false;
  }

  GetIncomingFriends() {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.GetDataR({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'incoming').subscribe(result => {
      vm.incomingFriends = result;
      vm.mobileAPI.GetNotifications(vm.auth.userData);
      vm.mobileAPI.hideLoader();
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }

  OpenProfile(person) {
    const vm = this;

    //Add this to recent profile-searches
    vm.storage.GetItemFromStorage('recentSearches').then(
      (items: any[]) => {
        //There's a local item, we add this one
        const found = items.find(x => x.ID === person.ID);
        if (found) {
          items.splice(items.indexOf(found), 1);
        }
        //Add the person and remove anything which is more than 8
        items.unshift(person);
        if (items.length > 8) { items.pop(); }

        vm.storage.PutItemInStorage('recentSearches', items);
      },
      () => {
        //No local items yet, create an array with this one
        vm.storage.PutItemInStorage('recentSearches', [person]);
      });

    this.nav.push(ProfileView, {
      profile: person,
      AccountID: person.ID
    });
  }

  AddFriend(account) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'add', account.ID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      account.Status = 'pending';
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }

  ShowPendingOptions(account) {
    const vm = this;

    const actionSheet = this.sheet.create(<any>{
      enableBackdropDismiss: true,
      buttons: [
        {
          text: 'Cancel request',
          icon: 'close',
          handler: (bla) => {
            this.RemoveFriend(account);
          }
        }
      ]
    });

    actionSheet.present();
  }

  RemoveFriend(account) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'friends', 'decline', account.ID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      account.Status = null;
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();

    });
  }
}
