import { Component } from '@angular/core';
import { App, Platform, IonicPage, NavController, MenuController, ToastController, ModalController } from 'ionic-angular';
import { AppRate } from '@ionic-native/app-rate';

import { Auth } from '../../providers';
import { MobileApiService, StorageProvider, PreferenceService } from '../../providers';

import { AcceptTermsDialog, ActionCodeDialog, LoginRequiredDialog, UpgradeSubscriptionDialog, UltimateSubscriptionDialog } from '../../dialogs';
import { Games } from '../';
import { LoginOrCreateDialog } from '../../dialogs/login-or-create/login-or-create';

/**
 * Generated class for the Games page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class Dashboard {
  public showCode;

  constructor(public app: App, private nav: NavController,
    private auth: Auth,
    public storage: StorageProvider,
    public menu: MenuController,
    public appRate: AppRate,
    public platform: Platform,
    private modal: ModalController,
    private preferenceService: PreferenceService,
    public mobileAPI: MobileApiService, public toast: ToastController) {
    const vm = this;

    vm.mobileAPI.GetData({}, 'code', 'show').subscribe(res => {
      vm.showCode = res.showCode;
    });

    if (vm.auth.userData) {
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
      }, 'profile', 'language').subscribe();

      if ((vm.preferenceService.Preferences.lang == 'nl' || vm.preferenceService.Preferences.lang == 'en') && (!vm.auth.userData.acceptedTerms && vm.auth.userData.acceptedTerms != '1')) {
        const modal = this.modal.create(AcceptTermsDialog);
        modal.onDidDismiss(
          (agreed) => {
            vm.mobileAPI.PostData({
              AccountID: this.auth.userData.AccountID,
              UniqueKey: this.auth.userData.UniqueKey,
            }, 'profile', 'agreement').subscribe(profile => {
              vm.auth.saveAuthentication(profile);
            });
          });
        modal.present();
      }
    }

    vm.storage.GetItemFromStorage('firstTime').then(
      (firstTime) => { },
      () => {
        vm.storage.PutItemInStorage('firstTime', true);
      });
  }

  ionViewWillEnter() {
    const vm = this;

    this.menu.enable(true, 'mainmenu');
    this.menu.enable(false, 'ingamemenu');
  }

  ShowLogin(){
    const loginDialog = this.modal.create(LoginOrCreateDialog);
    loginDialog.present();
  }

  EnterCode(showInfo: boolean) {
    const vm = this;

    if (vm.auth.guest) {
      const loginRequiredDialog = vm.modal.create(LoginRequiredDialog);
      loginRequiredDialog.present();
    }
    else {
      const actionCodeDialog = this.modal.create(ActionCodeDialog, { info: showInfo });
      actionCodeDialog.present();
    }
  }

  // DartboardPOC(){
  //   let vm = this;
  //   //send throwout with amount of darts
  //   let dartboardModal = this.modal.create(DartboardDialog);
  //   dartboardModal.onDidDismiss(data => {
  //     console.log(data)
  //   });

  //   dartboardModal.present();
  // }

  OpenSubscriptionDialog() {
    if (this.auth.userData.isPro == '1') {
      const subscriptionDialog = this.modal.create(UltimateSubscriptionDialog);
      subscriptionDialog.present();
    }
    else {
      const subscriptionDialog = this.modal.create(UpgradeSubscriptionDialog);
      subscriptionDialog.present();
    }
  }

  NewGame() {
    const vm = this;
    this.app.getActiveNav().push(Games);
  }

  ShowRating() {
    const vm = this;

    if (vm.preferenceService.Preferences.lang == 'nl') {
      vm.appRate.preferences.customLocale = {
        title: 'Beoordeel %@',
        message: 'Zou je onze app willen beoordelen? Bedankt voor je steun!',
        cancelButtonLabel: 'Nee, bedankt',
        laterButtonLabel: 'Herinner me later',
        rateButtonLabel: 'Ja!',
        yesButtonLabel: 'Ja',
        noButtonLabel: 'Nee',
        appRatePromptTitle: 'Ben je tevreden met %@?',
        feedbackPromptTitle: 'Zou je ons feedback willen geven?',
      };
    }
    else if (vm.preferenceService.Preferences.lang == 'de') {
      vm.appRate.preferences.customLocale = {
        title: 'Bewerte %@',
        message: 'Würdest Du uns bitte bewerten? Danke für die Unterstützung!',
        cancelButtonLabel: 'Nein, danke',
        laterButtonLabel: 'Später erinnern',
        rateButtonLabel: 'Ja!',
        yesButtonLabel: 'Ja',
        noButtonLabel: 'Nein',
        appRatePromptTitle: 'Bist du zufrieden mit %@?',
        feedbackPromptTitle: 'Geben Sie uns eine Rückmeldung',
      };
    }
    else if (vm.preferenceService.Preferences.lang == 'en') {
      vm.appRate.preferences.customLocale = {
        title: 'Rate %@?',
        message: 'Would you please rate our app? Thank you for your support!',
        cancelButtonLabel: 'No, Thanks',
        laterButtonLabel: 'Remind Me Later',
        rateButtonLabel: 'Yes!',
        yesButtonLabel: 'Yes',
        noButtonLabel: 'No',
        appRatePromptTitle: 'Do you like using %@?',
        feedbackPromptTitle: 'Would you like to give us some feedback?',
      };
    }
    vm.appRate.promptForRating(true);
  }
}
