import { Component } from '@angular/core';
import { ModalController, IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';

import { PreferenceService, MobileApiService, Auth } from '../../providers';
import { AgreementDialog, PolicyDialog } from '../../dialogs';

/**
 * Generated class for the Settings page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-app-settings',
  templateUrl: 'app-settings.html',
})

export class AppSettingsView {
  public title: string;
  public allowCaller: boolean;

  constructor(public nav: NavController,
    public mobileAPI: MobileApiService,
    public auth: Auth,
    public viewController: ViewController,
    public preferenceService: PreferenceService,
    public modal: ModalController, public navParams: NavParams) {

    this.title = 'page settings';
  }

  ionViewWillEnter() {
    this.allowCaller = this.preferenceService.Preferences.allowCaller;

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  OpenAgreementDialog() {
    const modal = this.modal.create(AgreementDialog);
    modal.present();
  }

  OpenPolicyDialog() {
    const modal = this.modal.create(PolicyDialog);
    modal.present();
  }

  LanguageChanged() {
    this.preferenceService.LanguageChanged();

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });

    if (this.auth.userData) {
      this.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
      }, 'profile', 'language').subscribe();
    }
  }
}
