import { Component } from '@angular/core';
import { IonicPage, ModalController, MenuController, NavController, ToastController } from 'ionic-angular';


import { AppMenuComponent } from '../../components';
import { Auth, MobileApiService, SocialMediaService } from '../../providers';
import { Games, Login } from '../';
 

/**
 * Generated class for the ForgotPassword page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPassword {
  step: number = 1;
  Email: string;
  Digits: string;
  Password: string;
  Password2: string;
  PassHash: string;
  Username: string;

  constructor(public menu: MenuController, 
    public appMenu: AppMenuComponent,
    public toast: ToastController, 
    public navCtrl: NavController,
    public social: SocialMediaService, 
    public modal: ModalController,
    public auth: Auth, 
    public mobileAPI: MobileApiService,
     ) {
  }

  GoToLogin() {
    this.navCtrl.setRoot(Login);
  }

  ForgotPass() {
    const vm = this;
    
    vm.mobileAPI.PostData({ 
      account: {
        Email: vm.Email,
      } 
    }, 'backend', 'recover').subscribe(res => {
      vm.step++;

      if (res.data != undefined){
        this.toast.create({
          cssClass: 'success',
          message: res.data,
          duration: 3000
        }).present();
      }
    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  AttemptDigits() {
    const vm = this;
    
    vm.mobileAPI.PostData({ 
      account: {
        Email: vm.Email,
        Digits: vm.Digits
      } 
    }, 'general', 'attempt').subscribe(dbaccount => {
      vm.step++;
      vm.PassHash = dbaccount.PassHash;
      vm.Username = dbaccount.Username;

      if (dbaccount.data != undefined){
        this.toast.create({
          cssClass: 'success',
          message: dbaccount.data,
          duration: 3000
        }).present();
      }
    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  SubmitNewPassword() {
    const vm = this;
    
    vm.mobileAPI.PostData({ 
      account: {
        Email: vm.Email,
        Password: vm.Password,
        Password2: vm.Password2,
        PassHash: vm.PassHash,
        Digits: null,
      } 
    }, 'general', 'newPass').subscribe(profile => {
      vm.appMenu.profile = profile;
      vm.auth.saveAuthentication(profile);
      vm.mobileAPI.GetNotifications(vm.auth.userData);
      vm.navCtrl.setRoot(Games);
      vm.step = 1;

      if (profile.data != undefined){
        this.toast.create({
          cssClass: 'success',
          message: profile.data,
          duration: 3000
        }).present();
      }
    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }
}
