import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Content, ModalController, ViewController, PopoverController, MenuController, IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Settings } from '../../../models/settings';

import { SimpleStatisticsDialog, TutorialDialog, SwipeForMenu } from '../../../dialogs';

import { IonNumericKeyboardOptions, IngameMenuComponent } from '../../../components';
import { StorageProvider, MobileApiService, Auth, PreferenceService } from '../../../providers';
import { Platform } from 'ionic-angular/platform/platform';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
 


@IonicPage()
@Component({
  selector: 'page-doubles-training',
  templateUrl: 'doubles-training.html',
})
export class DoublesTrainingPage {
  @ViewChild(Content) content: Content;
  settings: Settings;
  storageKey: string = 'doubles';
  ID: any = Date.now();

  title: string;
  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;
  score: any = '';
  players: any[] = [];
  currentPlayer: any;
  currentOrientation: any;

  invalid: boolean = false;

  finishedPeople: number = 0;
  dartboardValues: any[];

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public auth: Auth,
    public menu: MenuController,
    public popoverCtrl: PopoverController,
    public inGameMenu: IngameMenuComponent,
    public platform: Platform,
    public chRef: ChangeDetectorRef,
    public screenOrientation: ScreenOrientation,
    public toast: ToastController,
    public mobileAPI: MobileApiService,
    public preferenceService: PreferenceService,
    public viewController: ViewController,
    public storage: StorageProvider,
    public loading: LoadingController,
     ) {

    const vm = this;
    this.settings = navParams.get('settings');
    this.title = 'page doubles';

    this.menu.enable(false, 'mainmenu');
    this.menu.enable(true, 'ingamemenu');

    for (const team of this.settings.teams) {
      //Concat the players to the playerlist
      for (const player of team.players) {
        player.scores = [];
        player.dartsThrown = 0;
        player.done = false;
        player.atIndex = 0;
        player.finishes = 0;
        player.turns = [];

        vm.players.push(player);
      }
    }

    this.dartboardValues = [
      { val: 1, title: 1 },
      { val: 2, title: 2 },
      { val: 3, title: 3 },
      { val: 4, title: 4 },
      { val: 5, title: 5 },
      { val: 6, title: 6 },
      { val: 7, title: 7 },
      { val: 8, title: 8 },
      { val: 9, title: 9 },
      { val: 10, title: 10 },
      { val: 11, title: 11 },
      { val: 12, title: 12 },
      { val: 13, title: 13 },
      { val: 14, title: 14 },
      { val: 15, title: 15 },
      { val: 16, title: 16 },
      { val: 17, title: 17 },
      { val: 18, title: 18 },
      { val: 19, title: 19 },
      { val: 20, title: 20 },
      { val: 25, title: 'BULL' }
    ];

    if (this.settings.sequence == 'desc') {
      this.dartboardValues.reverse();
    }
    else if (this.settings.sequence == 'random') {
      this.dartboardValues = this.ShuffleArray(this.dartboardValues);
    }

    this.NextPlayer(0);
  }

  // -> Fisher–Yates shuffle algorithm
  ShuffleArray(array) {
    let m = array.length, t, i;

    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }

  SwipeForMenu() {
    const popover = this.popoverCtrl.create(SwipeForMenu, {}, {
      cssClass: 'swipePopover'
    });
    popover.present();
    this.storage.PutItemInStorage('swipeForMenu', true);

    setTimeout(() => {
      this.OpenInGameMenu();
    }, 2000);
  }

  OpenInGameMenu() {
    this.menu.open('ingamemenu');
  }

  Shuffle(array: any[]): any[] {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  ionViewWillEnter() {
    const vm = this;

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });

    vm.CheckOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        if (vm.screenOrientation.type.indexOf('portrait') >= 0) {
          vm.currentOrientation = 'portrait';
        }
        else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
          vm.currentOrientation = 'landscape';
        }

        setTimeout(() => {
          vm.CheckOrientation();
        }, 500);
      }
    );
  }

  CheckOrientation() {
    const vm = this;

    let totalHeight = this.platform.height();
    let totalWidth = this.platform.width();
    if (totalHeight < totalWidth) {
      const tempHeight = totalHeight;
      totalHeight = totalWidth;
      totalWidth = tempHeight;
    }

    if (vm.platform.is('core') || vm.platform.is('mobileweb') ||
      vm.screenOrientation.type.indexOf('portrait') >= 0) {
        vm.currentOrientation = 'portrait';
        if (document.querySelector('.playerssection')) {
        const playersHeight = document.querySelector('.playerssection').clientHeight;
        vm.preferenceService.Preferences.keySize = Math.floor((totalHeight - 115 - playersHeight) / 2);
      }
    }
    else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
      vm.currentOrientation = 'landscape';
      vm.preferenceService.Preferences.keySize = Math.floor((totalWidth - 115) / 2);
    }
    vm.chRef.detectChanges();
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      onlyThreeOptions: true,
      hideBottom: true,
      showExtraButton: true,
      ExtraButtonText: 'miss',
      rightControlKey: {
        type: 'icon',
        value: 'backspace'
      }
    };
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = this.score.substr(0, this.score.length - 1);
    }
    else if (event.source === 'NUMERIC_KEY') {
      //Score can be only a single digit with DoublesTraining (0 - 9)
      this.score = event.key;
    }
    else if (event.source === 'extra') {
      this.SubmitScore(null);
    }
    this.chRef.detectChanges();  
  }

  SubmitScore(_score) {
    const vm = this;

    if (_score == null) {
      _score = 0;
    }
    _score = parseInt(_score);

    let darts = 3;
    if (_score > 0) {
      darts = _score;
    }

    const dartboardValue = vm.dartboardValues[vm.currentPlayer.atIndex];
    if (!vm.currentPlayer.scores[dartboardValue.val]) {
      vm.currentPlayer.scores[dartboardValue.val] = { at: 0, finished: false, gaveup: false };
    }
    vm.currentPlayer.scores[dartboardValue.val].at += darts;

    vm.currentPlayer.dartsThrown += darts;
    vm.currentPlayer.turns.push({
      number: dartboardValue.val,
      finished: (_score > 0),
      darts: darts
    });

    if (_score > 0) {
      vm.currentPlayer.scores[dartboardValue.val].finished = true;
      vm.currentPlayer.finishes++;
      vm.currentPlayer.atIndex++;

      //There's no next one
      if (!vm.dartboardValues[vm.currentPlayer.atIndex]) {
        vm.finishedPeople++;
        if (vm.players.length == vm.finishedPeople) {
          vm.TrySave();
          return false;
        } else {
          // vm.toast.create({
          //   cssClass: 'success',
          //   message: 'You are done',
          //   duration: 3000
          // }).present();
        }
        vm.currentPlayer.done = true;
      }
    }
    vm.NextPlayer();
  }

  UndoScore(): void {
    const vm = this;
    const currentPlayerIndex = vm.players.indexOf(vm.currentPlayer);

    //First player & no turns played yet
    if (currentPlayerIndex == 0 && !vm.currentPlayer.turns.length) {
      return;
    }

    //Current player is the first player
    if (currentPlayerIndex == 0) {
      //Go to last player
      vm.currentPlayer = vm.players[vm.players.length - 1];
    } else {
      //CurrentPlayerIndex - 1
      vm.currentPlayer = vm.players[currentPlayerIndex - 1];
    }

    if (vm.currentPlayer.done) {
      vm.currentPlayer.done = false;
      vm.finishedPeople--;
    }

    const last_turn = vm.currentPlayer.turns.pop();
    if (last_turn.finished) {
      //Back to the previous double
      vm.currentPlayer.atIndex--;
      vm.currentPlayer.finishes--;
      vm.currentPlayer.scores[last_turn.number].finished = false;
    }
    vm.currentPlayer.dartsThrown -= last_turn.darts;
    vm.currentPlayer.scores[last_turn.number].at -= last_turn.darts;
  }

  NextPlayer(playerIndex: number = null) {
    const vm = this;

    if (playerIndex != null) {
      vm.currentPlayer = vm.players[playerIndex];
    } else {
      if (vm.players.length > 1) {
        const newIndex = vm.players.indexOf(vm.currentPlayer) + 1;
        //If we are at the last item of the array
        if (!vm.players[newIndex]) {
          //Go to first player
          vm.currentPlayer = vm.players[0];
        } else {
          vm.currentPlayer = vm.players[newIndex];
        }

        if (vm.currentPlayer.done) {
          vm.NextPlayer();
        }
      }
    }

    vm.score = '';
  }

  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  TrySave() {
    const vm = this;

    if (vm.auth.userData) {
      const gameData = {
        ID: vm.ID,
        singlePlayers: vm.players
      };

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Game: gameData
      }, 'complete', vm.storageKey).subscribe(result => {
        vm.ShowEnd();
        vm.mobileAPI.hideLoader();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.storageKey + 'unsaved').then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    /* set the custom stats */
    const checkoutRate = { title: 'checkout rate', format: 'percentage', values: [] };

    for (const player of vm.players) {
      checkoutRate.values.push((player.finishes / player.dartsThrown) * 100);
    }

    const statisticsModal = this.modal.create(SimpleStatisticsDialog,
      {
        ignoreTotal: true,
        statistics: [
          checkoutRate
        ],
        players: vm.players,
      });
    statisticsModal.onDidDismiss(data => {
    });

    this.nav.pop();
    statisticsModal.present();
  }

  ShowHelp() {
    const vm = this;

    const tutorial = this.modal.create(TutorialDialog,
      {
        slides: [
          {
            title: vm.storageKey + ' slide1 title',
            description: vm.storageKey + ' slide1 content',
          },
          {
            title: vm.storageKey + ' slide2 title',
            description: vm.storageKey + ' slide2 content',
            image: 'assets/img/ica-slidebox-img-2.png',
          },
          {
            title: vm.storageKey + ' slide3 title',
            description: vm.storageKey + ' slide3 content',
            image: 'assets/img/ica-slidebox-img-3.png',
          }
        ]
      });
    tutorial.onDidDismiss(data => {
    });

    tutorial.present();
  }
}
