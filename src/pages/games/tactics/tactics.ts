import { Component, ViewChild } from '@angular/core';
import { Content, ModalController, MenuController, IonicPage, ViewController, ToastController, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Settings } from '../../../models/settings';

import { SimpleStatisticsDialog, TutorialDialog } from '../../../dialogs';

import { Auth, MobileApiService, StorageProvider, PreferenceService } from '../../../providers';
import { IngameMenuComponent } from '../../../components';
 

//Tactics Bot
const _TACTICS_LEVELS = [];
_TACTICS_LEVELS[1] = { start: 50, decrease: 15, smart: 0 };
_TACTICS_LEVELS[2] = { start: 70, decrease: 20, smart: 0 };
_TACTICS_LEVELS[3] = { start: 70, decrease: 10, smart: 1 };

_TACTICS_LEVELS[4] = { start: 75, decrease: 15, smart: 1 };
_TACTICS_LEVELS[5] = { start: 100, decrease: 15, smart: 2 };
_TACTICS_LEVELS[6] = { start: 100, decrease: 10, smart: 2 };
_TACTICS_LEVELS[7] = { start: 105, decrease: 10, smart: 2 };

_TACTICS_LEVELS[8] = { start: 120, decrease: 15, smart: 3 };
_TACTICS_LEVELS[9] = { start: 130, decrease: 15, smart: 3 };
_TACTICS_LEVELS[10] = { start: 140, decrease: 10, smart: 3 };
//End of default settings

@IonicPage()
@Component({
  selector: 'page-tactics',
  templateUrl: 'tactics.html',

})
export class TacticsPage {
  @ViewChild(Content) content: Content;
  settings: Settings;
  vsCPU: boolean = false;
  storageKey: string = 'tactics';
  ID: any = Date.now();
  cpuLevel: number = null;

  title: string;
  thinkingBot: any;
  score: any = '';
  teams: any[] = [];
  players: any[] = [];

  currentPlayer: any;
  currentTeam: any;
  thinktime: number;
  CPUActions: string;

  gameValues: any[] = [];
  closedNumbers: any[] = [];
  finishedPeople: any[] = [];
  turnsThrown: number = 0;
  currentLog: any;

  invalid: boolean = false;
  teammode: boolean = false;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public menu: MenuController,
    public inGameMenu: IngameMenuComponent,
    public viewController: ViewController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public storage: StorageProvider,
    public preferenceService: PreferenceService,
    public toast: ToastController,
    public loading: LoadingController,
     ) {

    this.menu.enable(false, 'mainmenu');
    //this.menu.enable(true, "ingamemenu");

    const vm = this;
    this.settings = navParams.get('settings');
    this.vsCPU = this.settings.vsCPU;

    this.title = vm.settings.gameMode;
    this.teams = this.settings.teams;

    if (vm.settings.gameMode == 'Cricket') {
      vm.gameValues = [
        { val: 20, title: 20 },
        { val: 19, title: 19 },
        { val: 18, title: 18 },
        { val: 17, title: 17 },
        { val: 16, title: 16 },
        { val: 15, title: 15 },
        { val: 25, title: 25 },
      ];
    }
    else {
      vm.gameValues = [
        { val: 20, title: 20 },
        { val: 19, title: 19 },
        { val: 18, title: 18 },
        { val: 17, title: 17 },
        { val: 16, title: 16 },
        { val: 15, title: 15 },
        { val: 14, title: 14 },
        { val: 13, title: 13 },
        { val: 12, title: 12 },
        { val: 11, title: 11 },
        { val: 10, title: 10 },
        { val: 25, title: 25 },
      ];
    }

    for (const team of this.teams) {
      //default Team values
      team.scores = [];
      team.dartsThrown = 0;
      team.finishes = 0;
      team.totalAmount = [];
      team.total = 0;

      //MultiDimensional Array
      team.activityLog = [];
      team.turn = {
        minDarts: 0,
        score: 0,
        amountOf: [],
      };

      //Concat the players to the playerlist
      for (const player of team.players) {
        player.scores = [];
        player.dartsThrown = 0;
        player.finishes = 0;
        player.totalAmount = [];
        player.total = 0;

        //MultiDimensional Array
        player.activityLog = [];
        player.turn = {
          minDarts: 0,
          score: 0,
          amountOf: [],
        };

        if (player.isCPU) {
          vm.cpuLevel = player.cpuLevel;
        }
        vm.players.push(player);
      }
    }

    if (vm.players.length > vm.teams.length) {
      vm.teammode = true;
    }

    vm.SetPlayer();
    const turn = {
      minDarts: 0,
      score: 0,
      amountOf: [],
    };

    if (vm.currentPlayer.isCPU) {
      vm.CalculateTurnForCPU();
    }

    vm.currentLog = [];
    vm.currentTeam.turn = turn;
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  PressKey(number, logging): void {
    const vm = this;

    const currentTurn = vm.currentTeam.turn;
    const logItem = {
      number: number,
      amount: 1,
      score: 0,
      closed: false,
      extraDart: false,
    };

    if (vm.closedNumbers[number] && vm.closedNumbers[number] == vm.teams.length) {
      return;
    }


    if (currentTurn.minDarts == 3 && !vm.currentPlayer.level) {
      if (
        (number == 25 && (!currentTurn.amountOf[number] || currentTurn.amountOf[number] == 2 || currentTurn.amountOf[number] == 4 || currentTurn.amountOf[number] == 6))
        || (number != 25 && (!currentTurn.amountOf[number] || currentTurn.amountOf[number] == 3 || currentTurn.amountOf[number] == 6 || currentTurn.amountOf[number] == 9))
      ) {
        //Show score error
        vm.mobileAPI.translate.get('3 darts used').subscribe((res: string) => {
          vm.toast.create({
            cssClass: 'error',
            message: res,
            duration: 3000
          }).present();
        });
        return;
      }
    }

    //Fill the historicAmount and the currentAmount
    let historicAmount = 0;
    if (vm.currentTeam.totalAmount[number] != undefined && vm.currentTeam.totalAmount[number] > 0) {
      historicAmount = vm.currentTeam.totalAmount[number];
    }

    if (!currentTurn.amountOf[number]) {
      currentTurn.amountOf[number] = 0;
    }

    currentTurn.amountOf[number]++;
    if (number == 25 && (currentTurn.amountOf[number] == 1 || currentTurn.amountOf[number] == 3 || currentTurn.amountOf[number] == 5)) {
      currentTurn.minDarts++;
      logItem.extraDart = true;
    } else if (number != 25 && (currentTurn.amountOf[number] == 1 || currentTurn.amountOf[number] == 4 || currentTurn.amountOf[number] == 7)) {
      currentTurn.minDarts++;
      logItem.extraDart = true;
    }

    if (historicAmount < 3 && (historicAmount + currentTurn.amountOf[number]) == 3) {
      //This player closed the number
      if (vm.closedNumbers[number]) {
        vm.closedNumbers[number]++;
      } else {
        vm.closedNumbers[number] = 1;
      }

      logItem.closed = true;
      vm.currentTeam.finishes++;
    }

    if (!vm.closedNumbers[number] || vm.closedNumbers[number] < vm.teams.length) {

      const score = number;
      if (historicAmount == 3 || (historicAmount + currentTurn.amountOf[number]) > 3) {
        currentTurn.score += score;
        logItem.score = score;
      }
    }

    if (logging == true) {
      vm.currentLog.push(logItem);
    }
  }

  UndoScore() {
    const vm = this;

    if (vm.currentLog.length) {
      const logItem = vm.currentLog.pop();
      //Subtract the score
      vm.currentTeam.turn.score -= logItem.score;
      vm.currentPlayer.turn.score -= logItem.score;

      //Subtract the minDarts
      if (logItem.extraDart) {
        vm.currentTeam.turn.minDarts--;
        vm.currentPlayer.turn.minDarts--;
      }

      //Subtract the closeds
      if (logItem.closed) {
        vm.closedNumbers[logItem.number]--;
        vm.currentTeam.finishes--;
      }

      //Subtract the amount of thrown times
      vm.currentTeam.turn.amountOf[logItem.number] -= logItem.amount;

    } else if (vm.turnsThrown > 0) {
      vm.turnsThrown--;

      //Go to previous player
      vm.SetPlayer();

      if (vm.currentTeam.activityLog.length > 0) {
        //Pop activityLog from team
        vm.currentLog = vm.currentTeam.activityLog.pop();

        if (!vm.currentPlayer.isCPU) {
          vm.currentPlayer.turn = {
            minDarts: 0,
            score: 0,
            amountOf: [],
          };

          vm.currentLog.forEach(function (logItem) {
            vm.UndoLogItem(logItem);
          });
          vm.currentLog.forEach(function (logItem) {
            vm.PressKey(logItem.number, false);
          });
        } else if (vm.currentPlayer.isCPU) {
          while (vm.currentLog.length > 0) {
            vm.UndoLogItem(vm.currentLog.pop());
          }

          vm.UndoScore();
        }
      }
    }
    else if (vm.turnsThrown == 0 && vm.currentPlayer.isCPU) {
      //recalculate CPU throw, if no CPU -> the player will just fill in his score
      vm.CalculateTurnForCPU();
    }
  }

  UndoLogItem(logItem) {
    const vm = this;
    vm.currentTeam.total -= logItem.score;
    vm.currentPlayer.total -= logItem.score;
    vm.currentTeam.totalAmount[logItem.number] -= logItem.amount;

    //Subtract the closeds
    if (logItem.closed) {
      vm.closedNumbers[logItem.number]--;
      vm.currentTeam.finishes--;
    }
  }

  /**
   * Set the currentTeam and currentPlayer according to the amount of turns thrown in this match
   * 
   * @param {number} scoresThrown 
   * @memberof TacticsPage
   */
  SetPlayer() {
    const vm = this;

    const offset = vm.turnsThrown;
    const teamIndex = offset % vm.teams.length;
    //Set the currentTeam
    vm.currentTeam = vm.teams[teamIndex];

    let playerIndex = 0;
    if (vm.teammode) {
      const roundsThrown = Math.floor(offset / vm.teams.length);
      playerIndex = (roundsThrown) % vm.currentTeam.players.length;
    }

    //Set the currentPlayer
    vm.currentPlayer = vm.currentTeam.players[playerIndex];
    vm.currentLog = [];
  }

  NextPlayer() {
    const vm = this;

    vm.turnsThrown++;

    const turn = vm.currentTeam.turn;
    vm.currentTeam.total += turn.score;
    vm.currentPlayer.total += turn.score;
    vm.currentTeam.activityLog.push(vm.currentLog);

    turn.amountOf.forEach(function (value, number) {
      let oldPlayerAmount = 0;
      let oldTeamAmount = 0;
      if (vm.currentPlayer.totalAmount[number] != undefined) {
        oldPlayerAmount = vm.currentPlayer.totalAmount[number];
      }
      vm.currentPlayer.totalAmount[number] = oldPlayerAmount + value;

      if (vm.currentTeam.totalAmount[number] != undefined) {
        oldTeamAmount = vm.currentTeam.totalAmount[number];
      }
      vm.currentTeam.totalAmount[number] = oldTeamAmount + value;
    });

    vm.currentTeam.turn = {
      minDarts: 0,
      score: 0,
      amountOf: [],
    };

    //Check if the person closed everything
    if (vm.currentTeam.finishes == vm.gameValues.length) {
      let highest = true;

      //Check if the total score is also higher
      vm.teams.forEach(function (team) {
        if (team != vm.currentTeam && vm.currentTeam.total <= team.total) {
          highest = false;
        }
      });

      if (highest) {
        vm.TrySave();
      }
      else {
        this.SetPlayer();

        if (vm.currentPlayer.isCPU) {
          vm.CalculateTurnForCPU();
        }
      }
    }
    else {
      this.SetPlayer();

      if (vm.currentPlayer.isCPU) {
        vm.CalculateTurnForCPU();
      }
    }
  }

  /**
   * How long the loader should show up for the dartbot
   * 
   * @param {number} [_duration=null] 
   * @memberof TacticsPage
   */
  DartbotIsThinking(_duration: number = null) {
    this.thinkingBot = this.loading.create({
      spinner: 'hide',
      content: `
        <div class="cssload-loader-walk">
          <div></div><div></div><div></div><div></div><div></div>
        </div>`,
      duration: _duration
    });

    this.thinkingBot.present();
  }

  /**
   * Calculate the turn for the CPU based on it's level
   * 
   * @memberof TacticsPage
   */
  CalculateTurnForCPU(): void {
    const vm = this;

    vm.mobileAPI.translate.get('dartbot throws').subscribe((res: string) => {
      vm.CPUActions = res + ' ';
    });

    const setting = _TACTICS_LEVELS[vm.currentPlayer.cpuLevel];
    let percentage = setting.start;
    let points = 0;

    while (Math.floor((Math.random() * 100) + 1) <= percentage && points < 9) {
      points++;
      percentage -= setting.decrease;
    }

    vm.GetHitDarts(points);
  }

  GetHitDarts(points: number): void {
    const vm = this;

    const option = Math.floor((Math.random() * 3) + 1);
    let result = { first: 0, second: 0, third: 0 };

    if (points == 9) {
      result = { first: 3, second: 3, third: 3 };
    } else if (points == 8) {
      switch (option) {
        case 1:
          result = { first: 3, second: 2, third: 3 };
          break;
        case 2:
          result = { first: 2, second: 3, third: 3 };
          break;
        case 3:
          result = { first: 3, second: 3, third: 2 };
          break;
      }
    } else if (points == 7) {
      switch (option) {
        case 1:
          result = { first: 3, second: 1, third: 3 };
          break;
        case 2:
          result = { first: 1, second: 3, third: 3 };
          break;
        case 3:
          result = { first: 3, second: 3, third: 1 };
          break;
      }
    } else if (points == 6) {
      switch (option) {
        case 1:
          result = { first: 3, second: 3, third: 0 };
          break;
        case 2:
          result = { first: 3, second: 2, third: 1 };
          break;
        case 3:
          result = { first: 2, second: 1, third: 3 };
          break;
      }
    } else if (points == 5) {
      switch (option) {
        case 1:
          result = { first: 3, second: 2, third: 0 };
          break;
        case 2:
          result = { first: 3, second: 1, third: 1 };
          break;
        case 3:
          result = { first: 1, second: 3, third: 1 };
          break;
      }
    } else if (points == 4) {
      switch (option) {
        case 1:
          result = { first: 3, second: 1, third: 0 };
          break;
        default:
          result = { first: 1, second: 3, third: 0 };
          break;
      }
    } else if (points == 3) {
      switch (option) {
        case 1:
          result = { first: 3, second: 0, third: 0 };
          break;
        case 2:
          result = { first: 1, second: 2, third: 0 };
          break;
        default:
          result = { first: 1, second: 1, third: 1 };
          break;
      }
    } else if (points == 2) {
      switch (option) {
        case 1:
          result = { first: 2, second: 0, third: 0 };
          break;
        default:
          result = { first: 1, second: 1, third: 0 };
          break;
      }
    } else if (points == 1) {
      result = { first: 1, second: 0, third: 0 };
    }

    if (result.first > 0) {
      vm.GetTacticsTurn([result.first, result.second, result.third]);
    }
    else {
      vm.mobileAPI.translate.get('no score').subscribe((res: string) => {
        vm.CPUActions += res;
      });
    }

    vm.toast.create({
      cssClass: 'success',
      message: vm.CPUActions,
      duration: 3000
    }).present();

    vm.NextPlayer();
  }

  GetTacticsTurn(points: number[]) {
    const vm = this;
    points.forEach(function (point) {
      if (point > 0) {
        const setting = _TACTICS_LEVELS[vm.currentPlayer.cpuLevel];

        let item = null;

        if (setting.smart > 1) {
          let index = 0;
          item = vm.gameValues[index];

          if (!vm.closedNumbers[item.title]) {
            vm.closedNumbers[item.title] = 0;
          }

          //Score!
          if (Math.floor((Math.random() * 100) + 1) <= 40 || vm.currentPlayer.finishes == vm.gameValues.length) {
            while (item != undefined && vm.GetCloseRate(item.title, vm.currentTeam) > 2 &&
              vm.closedNumbers[item.title] == vm.teams.length) {
              index++;
              item = vm.gameValues[index];
            }
          } else {
            while (item != undefined && vm.GetCloseRate(item.title, vm.currentTeam) > 2) {
              index++;
              item = vm.gameValues[index];
            }
          }
        } else {
          item = vm.gameValues[Math.floor(Math.random() * vm.gameValues.length)];
          while (vm.GetCloseRate(item.title, vm.currentTeam) > 2 &&
            vm.closedNumbers[item.title] == vm.teams.length) {
            item = vm.gameValues[Math.floor(Math.random() * vm.gameValues.length)];
          }
        }

        if (!item) {
          item = vm.gameValues[Math.floor(Math.random() * vm.gameValues.length)];
          while (vm.GetCloseRate(item.title, vm.currentTeam) > 2 &&
            vm.closedNumbers[item.title] == vm.teams.length) {
            item = vm.gameValues[Math.floor(Math.random() * vm.gameValues.length)];
          }
        }

        let boardScore = '';
        if (item.val != 25) {
          switch (point) {
            case 1:
              boardScore = 'single';
              break;
            case 2:
              boardScore = 'double';
              break;
            case 3:
              boardScore = 'triple';
          }
          vm.CPUActions += boardScore + ' ' + item.title + ' ';
        } else {
          if (point > 2) {
            point = 2;
          }

          switch (point) {
            case 1:
              boardScore = 'single';
              break;
            case 2:
              boardScore = 'double';
          }
          vm.CPUActions += boardScore + ' BULL ';
        }

        for (let i = 0; i < point; i++) {
          vm.PressKey(item.title, true);
        }
      }
    });
  }

  GetCloseRate(number: number, team): number {
    const amount = team.totalAmount[number] ? team.totalAmount[number] : 0;
    const turnAmount = team.turn.amountOf[number] ? team.turn.amountOf[number] : 0;

    return turnAmount + amount;
  }

  GetAmountOfValue(value: number) {
    if (this.currentTeam) {
      return this.currentTeam.turn.amountOf[value];
    }
  }

  /**
   * Checks if the given param is a valid number
   * 
   * @param {*} n 
   * @returns {boolean} 
   * @memberof TacticsPage
   */
  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  TrySave() {
    const vm = this;

    if (vm.auth.userData) {

      const gameData = {
        ID: vm.ID,
        vsCPU: vm.vsCPU,
        gameMode: vm.settings.gameMode,
        singlePlayers: vm.players
      };

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Game: gameData
      }, 'complete', vm.storageKey).subscribe(result => {
        vm.ShowEnd();
        vm.mobileAPI.hideLoader();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.storageKey + 'unsaved').then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    if (vm.teams.length > 1) {
      /* set the custom stats */
      const totals = { title: 'total score', values: [] };

      for (const team of vm.teams) {
        totals.values.push(team.total);
      }

      const statisticsModal = this.modal.create(SimpleStatisticsDialog,
        {
          statistics: [
            totals
          ],
          teams: vm.teams,
        });
      statisticsModal.onDidDismiss(data => {
      });

      statisticsModal.present();
    }
    this.nav.pop();
  }

  ShowHelp() {
    const vm = this;

    const tutorial = this.modal.create(TutorialDialog,
      {
        slides: [
          {
            title: vm.storageKey + ' slide1 title',
            description: vm.storageKey + ' slide1 content',
          },
          {
            title: vm.storageKey + ' slide2 title',
            description: vm.storageKey + ' slide2 content',
            image: 'assets/img/ica-slidebox-img-2.png',
          },
          {
            title: vm.storageKey + ' slide3 title',
            description: vm.storageKey + ' slide3 content',
            image: 'assets/img/ica-slidebox-img-3.png',
          },
          {
            title: vm.storageKey + ' slide4 title',
            description: vm.storageKey + ' slide4 content',
            image: 'assets/img/ica-slidebox-img-4.png',
          }
        ]
      });
    tutorial.onDidDismiss(data => {
    });

    tutorial.present();
  }

}
