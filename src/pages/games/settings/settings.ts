import { Component } from '@angular/core';
import { ModalController, MenuController, ViewController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ISettingsConfig } from '../../../interfaces/ISettingsConfig';
import { Settings } from '../../../models/settings';
import { Team } from '../../../models/team';

import { LinkPeopleDialog, LoginRequiredDialog, LinkSoundDialog } from '../../../dialogs';
import { Auth, SmartAudioProvider, PreferenceService, StorageProvider, MobileApiService, FileTransferService } from '../../../providers';
import { FileEntry } from '@ionic-native/file';
import { LinkAccountWarningDialog } from '../../../dialogs/link-account-warning/link-account-warning';
import { LoginOrCreateDialog } from '../../../dialogs/login-or-create/login-or-create';


const _STARTSCORES = [301, 501, 701, 'Custom'];
const _SEQUENCES = [
  { type: 'asc', title: 'low to high' },
  { type: 'desc', title: 'high to low' },
  { type: 'random', title: 'random' }
];
const _DIFFICULTIES = [
  { type: 'easy', title: 'LBL_DIFFICULTY_EASY' },
  { type: 'hard', title: 'LBL_DIFFICULTY_HARD' },
];
const _GAMEMODES = [
  { mode: 'Cricket', title: '15 > 20 + BULL' },
  { mode: 'Tactics', title: '10 > 20 + BULL' }
];

/**
 * Generated class for the Settings page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})

export class SettingsView implements ISettingsConfig {
  private win: any = window;
  public settings: Settings;

  public title: string;
  public gameComponent: string;
  public teamMode: boolean = false;
  public startScores: any[];
  public sequences: any[];
  public gameModes: any[];
  public difficulties: any[];

  public amountOfPlayers: number = 0;
  public amountOfTeams: number = 0;
  public allowReorder: boolean = false;

  public unfinishedGames: any[] = [];
  public unsavedGames: any[] = [];

  //For turning the dartbot on and off
  public dartbotTeam: Team;
  public tempPlayer: any;

  constructor(public nav: NavController,
    public auth: Auth,
    public storage: StorageProvider,
    public menu: MenuController,
    public fileTransfer: FileTransferService,
    public smartAudio: SmartAudioProvider,
    public viewController: ViewController,
    public mobileAPI: MobileApiService,
    public preferenceService: PreferenceService,
    public modal: ModalController, public navParams: NavParams,
  ) {

    this.settings = this.navParams.get('settings');
    this.gameComponent = this.navParams.get('gameplay');
    this.title = this.navParams.get('title');

    //Initialize the default select-lists
    this.startScores = _STARTSCORES;
    this.sequences = _SEQUENCES;
    this.gameModes = _GAMEMODES;
    this.difficulties = _DIFFICULTIES;

    const vm = this;

    vm.storage.GetItemFromStorage(vm.settings.key).then(
      (savedSettings) => {
        //There's a local item
        vm.settings = savedSettings.settings;
        vm.amountOfPlayers = savedSettings.amountOfPlayers;
        vm.amountOfTeams = savedSettings.amountOfTeams;
        vm.dartbotTeam = savedSettings.dartbotTeam;
        vm.teamMode = savedSettings.teamMode;
      },
      () => {
        vm.settings.teams = [];
        vm.dartbotTeam = <Team>{
          players: [{
            name: 'Dartbot',
            isCPU: true,
            cpuLevel: this.settings.dartbotDefault,
            Account: 0,
          }]
        };

        //Default
        vm.AddTeam();
        vm.AddTeam();

        if (!this.auth.guest) {
          //Check if already authenticated
          this.auth.checkAuthentication().then((account: any) => {
            vm.settings.teams[0].players[0].name = account.FirstName;
            vm.settings.teams[0].players[0].ProfilePic = account.ProfilePic;
            vm.settings.teams[0].players[0].Account = account.AccountID;
          });
        }
      });
  }

  ionViewWillEnter() {
    this.menu.enable(true, 'mainmenu');
    this.menu.enable(false, 'ingamemenu');

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  public AddTeam() {
    this.amountOfTeams++;

    const team = <Team>{
      players: <any>[]
    };

    this.settings.teams.push(team);

    this.AddPlayer(team);
  }

  public AddPlayer(team: Team) {
    // If teammode is OFF and there is already a player, we can't add a second one
    if (this.teamMode == false && team.players.length) {
      return;
    }

    this.amountOfPlayers++;

    if (!team.players) {
      team.players = [];
    }
    else if (team.players.length === 2) {
      return;
    }

    team.players.push({
      name: '',
      isCPU: false,
      Account: 0,
    });
  }

  public RemoveTeam(team: Team) {
    const index = this.settings.teams.indexOf(team);
    this.amountOfPlayers -= team.players.length;
    this.amountOfTeams--;
    this.settings.teams.splice(index, 1);
  }

  public RemovePlayer(team: Team, player) {
    const index = team.players.indexOf(player);
    if (team.players.splice(index, 1)) {
      this.amountOfPlayers--;
    }
  }

  RemoveItemFromStorageListByID(itemID) {
    const vm = this;

    vm.storage.RemoveItemFromStorageListByID(vm.settings.unfinishedKey, itemID);
  }

  public ContinueGame(game): void {
    const vm: SettingsView = this;

    if (vm.settings.selectedStartScore == 'Custom') {
      vm.settings.startScore = vm.settings.customScore;
    } else {
      vm.settings.startScore = vm.settings.selectedStartScore;
    }

    if (this.preferenceService.Preferences.allowCaller) {
      const sound = vm.settings.teams[0].players[0].sound;
      if (sound) {
        //Call the name
        vm.smartAudio.playAndObserve(sound).onpause = (
          () => {
            //Call 'to throw first'
            vm.smartAudio.playAndObserve('assets/sounds/texts/tothrowfirst.mp3').onpause = (
              () => {
                //Call 'Game on'
                vm.smartAudio.playHTML('assets/sounds/texts/gameon.mp3');
              });
          });
      }
      else {
        vm.smartAudio.playHTML('assets/sounds/texts/gameon.mp3');
      }
    }

    this.nav.push(vm.gameComponent,
      <any>{ game: game });
  }

  public TryStartGame(): void {
    const vm: SettingsView = this;

    let linked = false;
    vm.settings.teams.forEach(team => {
      team.players.forEach(player => {
        if (player.Account) {
          linked = true;
        }
      });
    });

    if (!linked) {
      const vm = this;

      const dialogRef = this.modal.create(LinkAccountWarningDialog);
      dialogRef.present();
      dialogRef.onDidDismiss(data => {
        if (data === true) {
          vm.StartGame();
        }
        else{
          this.ShowLogin();
        }
      });
    }
    else {
      vm.StartGame();
    }
  }

  ShowLogin(){
    const loginDialog = this.modal.create(LoginOrCreateDialog);
    loginDialog.present();
  }

  public StartGame(): void {
    const vm: SettingsView = this;

    if (vm.settings.selectedStartScore == 'Custom') {
      vm.settings.startScore = vm.settings.customScore;
    } else {
      vm.settings.startScore = vm.settings.selectedStartScore;
    }

    if (this.preferenceService.Preferences.allowCaller) {
      const sound = vm.settings.teams[0].players[0].sound;
      if (sound) {
        //Call the name
        vm.smartAudio.playAndObserve(sound).onpause = (
          () => {
            //Call 'to throw first'
            vm.smartAudio.playAndObserve('assets/sounds/texts/tothrowfirst.mp3').onpause = (
              () => {
                //Call 'Game on'
                vm.smartAudio.playHTML('assets/sounds/texts/gameon.mp3');
              });
          });
      }
      else {
        vm.smartAudio.playHTML('assets/sounds/texts/gameon.mp3');
      }
    }

    vm.settings.amountOfPlayers = this.amountOfPlayers;

    //DoubleCheck the DartBotTeam Average
    vm.settings.teams.forEach(function (team) {
      team.players.forEach(function (player) {
        if (player.isCPU) {
          player.cpuLevel = vm.dartbotTeam.players[0].cpuLevel;
        }
      });
    });

    vm.storage.PutItemInStorage(vm.settings.key, {
      settings: vm.settings,
      amountOfPlayers: vm.amountOfPlayers,
      amountOfTeams: vm.amountOfTeams,
      dartbotTeam: vm.dartbotTeam,
      teamMode: vm.teamMode
    });

    this.nav.push(vm.gameComponent,
      <any>{ settings: vm.settings });
  }

  public ToggleReorder() {
    this.allowReorder = !this.allowReorder;
  }

  ReorderTeams(indexes) {
    const element = this.settings.teams[indexes.from];
    this.settings.teams.splice(indexes.from, 1);
    this.settings.teams.splice(indexes.to, 0, element);
  }

  SetDartbot(vsCPU: boolean) {
    const vm = this;

    if (vsCPU) {
      if (!vm.teamMode && vm.amountOfTeams == 4) {
        const lastTeam = vm.settings.teams.pop();
        vm.tempPlayer = lastTeam.players[0];
        vm.settings.teams.push(vm.dartbotTeam);
      }
      else if (vm.teamMode && vm.amountOfTeams == 2) {
        if (vm.amountOfPlayers == 4) {
          const lastTeam = vm.settings.teams[vm.settings.teams.length - 1];
          vm.tempPlayer = lastTeam.players.pop();
          lastTeam.players.push(vm.dartbotTeam.players[0]);
        }
        else {
          let found = false;
          vm.settings.teams.forEach(function (team) {
            if (team.players.length < 2 && found == false) {
              team.players.push(vm.dartbotTeam.players[0]);
              found = true;
            }
          });
          vm.amountOfPlayers++;
        }
      }
      else {
        vm.settings.teams.push(vm.dartbotTeam);
        vm.amountOfPlayers++;
      }
    }
    //Not vs CPU
    else {
      if (!vm.teamMode) {
        vm.settings.teams.forEach(function (team, index) {
          if (team.players[0].isCPU) {
            vm.settings.teams.splice(index, 1);
            vm.amountOfPlayers--;
          }
        });
        if (vm.tempPlayer != null) {
          vm.settings.teams.push(<Team>{
            players: [vm.tempPlayer]
          });
        }
      }
      else {
        vm.settings.teams.forEach(function (team, index) {
          team.players.forEach(function (player, index2) {
            if (player.isCPU) {
              const spliced = team.players.splice(index2, 1);
              player = spliced[0];
              vm.amountOfPlayers--;

              if (vm.tempPlayer != null) {
                team.players.push(vm.tempPlayer);
                vm.tempPlayer = null;
                vm.amountOfPlayers++;
              }

              if (team.players.length == 0) {
                vm.settings.teams.splice(index, 1);
              }
            }
          });
        });
      }
    }

    vm.amountOfTeams = vm.settings.teams.length;
  }

  SetTeams(teamMode: boolean) {
    const vm = this;

    const newTeams = [];
    if (teamMode && vm.amountOfTeams > 2) {
      let newTeam = null;
      vm.settings.teams.forEach(function (team) {
        if (newTeam == null) {
          newTeam = team;
        }
        else {
          newTeam.players.push(team.players[0]);
          newTeams.push(newTeam);
          newTeam = null;
        }
      });
      //Don't skip the last team (when odd number)
      if (newTeam != null) {
        newTeams.push(newTeam);
      }
      vm.settings.teams = newTeams;
      vm.amountOfTeams = newTeams.length;
    }
    else if (!teamMode && vm.amountOfPlayers > vm.amountOfTeams) {
      vm.settings.teams.forEach(function (team) {
        team.players.forEach(function (player) {
          newTeams.push(<Team>{
            players: [player]
          });
        });
      });
      vm.settings.teams = newTeams;
      vm.amountOfTeams = newTeams.length;
    }

  }

  AddSound(_player, sound) {
    this.fileTransfer.download(sound).then(
      (fe: FileEntry) => {
        _player.sound = this.win.Ionic.WebView.convertFileSrc(fe.nativeURL);
      },
      err => {
        console.log(JSON.stringify(err));
      });
  }

  ShowLinkPlayer(_player) {
    const vm = this;

    if (vm.auth.guest) {
      const loginDialog = vm.modal.create(LoginRequiredDialog);
      loginDialog.present();
    }
    else {

      const linkPeopleDialog = this.modal.create(LinkPeopleDialog,
        {
          player: _player
        });
      linkPeopleDialog.onDidDismiss(linkedPlayer => {
        if (linkedPlayer != null) {
          _player.name = linkedPlayer.DisplayName;
          _player.ProfilePic = linkedPlayer.ProfilePic;
          _player.Account = linkedPlayer.AccountID;
          _player.isUltimate = linkedPlayer.isUltimate;
        }
      });

      linkPeopleDialog.present();
    }
  }

  ShowLinkSound(_player) {
    const vm = this;

    const linkSoundDialog = vm.modal.create(LinkSoundDialog,
      {
        player: _player
      });
    linkSoundDialog.onDidDismiss(sound => {
      if (sound != null) {
        vm.AddSound(_player, sound);
      }
      else if (sound != false) {
        _player.sound = null;
      }
    });

    linkSoundDialog.present();
  }

  UnlinkPlayer(_player) {
    _player.name = '';
    _player.Account = 0;
    _player.ProfilePic = null;
    _player.isUltimate = null;
  }
}
