import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Content, Platform, ModalController, PopoverController, ViewController, MenuController, ToastController, IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Settings } from '../../../models/settings';

import { SimpleStatisticsDialog, TutorialDialog, SwipeForMenu } from '../../../dialogs';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { IonNumericKeyboardOptions, IngameMenuComponent } from '../../../components';
import { SmartAudioProvider, PreferenceService, Auth, MobileApiService, StorageProvider } from '../../../providers';
 

const _IMPOSSIBLETHROWS = [163, 166, 169, 172, 173, 175, 176, 178, 179];

@IonicPage()
@Component({
  selector: 'page-score-training',
  templateUrl: 'score-training.html',
})
export class ScoreTrainingPage {
  @ViewChild(Content) content: Content;
  settings: Settings;
  storageKey: string = 'scores';
  ID: any = Date.now();

  title: string;
  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;
  score: any = '';
  players: any[] = [];
  currentPlayer: any;
  currentOrientation: string;

  throwsLeft: number;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modal: ModalController,
    public viewController: ViewController,
    public auth: Auth,
    public toast: ToastController,
    public mobileAPI: MobileApiService,
    public storage: StorageProvider,
    public platform: Platform,
    public chRef: ChangeDetectorRef,
    public screenOrientation: ScreenOrientation,
    public menu: MenuController,
    public inGameMenu: IngameMenuComponent,
    public smartAudio: SmartAudioProvider,
    public preferenceService: PreferenceService,
    public loading: LoadingController,
     ) {

    const vm = this;
    this.settings = navParams.get('settings');
    this.title = 'page scores';

    this.menu.enable(false, 'mainmenu');
    this.menu.enable(true, 'ingamemenu');

    vm.throwsLeft = this.settings.amountOfTurns;

    for (const team of this.settings.teams) {
      //Concat the players to the playerlist
      for (const player of team.players) {
        player.scores = [];
        player.total = 0;
        player.throws = 0;

        vm.players.push(player);
      }
    }

    this.NextPlayer(0);
  }

  ionViewWillEnter() {
    const vm = this;

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });

    vm.CheckOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        if (vm.screenOrientation.type.indexOf('portrait') >= 0) {
          vm.currentOrientation = 'portrait';
        }
        else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
          vm.currentOrientation = 'landscape';
        }

        setTimeout(() => {
          vm.CheckOrientation();
        }, 500);
      }
    );
  }

  CheckOrientation() {
    const vm = this;

    let totalHeight = this.platform.height();
    let totalWidth = this.platform.width();
    if (totalHeight < totalWidth) {
      const tempHeight = totalHeight;
      totalHeight = totalWidth;
      totalWidth = tempHeight;
    }

    if (vm.platform.is('core') || vm.platform.is('mobileweb') ||
      vm.screenOrientation.type.indexOf('portrait') >= 0) {
        vm.currentOrientation = 'portrait';
        if (document.querySelector('.playerssection')) {
        const playersHeight = document.querySelector('.playerssection').clientHeight;
        vm.preferenceService.Preferences.keySize = Math.floor((totalHeight - 115 - playersHeight) / 4);
      }
    }
    else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
      vm.currentOrientation = 'landscape';
      vm.preferenceService.Preferences.keySize = Math.floor((totalWidth - 115) / 4);
    }
    vm.chRef.detectChanges();    
  }

  SwipeForMenu() {
    const popover = this.popoverCtrl.create(SwipeForMenu, {}, {
      cssClass: 'swipePopover'
    });
    popover.present();
    this.storage.PutItemInStorage('swipeForMenu', true);

    setTimeout(() => {
      this.OpenInGameMenu();
    }, 2000);
  }

  OpenInGameMenu() {
    this.menu.open('ingamemenu');
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      rightControlKey: {
        type: 'icon',
        value: 'backspace'
      }
    };
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = this.score.substr(0, this.score.length - 1);
    }
    else if (event.source === 'NUMERIC_KEY') {
      const newScore = this.score + event.key;

      if (parseInt(newScore).toString().length <= 3) {
        this.score = parseInt(newScore).toString();
      }
    }
    this.chRef.detectChanges();  
  }

  SubmitScore(_score) {
    const vm = this;
    if (_score === '') { return; }

    _score = parseInt(_score);
    //Check if score is possible
    if ((_score <= 180 && _IMPOSSIBLETHROWS.indexOf(_score) < 0)
      && _score >= 0) {

      vm.currentPlayer.scores.push(_score);
      vm.currentPlayer.total += _score;
      vm.currentPlayer.throws++;

      if (vm.preferenceService.Preferences.allowCaller) {
        this.smartAudio.play(_score, 'assets/sounds/caller/' + _score + '.mp3');
      }

      //Next player for a throw
      vm.NextPlayer();
    }
    else {//Show score error
      vm.mobileAPI.translate.get('invalid score', { value: vm.score }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
      vm.score = '';

      return;
    }
  }

  UndoScore(): void {
    const vm = this;
    const currentPlayerIndex = vm.players.indexOf(vm.currentPlayer);

    if (!vm.players[0].scores.length) {
      return;
    }

    //It's the first player
    if (currentPlayerIndex == 0) {
      //Go to last player
      vm.throwsLeft++;
      vm.currentPlayer = vm.players[(vm.players.length - 1)];
    } else {
      //ID = nextplayer, ID-1 = currentPlayer, ID-2 = previousplayer
      vm.currentPlayer = vm.players[currentPlayerIndex - 1];
    }

    const lastScore = vm.currentPlayer.scores.pop();
    vm.currentPlayer.total -= lastScore;
    vm.currentPlayer.throws--;

  }

  NextPlayer(playerIndex: number = null) {
    const vm = this;

    if (playerIndex != null) {
      vm.currentPlayer = vm.players[playerIndex];
    } else {
      if (vm.players.length > 1) {
        const newIndex = vm.players.indexOf(vm.currentPlayer) + 1;
        //If we are at the last item of the array
        if (!vm.players[newIndex]) {
          vm.throwsLeft--;
          if (vm.throwsLeft == 0) {
            //Show end of match
            vm.TrySave();
          } else {
            //Go to first player
            vm.currentPlayer = vm.players[0];
          }
        } else {
          vm.currentPlayer = vm.players[newIndex];
        }
      } else {
        //There's only one player
        vm.throwsLeft--;
        if (vm.throwsLeft == 0) {
          //Show end of match
          vm.TrySave();
        }
      }
    }

    vm.score = '';
  }

  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  TrySave() {
    const vm = this;

    if (vm.auth.userData) {
      const gameData = {
        ID: vm.ID,
        singlePlayers: vm.players
      };

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Game: gameData
      }, 'complete', vm.storageKey).subscribe(result => {
        vm.ShowEnd();
        vm.mobileAPI.hideLoader();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.storageKey + 'unsaved').then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    /* set the custom stats */
    const totals = { title: 'total score', values: [] };
    const threedart = { title: '3-dart average', values: [] };
    const onedart = { title: '1-dart average', values: [] };

    for (const player of vm.players) {
      totals.values.push(player.total);
      threedart.values.push(player.total / player.throws);
      onedart.values.push(player.total / (player.throws * 3));
    }


    const statisticsModal = this.modal.create(SimpleStatisticsDialog,
      {
        ignoreTotal: true,
        statistics: [
          totals,
          threedart,
          onedart,
        ],
        players: vm.players,
      });
    statisticsModal.onDidDismiss(data => {
    });

    this.nav.pop();
    statisticsModal.present();
  }

  ShowHelp() {
    const vm = this;

    const tutorial = this.modal.create(TutorialDialog,
      {
        slides: [
          {
            title: vm.storageKey + ' slide1 title',
            description: vm.storageKey + ' slide1 content',
          }
        ]
      });
    tutorial.onDidDismiss(data => {
    });

    tutorial.present();
  }
}
