import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import {
  Content, Platform, ToastController, PopoverController, ViewController, MenuController,
  ModalController, IonicPage, NavController, NavParams, LoadingController
} from 'ionic-angular';
import { Settings } from '../../../models/settings';

import { MatchStatisticsDialog, ThrowoutDialog, SaveOrDeleteDialog, SwipeForMenu } from '../../../dialogs';
import { TranslateService } from '@ngx-translate/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { IonNumericKeyboardOptions, IngameMenuComponent } from '../../../components';
import { SmartAudioProvider, PreferenceService, Auth, MobileApiService, StorageProvider, FileTransferService } from '../../../providers';
import { FileEntry } from '@ionic-native/file';


const _THROWOUTS = {
  '2': { 'default': 'D1' }, '3': { 'default': '1, D1' }, '4': { 'default': 'D2' },
  '5': { 'default': '1, D2' }, '6': { 'default': 'D3' }, '7': { 'default': '3, D2' }, '8': { 'default': 'D4' },
  '9': { 'default': '1, D4' }, '10': { 'default': 'D5' }, '11': { 'default': '3, D4' }, '12': { 'default': 'D6' },
  '13': { 'default': '5, D4' }, '14': { 'default': 'D7' }, '15': { 'default': '7, D4' }, '16': { 'default': 'D8' },
  '17': { 'default': '1, D8' }, '18': { 'default': 'D9' }, '19': { 'default': '3, D8' }, '20': { 'default': 'D10' },
  '21': { 'default': '5, D8' }, '22': { 'default': 'D11' }, '23': { 'default': '7, D8' }, '24': { 'default': 'D12' }, '25': { 'default': '9, D8' },
  '26': { 'default': 'D13' }, '27': { 'default': '11, D8' }, '28': { 'default': 'D14' }, '29': { 'default': '13, D8' }, '30': { 'default': 'D15' },
  '31': { 'default': '15, D8' }, '32': { 'default': 'D16' }, '33': { 'default': '1, D16' }, '34': { 'default': 'D17' }, '35': { 'default': '3, D16' },
  '36': { 'default': 'D18' }, '37': { 'd18': '1, D18', 'default': '5, D16' }, '38': { 'default': 'D19' }, '39': { 'd18': '3, D18', 'default': '7, D16' },
  '40': { 'default': 'D20' }, '41': { 'd20': '1, D20', 'd18': '5, D18', 'default': '9, D16' }, '42': { 'd20': '2, D20', 'd18': '6, D18', 'default': '10, D16' },
  '43': { 'd20': '3, D20', 'd18': '7, D18', 'default': '11, D16' }, '44': { 'd20': '4, D20', 'd18': '8, D18', 'default': '12, D16' },
  '45': { 'd20': '5, D20', 'd18': '9, D18', 'default': '13, D16' }, '46': { 'd20': '6, D20', 'd18': '10, D18', 'default': '14, D16' },
  '47': { 'd20': '7, D20', 'd18': '11, D18', 'default': '15, D16' }, '48': { 'd20': '8, D20', 'd18': '12, D18', 'default': '16, D16' },
  '49': { 'd20': '9, D20', 'd18': '13, D18', 'default': '17, D16' }, '50': { 'd20': '10, D20', 'd18': '14, D18', 'default': '18, D16' },
  '51': { 'd20': '11, D20', 'd18': '15, D18', 'default': '19, D16' }, '52': { 'd20': '12, D20', 'd18': '16, D18', 'default': '20, D16' },
  '53': { 'd18': '17, D18', 'default': '13, D20' }, '54': { 'd18': '18, D18', 'default': '14, D20' }, '55': { 'd18': '19, D18', 'default': '15, D20' },
  '56': { 'd18': '20, D18', 'default': '16, D20' }, '57': { 'default': '17, D20' }, '58': { 'default': '18, D20' }, '59': { 'default': '19, D20' },
  '60': { 'default': '20, D20' }, '61': { 'default': '25, D18' }, '62': { 'd20': 'T14, D10', 'default': 'T10, D16' }, '63': { 'd18': 't9, D18', 'default': 'T13, D12' },
  '64': { 'default': 'T16, D8' }, '65': { 'd16': 'T11, D16', 'default': '25, D20' }, '66': { 'default': 'T10, D18' }, '67': { 'd20': 't9, D20', 'default': 'T17, D8' },
  '68': { 'd16': 'T12, D16', 'default': 'T20, D4' }, '69': { 'default': 'T15, D12' }, '70': { 'd20': 'T10, D20', 'default': 'T18, D8' },
  '71': { 'd20': 'T17, D10', 'default': 'T13, D16' }, '72': { 'default': 'T12, D18' }, '73': { 'default': 'T19, D8' }, '74': { 'default': 'T14, D16' },
  '75': { 'default': 'T17, D12' }, '76': { 'default': 'T20, D8' }, '77': { 'default': 'T19, D10' }, '78': { 'default': 'T18, D12' },
  '79': { 'd20': 'T13, D20', 'default': 'T19, D11' }, '80': { 'd16': 'T16, D16', 'default': 'T20, D10' }, '81': { 'd18': 'T15, D18', 'default': 'T19, D12' },
  '82': { 'd20': 'T14, D20', 'default': 'BULL, D16' }, '83': { 'default': 'T17, D16' }, '84': { 'd18': 'T16, D18', 'default': 'T20, D12' },
  '85': { 'default': 'T15, D20' }, '86': { 'default': 'T18, D16' }, '87': { 'default': 'T17, D18' }, '88': { 'd20': 'T16, D20', 'default': 'T20, D14' },
  '89': { 'default': 'T19, D16' }, '90': { 'd18': 'T18, D18', 'default': 'T20, D15' }, '91': { 'default': 'T17, D20' }, '92': { 'default': 'T20, D16' },
  '93': { 'default': 'T19, D18' }, '94': { 'default': 'T18, D20' }, '95': { 'default': 'T19, D19' }, '96': { 'default': 'T20, D18' }, '97': { 'default': 'T19, D20' },
  '98': { 'default': 'T20, D19' }, '99': { 'd20': 'T19, 2, D20', 'd18': 'T19, 6, D18', 'default': 'T19, 10, D16' }, '100': { 'default': 'T20, D20' },
  '101': { 'd20': 'T20, 1, D20', 'd18': 'T18, 5, D18', 'default': 'T20, 9, D16' }, '102': { 'd20': 'T20, 2, D20', 'd18': 'T20, 6, D18', 'default': 'T20, 10, D16' },
  '103': { 'd20': 'T20, 3, D20', 'default': 'T20, 11, D16' }, '104': { 'd20': 'T20, 4, D20', 'd18': '20, T16, D18', 'default': 'T20, 12, D16' },
  '105': { 'd20': 'T20, 5, D20', 'd18': 'T20, 9, D18', 'default': 'T20, 13, D16' }, '106': { 'd20': 'T20, 6, D20', 'd18': 'T20, 10, D18', 'default': 'T20, 14, D16' },
  '107': { 'd20': 'T20, 7, D20', 'd18': '17, T18, D18', 'default': 'T19, 18, D16' }, '108': { 'd20': 'T20, 8, D20', 'd18': 'T18, 18, D18', 'default': 'T19, 19, D16' },
  '109': { 'd20': 'T20, 9, D20', 'd18': '19, T18, D18', 'default': 'T19, 20, D16' }, '110': { 'd20': 'T20, 10, D20', 'd18': 'T18, 20, D18', 'default': 'T20, 18, D16' },
  '111': { 'd20': 'T19, 14, D20', 'd18': 'T19, 18, D18', 'default': 'T20, 19, D16' }, '112': { 'd20': 'T20, 12, D20', 'd18': 'T19, 19, D18', 'default': 'T20, 20, D16' },
  '113': { 'd20': 'T20, 13, D20', 'default': 'T19, 20, D18' }, '114': { 'd18': 'T20, 18, D18', 'default': 'T20, 14, D20' },
  '115': { 'd18': 'T20, 19, D18', 'default': 'T19, 18, D20' }, '116': { 'd18': 'T20, 20, D18', 'default': 'T20, 16, D20' }, '117': { 'default': 'T20, 17, D20' },
  '118': { 'd16': 'T18, T16, D8', 'default': 'T20, 18, D20' }, '119': { 'default': 'T19, T12, D13' }, '120': { 'default': 'T20, 20, D20' },
  '121': { 'default': 'T20, T11, D14' }, '122': { 'default': 'T18, T18, D7' }, '123': { 'default': 'T19, T16, D9' }, '124': { 'default': 'T20, T14, D11' },
  '125': { 'd16': 'T18, T13, D16', 'default': '25, T20, D20' }, '126': { 'default': 'T19, T19, D6' }, '127': { 'default': 'T20, T17, D8' },
  '128': { 'd20': 'T18, T18, D10', 'default': 'T18, T14, D16' }, '129': { 'default': 'T19, T20, D6' }, '130': { 'default': 'T20, T20, D5' },
  '131': { 'default': 'T20, T13, D16' }, '132': { 'd16': 'BULL, BULL, D16', 'default': 'BULL, T14, D20' }, '133': { 'd20': 'T20, T13, D20', 'default': 'T20, T19, D8' },
  '134': { 'd20': 'T18, T20, D10', 'default': 'T20, T14, D16' }, '135': { 'default': 'BULL, T15, D20' }, '136': { 'default': 'T20, T20, D8' },
  '137': { 'default': 'T20, T19, D10' }, '138': { 'default': 'T20, T18, D12' }, '139': { 'd16': 'T19, BULL, D16', 'default': 'T19, T14, D20' },
  '140': { 'd16': 'T20, T16, D16', 'default': 'T20, T20, D10' }, '141': { 'd18': 'T20, T15, D18', 'default': 'T20, T19, D12' },
  '142': { 'd16': 'T20, BULL, D16', 'default': 'T20, T14, D20' }, '143': { 'default': 'T20, T17, D16' }, '144': { 'd18': 'T18, T18, D18', 'default': 'T20, T20, D12' },
  '145': { 'default': 'T20, T15, D20' }, '146': { 'default': 'T20, T18, D16' }, '147': { 'default': 'T20, T17, D18' }, '148': { 'default': 'T20, T16, D20' },
  '149': { 'default': 'T20, T19, D16' }, '150': { 'd20': 'T20, BULL, D20', 'default': 'T20, T18, D18' }, '151': { 'default': 'T20, T17, D20' },
  '152': { 'default': 'T20, T20, D16' }, '153': { 'default': 'T20, T19, D18' }, '154': { 'default': 'T20, T18, D20' }, '155': { 'default': 'T20, T19, D19' },
  '156': { 'default': 'T20, T20, D18' }, '157': { 'default': 'T20, T19, D20' }, '158': { 'default': 'T20, T20, D19' }, '160': { 'default': 'T20, T20, D20' },
  '161': { 'default': 'T20, T17, BULL' }, '164': { 'default': 'T20, T18, BULL' }, '167': { 'default': 'T20, T19, BULL' }, '170': { 'default': 'T20, T20, BULL' }
};

const _LOGICTHROWS = [3, 5, 7, 9, 11, 21, 25, 26, 30, 40, 41, 43, 45, 55, 59, 60, 66, 68, 76, 78, 80, 81, 83, 85, 96, 97, 99, 100, 121, 123, 125, 137, 140, 174, 177, 180];
const _IMPOSSIBLETHROWS = [163, 166, 169, 172, 173, 175, 176, 178, 179];
const _IMPOSSIBLEOUTS = [159, 162, 163, 165, 166, 168, 169];
const _THREEDARTFINISH = [99, 102, 103, 105, 106, 108, 109];
const _TWODARTFINISH = [3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 42, 43, 44, 45, 46, 47, 48, 49];

//Config default settings
const _HIGH_THROWS = [
  { min: 120, max: 130, possibility: 35 },
  { min: 130, max: 140, possibility: 50 },
  { min: 160, max: 180, possibility: 15 }];

const _LOW_THROWS = [
  { min: 0, max: 10, possibility: 15 },
  { min: 10, max: 20, possibility: 35 },
  { min: 20, max: 30, possibility: 50 }];

const _MINDEVIATION = -5;
const _MAXDEVIATION = 5;

const _SCORINGS = [
  { min: 40, max: 59, amount: 0 },
  { min: 60, max: 79, amount: 0 },
  { min: 80, max: 99, amount: 0 },
  { min: 100, max: 119, amount: 0 },
  { min: 120, max: 139, amount: 0 },
  { min: 140, max: 159, amount: 0 },
  { min: 160, max: 179, amount: 0 },
  { min: 180, max: 180, amount: 0 }];

@IonicPage()
@Component({
  selector: 'page-match',
  templateUrl: 'match.html',

})
export class MatchPage {
  @ViewChild(Content) content: Content;
  settings: Settings;
  unfinishedKey: string;
  unsavedKey: string;

  gameState: {
    title?: string;
    ID?: number,
    finished?: boolean,
    vsCPU?: boolean,
    cpuLevel?: number,
    isBestOf?: boolean,
    isSets?: boolean,
    teammode?: boolean,
    checkoutRate?: boolean,
    goalAmount?: number,
    startScore?: number,
    legs?: any[],
    sets?: any[],
    legsThrown?: number,
    setsThrown?: number,
    legsThisSet?: number,
    subtexts?: number,
  };
  teams: any[] = [];

  //Will be calculated
  currentLeg: any;
  currentSet: any;
  currentPlayer: any;
  currentTeam: any;

  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;
  thinkingBot: any;
  score: any = '';
  currentOrientation: string;

  invalid: boolean = false;

  constructor(
    public nav: NavController,
    public popoverCtrl: PopoverController,
    public navParams: NavParams,
    public modal: ModalController,
    public smartAudio: SmartAudioProvider,
    public translate: TranslateService,
    public toast: ToastController,
    public menu: MenuController,
    public platform: Platform,
    public chRef: ChangeDetectorRef,
    public screenOrientation: ScreenOrientation,
    public inGameMenu: IngameMenuComponent,
    public viewController: ViewController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public storage: StorageProvider,
    public preferenceService: PreferenceService,
    public loading: LoadingController,
  ) {

    const vm = this;

    this.menu.enable(false, 'mainmenu');
    this.menu.enable(true, 'ingamemenu');

    vm.storage.GetItemFromStorage('swipeForMenu').then(
      (_res) => { },
      () => {
        vm.SwipeForMenu();
      });

    const storageItem = navParams.get('game');
    if (storageItem) {
      vm.teams = storageItem.Teams;
      vm.gameState = storageItem.Game;
      vm.currentSet = storageItem.currentSet;
      vm.currentLeg = vm.gameState.legs[vm.gameState.legs.length - 1];
      vm.unfinishedKey = storageItem.unfinishedKey;
      vm.unsavedKey = storageItem.unsavedKey;

      //Remove the temporary game
      vm.storage.RemoveItemFromStorageListByID(vm.unfinishedKey, vm.gameState.ID);
    }
    else {
      this.settings = navParams.get('settings');

      vm.unfinishedKey = this.settings.unfinishedKey;
      vm.unsavedKey = this.settings.unsavedKey;

      this.teams = this.settings.teams;
      this.currentLeg = {
        ID: 1,
        number: 1,
        setID: 1,
        scores: [],
        darts: []
      };
      this.currentSet = {
        ID: 1,
        number: 1,
        legsWon: []
      };

      this.gameState = {
        ID: Date.now(),
        finished: false,
        vsCPU: this.settings.vsCPU,
        isBestOf: this.settings.isBestOf == 'true',
        goalAmount: this.settings.goalAmount,
        isSets: this.settings.isSets == 'true',
        checkoutRate: this.settings.checkoutRate,
        startScore: parseInt(this.settings.startScore),
        teammode: vm.settings.amountOfPlayers > vm.teams.length,
        setsThrown: 0,
        legsThrown: 0,
        legsThisSet: 0,
        legs: [],
        sets: [],
        subtexts: 0,
      };

      vm.gameState.legs.push(vm.currentLeg);
      vm.gameState.sets.push(vm.currentSet);

      let teamID = 1;
      for (const team of this.teams) {
        //default Team values
        team.ID = teamID++;
        team.remaining = vm.settings.startScore;
        team.legsWon = 0;
        team.legDarts = 0;
        team.setsWon = 0;
        team.highestOut = 0;

        team.dartsThrown = 0;
        team.totalPoints = 0;
        team.first9Thrown = 0;
        team.first9Points = 0;

        team.lastScore = null;
        team.hasWon = false;
        team.avgDarts = null;
        team.bestLeg = null;
        team.worstLeg = null;
        team.defHighestScore = 0;
        team.highestScore = 0;

        team.subtext = vm.GetPossibleThrowout(team);
        team.scorings = [
          { min: 40, max: 59, amount: 0 },
          { min: 60, max: 79, amount: 0 },
          { min: 80, max: 99, amount: 0 },
          { min: 100, max: 119, amount: 0 },
          { min: 120, max: 139, amount: 0 },
          { min: 140, max: 159, amount: 0 },
          { min: 160, max: 179, amount: 0 },
          { min: 180, max: 180, amount: 0 }];

        //Concat the players to the playerlist
        let playerID = 1;
        for (const player of team.players) {
          player.ID = playerID++;
          if (player.isCPU) {
            vm.gameState.cpuLevel = player.cpuLevel;
            player.possibilities = vm.calcPossibilities(player.cpuLevel);
          }
        }
      }
    }

    vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, vm.currentLeg.scores.length);
    this.gameState.title = this.GetTitle();
  }

  public GetTitle(): string {
    if (this.gameState) {
      let title = this.gameState.isBestOf ? 'Best of ' : 'First to ';
      title += this.gameState.goalAmount;
      title += this.gameState.isSets ? ' sets' : ' legs';
      return title;
    }
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      rightControlKey: {
        type: 'icon',
        value: 'backspace'
      }
    };
  }

  SwipeForMenu() {
    const popover = this.popoverCtrl.create(SwipeForMenu, {}, {
      cssClass: 'swipePopover'
    });
    popover.present();
    this.storage.PutItemInStorage('swipeForMenu', true);

    setTimeout(() => {
      this.OpenInGameMenu();
    }, 2000);
  }

  OpenInGameMenu() {
    this.menu.open('ingamemenu');
  }

  ionViewWillEnter() {
    const vm = this;

    vm.gameState.subtexts = 0;
    for (const team of this.teams) {
      team.subtext = null;
      team.subtext = vm.GetPossibleThrowout(team);
    }

    vm.CheckOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        if (vm.screenOrientation.type.indexOf('portrait') >= 0) {
          vm.currentOrientation = 'portrait';
        }
        else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
          vm.currentOrientation = 'landscape';
        }

        setTimeout(() => {
          vm.CheckOrientation();
        }, 500);
      }
    );

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  CheckOrientation() {
    const vm = this;

    let totalHeight = this.platform.height();
    let totalWidth = this.platform.width();
    if (totalHeight < totalWidth) {
      const tempHeight = totalHeight;
      totalHeight = totalWidth;
      totalWidth = tempHeight;
    }

    if (vm.platform.is('core') || vm.platform.is('mobileweb') ||
      vm.screenOrientation.type.indexOf('portrait') >= 0) {
      vm.currentOrientation = 'portrait';
      if (document.querySelector('.playerssection')) {
        const playersHeight = document.querySelector('.playerssection').clientHeight;
        vm.preferenceService.Preferences.keySize = Math.floor((totalHeight - 115 - playersHeight) / 4);
      }
    }
    else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
      vm.currentOrientation = 'landscape';
      vm.preferenceService.Preferences.keySize = Math.floor((totalWidth - 115) / 4);
    }
    vm.chRef.detectChanges();
  }

  ionViewWillLeave() {
    const vm = this;

    if (!vm.gameState.finished) {

      const saveOrDeleteModal = this.modal.create(SaveOrDeleteDialog,
        {
          title: vm.gameState.title,
        });
      saveOrDeleteModal.onDidDismiss(data => {
        //Remove if person didn't want to save it
        if (!data) {
          vm.storage.RemoveItemFromStorageListByID(vm.unfinishedKey, vm.gameState.ID);
        }
        else {
          vm.SaveLocalGame();
        }
      });

      saveOrDeleteModal.present();
    }
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = this.score.substr(0, this.score.length - 1);
    }
    else if (event.source === 'NUMERIC_KEY') {
      const newScore = this.score + event.key;

      if (parseInt(newScore).toString().length <= 3) {
        this.score = parseInt(newScore).toString();
      }
    }
    this.chRef.detectChanges();
  }

  SubmitScore(_score) {
    const vm = this;
    if (_score === '') { return; }

    if (vm.currentLeg.scores == undefined) {
      vm.currentLeg.scores = [];
    }

    _score = parseInt(_score);

    //Check if score is possible
    if (_score < (vm.currentTeam.remaining - 1) &&
      (_score <= 180 && _IMPOSSIBLETHROWS.indexOf(_score) < 0)
      && _score >= 0) {

      //Check if it is the first throw this leg for this team
      if (!vm.currentLeg.darts[vm.currentTeam.ID]) {
        vm.currentLeg.darts[vm.currentTeam.ID] = 0;
      }

      if (_score > vm.currentTeam.highestScore || !vm.currentTeam.highestScore) {
        vm.currentTeam.highestScore = _score;
      }
      vm.currentLeg.darts[vm.currentTeam.ID] += 3;

      if (vm.currentLeg.darts[vm.currentTeam.ID] <= 9) {
        vm.currentTeam.first9Points += _score;
        vm.currentTeam.first9Thrown += 3;
      }
      vm.currentTeam.totalPoints += _score;
      vm.currentTeam.dartsThrown += 3;

      vm.currentTeam.scorings.forEach(function (statistic, index) {
        if (_score >= statistic.min) {
          if (_score <= statistic.max) {
            statistic.amount += 1;
          }
        } else {
          return false;
        }
      });

      if (vm.gameState.checkoutRate && (vm.currentTeam.remaining - _score <= 50 || _score == 0)) {
        vm.ShowCheckoutDialog(vm.currentTeam.remaining, _score);
      }
      else {
        vm.EndTurn(_score);
      }
    }
    //SPELER GOOIT LEG EN/OF SET UIT
    else if (_score == vm.currentTeam.remaining &&
      (vm.currentTeam.remaining <= 170 && _IMPOSSIBLEOUTS.indexOf(_score) < 0)) {
      vm.currentTeam.scorings.forEach(function (statistic, index) {
        if (_score >= statistic.min) {
          if (_score <= statistic.max) {
            statistic.amount += 1;
          }
        } else {
          return false;
        }
      });

      vm.ShowCheckoutDialog(vm.currentTeam.remaining, _score);

    } else {
      //Show score error
      vm.mobileAPI.translate.get('invalid score', { value: vm.score }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
      vm.score = '';

      return;
    }
  }

  EndTurn(_score, legDarts = 0) {
    const vm = this;

    vm.currentTeam.remaining -= _score;
    vm.currentTeam.subtext = vm.GetPossibleThrowout(vm.currentTeam);
    vm.currentTeam.lastScore = _score;

    vm.currentTeam.legDarts += legDarts;

    //Add score to list
    vm.currentLeg.scores.push({
      teamID: vm.currentTeam.ID,
      playerID: vm.currentPlayer.ID,
      remaining: vm.currentTeam.remaining,
      legDarts: legDarts,
      darts: 3,
      score: _score
    });

    if (vm.preferenceService.Preferences.allowCaller) {
      if (vm.gameState.vsCPU && vm.currentPlayer.isCPU == true) {
        const score = _score;
        if (vm.preferenceService.Preferences.callDartbot) {
          this.smartAudio.playHTML('assets/sounds/caller/' + score + '.mp3');
          //Next player for a throw
          vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, vm.currentLeg.scores.length);
        }
        else {
          //Next player for a throw
          vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, vm.currentLeg.scores.length);
        }
      } else {
        const score = _score;
        this.smartAudio.playHTML('assets/sounds/caller/' + _score + '.mp3');
        
        const dbthinking = score == 180 ? 3700 : 2500;
        //Next player for a throw
        vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, vm.currentLeg.scores.length, dbthinking);
      }
    }
    else {
      //Next player for a throw
      vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, vm.currentLeg.scores.length);
    }
  }

  ShowCheckoutDialog(remainingAmount, thrownScore) {
    const vm = this;

    let minDartsOut = 2;
    const newRemaining = (remainingAmount - thrownScore);

    //it's a possible throwout
    if (remainingAmount <= 170 && _IMPOSSIBLEOUTS.indexOf(remainingAmount) < 0) {
      //Can be finished in 3-darts or more
      if (_THREEDARTFINISH.indexOf(remainingAmount) > -1 || remainingAmount > 110) {
        minDartsOut = 3;
      }
      //Can be finished in one dart or more
      else if (_TWODARTFINISH.indexOf(remainingAmount) < 0 && remainingAmount <= 50) {
        minDartsOut = 1;
      }
    }
    else {
      vm.EndTurn(thrownScore);
      vm.score = '';
      return false;
    }

    if (vm.gameState.checkoutRate) {
      //Default = 1-dart finish, so max 3 darts on a double.
      let possibleDartsAtDouble = [0, 1, 2, 3];

      //No-score means -> could be a bust
      if (thrownScore == 0) {
        if (minDartsOut == 2) {
          possibleDartsAtDouble = [0, 1, 2];
        }
        else if (minDartsOut == 3) {
          possibleDartsAtDouble = [0, 1];
        }
      }
      //it's a throwout
      else if (newRemaining == 0) {
        possibleDartsAtDouble = [1, 2, 3];
        if (minDartsOut == 2) {
          possibleDartsAtDouble = [1, 2];
        }
        else if (minDartsOut == 3) {
          possibleDartsAtDouble = [1];
        }
      }
      //From 3-dart to 50 or lower => possibly 0 or 1 dart at double
      else if (minDartsOut == 3 && newRemaining <= 50) {
        possibleDartsAtDouble = [0, 1];
      }
      //From 2-dart to 50 or lower => possibly 0, 1 or 2 darts at double
      else if (minDartsOut == 2 && newRemaining <= 50) {
        possibleDartsAtDouble = [0, 1, 2];
      }
      vm.OpenCheckoutDialog(minDartsOut, possibleDartsAtDouble, newRemaining);
    }
    //No checkout-rate calculation needed
    else {
      vm.OpenCheckoutDialog(minDartsOut, [], newRemaining);
    }
  }

  UndoScore() {
    const vm = this;

    //There are no scores, back to previous leg if there is one!
    if (vm.currentLeg.scores.length < 1) {
      const index = vm.gameState.legs.indexOf(vm.currentLeg);
      if (index > 0) {
        vm.BackToLastLeg(index);
      }
    } else {
      const lastScoreIndex = (vm.currentLeg.scores.length - 1);
      const teamID = vm.currentLeg.scores[lastScoreIndex].teamID;
      const oldScore = vm.currentLeg.scores[lastScoreIndex];
      //Set currentPlayer and Team to the previous one

      vm.SetPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, lastScoreIndex);

      if (vm.currentLeg.darts[vm.currentTeam.ID] <= 9) {
        vm.currentTeam.first9Points -= parseInt(oldScore.score);
        vm.currentTeam.first9Thrown -= oldScore.darts;
      }
      vm.currentTeam.totalPoints -= parseInt(oldScore.score);
      vm.currentTeam.dartsThrown -= oldScore.darts;
      vm.currentTeam.legDarts -= oldScore.legDarts;

      //Add score to the remains of the team and set the lastScore
      vm.currentTeam.remaining += parseInt(vm.currentLeg.scores[lastScoreIndex].score);
      if (vm.currentLeg.scores[lastScoreIndex].darts != undefined) {
        vm.currentLeg.darts[vm.currentTeam.ID] -= oldScore.darts;
      } else {
        vm.currentLeg.darts[vm.currentTeam.ID] -= 3;
      }

      vm.currentTeam.scorings.forEach((statistic, index) => {
        if (oldScore.score >= statistic.min) {
          if (oldScore.score <= statistic.max) {
            statistic.amount -= 1;
          }
        }
      });

      //UPDATE THE LASTSCORE and HighestScore

      //Reset de hoogste score als dit gelijk was aan de hoogste
      //En als het natuurlijk niet al de hoogste bevestigde score was
      const lastScore = vm.currentLeg.scores.pop();
      vm.currentTeam.highestScore = vm.currentTeam.defHighestScore;
      let newLastScore = null;

      vm.currentLeg.scores.forEach(legScore => {
        if (legScore.teamID == vm.currentTeam.ID) {
          newLastScore = legScore.score;
          if (legScore.score >= vm.currentTeam.highestScore) {
            vm.currentTeam.highestScore = legScore.score;
          }
        }
      });

      //Remove score and calculate averages
      vm.currentTeam.lastScore = newLastScore;
      vm.currentTeam.subtext = vm.GetPossibleThrowout(vm.currentTeam);
      vm.score = '';

      if (vm.currentPlayer.isCPU) {
        if (vm.currentLeg.scores.length == 0) {
          vm.CalculateCPUThrow();
        }
        else {
          vm.UndoScore();
        }
      }
    }
  }

  BackToLastLeg(index) {
    const vm = this;

    let oldSet = false;
    vm.gameState.legs.splice(index, 1);
    vm.currentLeg = vm.gameState.legs[index - 1];
    vm.gameState.legsThrown--;

    //We went to a previous set
    if (vm.gameState.setsThrown == vm.currentLeg.setID) {
      oldSet = true;
      const setIndex = vm.gameState.sets.indexOf(vm.currentSet);
      vm.gameState.sets.splice(setIndex, 1);
      vm.currentSet = vm.gameState.sets[setIndex - 1];
      vm.gameState.setsThrown--;
    }

    for (const team of this.teams) {
      team.remaining = vm.gameState.startScore;
    }

    vm.currentLeg.scores.forEach(score => {
      vm.teams.forEach(team => {
        if (team.ID == score.teamID) {
          team.remaining -= score.score;
        }
      });
    });

    for (const team of vm.teams) {
      team.subtext = vm.GetPossibleThrowout(team);
    }

    vm.UndoScore();

    //Undo went to a previous set
    if (oldSet) {
      vm.currentTeam.setsWon--;
    }

    vm.currentSet.legsWon[vm.currentTeam.ID]--;
    vm.currentTeam.legsWon--;

    //Set the old highests back.
    if (vm.currentLeg.stats.length) {
      for (const team of vm.teams) {
        if (vm.currentLeg.stats[team.ID]) {
          team.bestLeg = vm.currentLeg.stats[team.ID].bestLeg;
          team.worstLeg = vm.currentLeg.stats[team.ID].worstLeg,
            team.highestOut = vm.currentLeg.stats[team.ID].highestOut;
          team.highestScore = vm.currentLeg.stats[team.ID].highestScore;
          team.avgDarts = vm.currentLeg.stats[team.ID].avgDarts,
            team.defHighestScore = vm.currentLeg.stats[team.ID].highestScore;

          vm.SetHighestScore(team.ID);
        }
      }
    }
    else {
      for (const team of vm.teams) {
        team.bestLeg = null;
        team.highestOut = null;
        team.highestScore = null;
        team.defHighestScore = 0;
        team.worstLeg = null,
          team.avgDarts = null,
          vm.SetHighestScore(team.ID);
      }
    }
  }

  SetHighestScore(teamID) {
    const vm = this;

    vm.currentLeg.scores.forEach(score => {
      vm.teams.forEach(team => {
        if (team.ID == score.teamID) {
          if (score.score > team.highestScore || !team.highestScore) {
            team.highestScore = score.score;
          }
        }
      });
    });
  }

  OpenCheckoutDialog(minDartsOut: number, possibleDartsAtDouble: number[], newRemaining: number) {
    const vm = this;

    let possibleDarts = [];

    //it's a throwout
    if (newRemaining == 0) {
      possibleDarts = [1, 2, 3];
      if (minDartsOut == 3) {
        //3-dart finish: 1 dart at double, 3 darts in this turn
        vm.AddThrowout(3, 1);
        vm.score = '';
        return false;
      }
      if (minDartsOut == 2) {
        possibleDarts = [2, 3];
      }
    }

    if (possibleDarts.length || possibleDartsAtDouble.length) {

      if (!vm.currentPlayer.isCPU) {
        //send throwout with amount of darts
        const throwoutModal = this.modal.create(ThrowoutDialog, {
          possibleDarts: possibleDarts, // can be an emtpy list when it's not a throwout
          possibleDartsAtDouble: possibleDartsAtDouble, // can be an emtpy list when checkoutRate is turned off
        });
        throwoutModal.onDidDismiss(data => {
          if (data != null) {
            if (data.selectedDart != null) {
              //Darts for the throwout
              vm.AddThrowout(parseInt(data.selectedDart), parseInt(data.selectedDartAtDouble));
            }
            else {
              //Darts for the checkoutRate
              vm.EndTurn(vm.score, parseInt(data.selectedDartAtDouble));
            }
          }
          vm.score = '';
        });
        throwoutModal.present();
      }
      else {
        //CPU
        vm.EndTurn(vm.score, possibleDartsAtDouble.pop());
      }
    }
  }

  AddThrowout(amountOfDarts, legDarts) {
    const vm = this;

    vm.score = parseInt(vm.score);

    let playerID = 0;
    if (vm.currentTeam.players.length < vm.currentPlayer.ID) {
      playerID = vm.currentTeam.players[0].ID;
    } else {
      playerID = vm.currentPlayer.ID;
    }

    vm.currentTeam.legDarts += legDarts;

    //Add score to list		
    vm.currentLeg.scores.push({
      teamID: vm.currentTeam.ID,
      playerID: playerID,
      throwOutDarts: amountOfDarts,
      legDarts: legDarts,
      darts: amountOfDarts,
      score: vm.score
    });

    if (!vm.currentLeg.darts[vm.currentTeam.ID]) {
      vm.currentLeg.darts[vm.currentTeam.ID] = 0;
    }

    vm.currentLeg.darts[vm.currentTeam.ID] += amountOfDarts;
    if (vm.currentTeam.highestOut < vm.score) {
      vm.currentTeam.highestOut = vm.score;
    }
    if (vm.score > vm.currentTeam.highestScore) {
      vm.currentTeam.highestScore = vm.score;
    }

    if (vm.currentLeg.darts[vm.currentTeam.ID] <= 9) {
      vm.currentTeam.first9Points += vm.score;
      vm.currentTeam.first9Thrown += amountOfDarts;
    }
    vm.currentTeam.totalPoints += vm.score;
    vm.currentTeam.dartsThrown += amountOfDarts;

    const dartsToWin = vm.currentLeg.darts[vm.currentTeam.ID];
    //Check if the average is the highest average so far
    if (dartsToWin < vm.currentTeam.bestLeg || vm.currentTeam.bestLeg == null) {
      vm.currentTeam.bestLeg = dartsToWin;
    }
    if (dartsToWin > vm.currentTeam.worstLeg || vm.currentTeam.worstLeg == null) {
      vm.currentTeam.worstLeg = dartsToWin;
    }

    vm.currentTeam.remaining = 0;
    vm.currentTeam.lastScore = vm.score;
    vm.score = null;

    if (vm.currentSet.legsWon[vm.currentTeam.ID] == undefined) {
      vm.currentSet.legsWon[vm.currentTeam.ID] = 0;
    }

    //Set a new leg as WON
    vm.teams.forEach((team) => {
      team.defHighestScore = team.highestScore;
    });

    vm.currentLeg.teamWonID = vm.currentTeam.ID;
    if (vm.currentTeam.players.length < vm.currentPlayer.ID) {
      vm.currentLeg.playerWonID = vm.currentTeam.players[0].ID;
    } else {
      vm.currentLeg.playerWonID = vm.currentPlayer.ID;
    }

    vm.currentSet.legsWon[vm.currentTeam.ID]++;
    vm.currentTeam.legsWon++;
    vm.gameState.legsThrown++;

    if (vm.currentTeam.legsWon == 1) {
      vm.currentTeam.avgDarts = vm.currentLeg.darts[vm.currentTeam.ID];
    } else {
      const darts = (vm.currentTeam.avgDarts * (vm.currentTeam.legsWon - 1)) + vm.currentLeg.darts[vm.currentTeam.ID];
      vm.currentTeam.avgDarts = darts / vm.currentTeam.legsWon;
    }

    //BestOf or FirstTo x Sets
    if (vm.gameState.isSets) {

      //Team won a set
      if (vm.currentSet.legsWon[vm.currentTeam.ID] == 3) {
        vm.currentTeam.setsWon += 1;
        vm.gameState.legsThrown++;
        vm.gameState.setsThrown++;

        //The goal amount is an even number so it can be a draw
        if (vm.gameState.goalAmount % 2 == 0 && (
          (vm.gameState.isBestOf && vm.currentTeam.setsWon > (vm.gameState.goalAmount / 2)) ||
          (vm.gameState.isBestOf && vm.gameState.setsThrown == vm.gameState.goalAmount) ||
          (!vm.gameState.isBestOf && vm.currentTeam.setsWon == vm.gameState.goalAmount))
        ) {
          if (vm.preferenceService.Preferences.allowCaller) {
            console.log(vm.currentPlayer.sound);
            if (vm.currentPlayer.sound) {
              vm.smartAudio.playAndObserve('assets/sounds/texts/gameshotandthematch.mp3').onpause = (
                () => {
                  this.smartAudio.playHTML(vm.currentPlayer.sound);
                });
            }
            else {
              this.smartAudio.playHTML('assets/sounds/texts/gameshotandthematch.mp3');
            }
          }

          vm.TrySave();
        }
        else if (vm.gameState.goalAmount % 2 != 0 && (vm.gameState.isBestOf && vm.currentTeam.setsWon == Math.ceil(vm.gameState.goalAmount / 2)) ||
          (!vm.gameState.isBestOf && vm.currentTeam.setsWon == vm.gameState.goalAmount)
        ) {
          if (vm.preferenceService.Preferences.allowCaller) {
            console.log(vm.currentPlayer.sound);
            if (vm.currentPlayer.sound) {
              vm.smartAudio.playAndObserve('assets/sounds/texts/gameshotandthematch.mp3').onpause = (
                () => {
                  this.smartAudio.playHTML(vm.currentPlayer.sound);
                });
            }
            else {
              this.smartAudio.play('gameshotmatch', 'assets/sounds/texts/gameshotandthematch.mp3');
            }
          }

          vm.TrySave();
        } else {
          vm.gameState.legsThisSet = 0;
          if (vm.preferenceService.Preferences.allowCaller) {
            this.smartAudio.play('set' + vm.gameState.setsThrown, 'assets/sounds/gameshot/sets/' + vm.gameState.setsThrown + '.mp3');
          }

          vm.StartNewSet();
        }
      } else {
        vm.gameState.legsThisSet++;
        if (vm.preferenceService.Preferences.allowCaller) {
          this.smartAudio.play('leg' + vm.gameState.legsThisSet, 'assets/sounds/gameshot/legs/' + vm.gameState.legsThisSet + '.mp3');
        }

        vm.StartNewLeg();
      }
    } else {

      //BestOf or FirstTo x legs
      if (vm.gameState.goalAmount % 2 == 0 && (
        (vm.gameState.isBestOf && vm.currentSet.legsWon[vm.currentTeam.ID] > (vm.gameState.goalAmount / 2)) ||
        (vm.gameState.isBestOf && vm.gameState.legsThrown == vm.gameState.goalAmount) ||
        (!vm.gameState.isBestOf && vm.currentTeam.setsWon == vm.gameState.goalAmount))
      ) {
        if (vm.preferenceService.Preferences.allowCaller) {
          this.smartAudio.play('gameshotmatch', 'assets/sounds/texts/gameshotandthematch.mp3');
        }

        vm.TrySave();
      }
      else if (vm.gameState.goalAmount % 2 != 0 && (vm.gameState.isBestOf && Math.ceil(vm.gameState.goalAmount / 2) == vm.currentSet.legsWon[vm.currentTeam.ID]) ||
        (!vm.gameState.isBestOf && vm.currentSet.legsWon[vm.currentTeam.ID] == vm.gameState.goalAmount)
      ) {

        if (vm.preferenceService.Preferences.allowCaller) {
          this.smartAudio.play('gameshotmatch', 'assets/sounds/texts/gameshotandthematch.mp3');
        }

        vm.TrySave();
      } else {
        if (vm.preferenceService.Preferences.allowCaller) {
          this.smartAudio.play('leg' + vm.gameState.legsThrown, 'assets/sounds/gameshot/legs/' + vm.gameState.legsThrown + '.mp3');
        }

        vm.StartNewLeg();
      }
    }
  }

  StartNewSet() {
    const vm = this;
    if (vm.gameState.sets.length > 0) {
      const id = ((vm.currentSet.ID + 1));
      const newSet = {
        ID: id,
        number: id,
        legsWon: []
      };
      vm.currentSet = newSet;
    }
    vm.StartNewLeg();
  }

  StartNewLeg() {
    const vm = this;

    if (vm.gameState.legs.length > 0) {
      const id = (vm.gameState.legs.length + 1);
      const newLeg = {
        ID: id,
        number: id,
        setID: vm.currentSet.ID,
        scores: [],
        darts: []
      };
      vm.currentLeg = newLeg;
      vm.gameState.legs.push(newLeg);
    }

    vm.gameState.subtexts = 0;
    for (const team of this.teams) {
      team.remaining = vm.gameState.startScore;
      team.subtext = null;
      team.subtext = vm.GetPossibleThrowout(team);
    }

    vm.SaveLocalGame();

    //Next player for a throw
    vm.NextPlayer(vm.gameState.setsThrown, vm.gameState.legsThrown, 0);
  }

  SaveLocalGame() {
    const vm = this;

    const data = {
      Game: vm.gameState,
      Teams: vm.teams,
      currentSet: vm.currentSet,
      currentLeg: vm.currentLeg,
      unfinishedKey: vm.unfinishedKey,
      unsavedKey: vm.unsavedKey
    };
    vm.storage.PutItemInStorageByID(vm.unfinishedKey, data, data.Game.ID);
  }

  ShowStatistics() {
    const vm = this;

    let scoreStatisticIndex = 0;
    const scorings = [];
    while (scoreStatisticIndex < _SCORINGS.length) {
      const scoring = {
        title: (_SCORINGS[scoreStatisticIndex].min == _SCORINGS[scoreStatisticIndex].max) ? _SCORINGS[scoreStatisticIndex].min : _SCORINGS[scoreStatisticIndex].min + '+',
        values: []
      };
      for (const team of vm.teams) {
        team.total = team.legsWon;
        if (vm.gameState.isSets) {
          team.total = team.setsWon;
        }
        scoring.values.push(team.scorings[scoreStatisticIndex].amount);
      }

      scorings.push(scoring);
      scoreStatisticIndex++;
    }

    const statisticsModal = this.modal.create(MatchStatisticsDialog,
      {
        title: vm.gameState.title,
        checkoutRate: vm.gameState.checkoutRate,
        teams: vm.teams,
        scorings: scorings
      });
    statisticsModal.onDidDismiss(data => {
      //console.log(data);
    });

    statisticsModal.present();
  }

  SetPlayer(setsThrown: number, legsThrown: number, scoresThisLeg: number) {
    const vm = this;

    //sets fix
    if (setsThrown > 0) {
      legsThrown = this.gameState.legsThisSet;
    }

    const offset = setsThrown + legsThrown + scoresThisLeg;
    const teamIndex = offset % vm.teams.length;
    //Set the currentTeam
    vm.currentTeam = vm.teams[teamIndex];

    let playerIndex = 0;
    if (vm.gameState.teammode) {
      const roundsThrown = Math.floor(offset / vm.teams.length);
      playerIndex = (roundsThrown + legsThrown) % vm.currentTeam.players.length;
    }

    //Set the currentPlayer
    vm.currentPlayer = vm.currentTeam.players[playerIndex];
  }

  NextPlayer(setsThrown: number, legsThrown: number, scoresThisLeg: number, dbtimeout: number = 2500) {
    const vm = this;

    vm.SetPlayer(setsThrown, legsThrown, scoresThisLeg);

    const require = JSON.parse(JSON.stringify(vm.currentTeam.remaining));

    if (vm.preferenceService.Preferences &&
      vm.preferenceService.Preferences.allowCaller &&
      vm.preferenceService.Preferences.callYouRequire &&
      !vm.currentPlayer.isCPU &&
      vm.currentTeam.remaining <= 170 &&
      _IMPOSSIBLEOUTS.indexOf(vm.currentTeam.remaining) < 0) {

      let timeout = 2500;
      if (!vm.currentLeg.darts.length && vm.currentPlayer.sound) {
        timeout += 1200;
      }
      else if (vm.smartAudio.audioAsset && !vm.smartAudio.audioAsset.paused) {
        vm.smartAudio.audioAsset.onpause = (
          () => {
            if (vm.currentPlayer.sound) {
              vm.smartAudio.playAndObserve(vm.currentPlayer.sound).onpause = (
                () => {
                  vm.smartAudio.play('require' + require, 'assets/sounds/yourequire/' + require + '.mp3');
                });
            }
            else {
              vm.smartAudio.play('require' + require, 'assets/sounds/yourequire/' + require + '.mp3');
            }
          });
      }
      else {
        setTimeout(() => {
          if (vm.preferenceService.Preferences &&
            vm.preferenceService.Preferences.allowCaller &&
            vm.preferenceService.Preferences.callYouRequire &&
            !vm.currentPlayer.isCPU &&
            vm.currentTeam.remaining <= 170 &&
            _IMPOSSIBLEOUTS.indexOf(vm.currentTeam.remaining) < 0) {

            if (vm.currentPlayer.sound) {
              vm.smartAudio.playAndObserve(vm.currentPlayer.sound).onpause = (
                () => {
                  this.smartAudio.play('require' + require, 'assets/sounds/yourequire/' + require + '.mp3');
                });
            }
            else {
              this.smartAudio.play('require' + require, 'assets/sounds/yourequire/' + require + '.mp3');
            }
          }
        }, timeout);
      }
    }

    if (vm.currentPlayer.isCPU &&
      vm.preferenceService.Preferences.allowCaller &&
      vm.preferenceService.Preferences.callDartbot) {

      vm.DartbotIsThinking(dbtimeout);
      this.thinkingBot.onDidDismiss(() => {
        const loader = this.loading.create({
          showBackdrop: false,
          duration: 1000
        });

        loader.present();
        //Dartbot is about to throw now
        vm.CalculateCPUThrow();
      });
    } else if (vm.currentPlayer.isCPU) {
      setTimeout(() => {
        vm.CalculateCPUThrow();
      }, 100);
    }
    //vm.setUnfinished();

    //Reset the score
    this.score = '';
  }

  DartbotIsThinking(_duration: number = null) {
    this.thinkingBot = this.loading.create({
      spinner: 'hide',
      content: `
        <div class="cssload-loader-walk">
          <div></div><div></div><div></div><div></div><div></div>
        </div>`,
      duration: _duration
    });

    this.thinkingBot.present();
  }

  CalculateCPUThrow() {
    const vm = this;

    let amount = 0;
    let darts = 3;
    if (vm.currentTeam.remaining <= 170 && _IMPOSSIBLEOUTS.indexOf(vm.currentTeam.remaining) < 0) {
      amount = vm.GetRandomThrowOut();
    } else {
      amount = vm.GetRandomScore(false);
    }

    //Let's see if the bot used less than 3 darts!
    if (vm.currentTeam.remaining == amount && amount < 110 && _THREEDARTFINISH.indexOf(amount) < 0) {
      let minCount = 2;
      if (_TWODARTFINISH.indexOf(amount) < 0 && amount <= 50) {
        minCount = 1;
      }
      //Botupdate
      darts = vm.GetAmountOfDarts(minCount, vm.currentLeg.darts[vm.currentTeam.ID]);
    }

    vm.score = amount;
    if (vm.currentTeam.remaining == amount) {
      vm.AddThrowout(darts, darts);
    } else {
      vm.SubmitScore(amount);
    }
  }

  /**
   * Calculates if the dartbot went out in less than 3 darts (for it's average)
   * 
   * @param {number} min 
   * @param {number} currentDarts 
   * @returns {number} 
   * @memberof Match
   */
  GetAmountOfDarts(min: number, currentDarts: number): number {
    const vm = this;

    let count = 0;
    let diffFromWanted = null;
    for (let i = min; i <= 3; i++) {
      const newAverage = Math.round(vm.gameState.startScore / (currentDarts + i) * 300) / 100;
      let diff = (vm.currentPlayer.cpuLevel - newAverage);
      if (diff < 0) {
        diff = diff * -1;
      }
      if (diffFromWanted == null || diff < diffFromWanted) {
        diffFromWanted = diff;
        count = i;
      }
    }
    return count;
  }

  GetRandomThrowOut(): number {
    const vm = this;

    const cpuTeamAverage = (vm.currentTeam.totalPoints / vm.currentTeam.dartsThrown) * 3;
    const deviation = ((cpuTeamAverage / vm.currentPlayer.cpuLevel) * 100) - 100;
    const random = vm.rnd() + 1;
    const percentage = (100 * random) / 2;
    let returnValue;
    const newAverage = Math.round(vm.gameState.startScore / (vm.currentTeam.dartsThrown + 3) * 300) / 100;
    const newDeviation = ((newAverage / vm.currentPlayer.cpuLevel) * 100) - 100;
    //Average is too low
    if (deviation < _MINDEVIATION) {
      if (vm.currentTeam.remaining > 100) {
        if (percentage <= 25) {
          //If the average will be too high after throwOut, and no 170's please!
          if (newDeviation > _MAXDEVIATION || vm.currentTeam.remaining == 170) {
            returnValue = vm.GetRandomScore(true);
          }
          //Average is acceptable so throw out
          else {
            returnValue = vm.currentTeam.remaining;
          }
        }
        //Whether the average is acceptable or not, he's not throwing out
        else {
          returnValue = vm.GetRandomScore(true);
        }
        while (_LOGICTHROWS.indexOf(returnValue) < 0) {
          returnValue++;
        }
      } else if (vm.currentTeam.remaining >= 50 && vm.currentTeam.remaining <= 100) {
        //throw it out!
        if (percentage <= 50) {
          returnValue = vm.currentTeam.remaining;
        }
        //throw a number
        else {
          returnValue = vm.GetRandomScore(true);
        }
      } else if (vm.currentTeam.remaining < 50) {
        //throw it out!
        if (percentage <= 80) {
          returnValue = vm.currentTeam.remaining;
        }
        //throw a number
        else {
          returnValue = vm.GetRandomScore(true);
        }
      }
    }
    //Average is too high
    else if (deviation > _MAXDEVIATION) {
      if (vm.currentTeam.remaining > 50) {
        if (newDeviation < _MAXDEVIATION && newDeviation > _MINDEVIATION) {
          returnValue = vm.currentTeam.remaining;
        } else {
          //Just throw in a random number
          returnValue = vm.GetRandomScore(true);
        }
      } else if (vm.currentTeam.remaining < 50) {
        if (newDeviation < _MAXDEVIATION && newDeviation > _MINDEVIATION) {
          returnValue = vm.currentTeam.remaining;
        } else if (newDeviation > _MAXDEVIATION || percentage <= 60) {
          returnValue = vm.GetRandomScore(true);
        }
        //throw a miss (no score)
        else {
          returnValue = 0;
        }
      }
    }
    //Average is okay so far
    else {
      if (vm.IsInt(deviation)) {
        //If the average will be too high after throwOut
        if (newDeviation > _MAXDEVIATION) {
          //Throw a random number
          returnValue = vm.GetRandomScore(true);
        }
        //Average is acceptable so 70% chance throw out
        else {
          if (vm.currentTeam.remaining != 170 && (percentage <= 70 || (newDeviation > _MINDEVIATION && newDeviation < _MAXDEVIATION))) {
            returnValue = vm.currentTeam.remaining;
          }
          //Whether the average is acceptable or not, he's not throwing out
          else {
            returnValue = vm.GetRandomScore(true);
          }
        }
      } else if (vm.currentPlayer.cpuLevel > vm.currentTeam.remaining) {
        //throw it out
        returnValue = vm.currentTeam.remaining;
      } else {
        //don't throw it out
        returnValue = vm.GetRandomScore(true);
      }
    }

    if (vm.currentTeam.remaining != returnValue) {
      while (returnValue > (vm.currentTeam.remaining - 2)) {
        returnValue = Math.floor(returnValue / 2);
      }

      //Always have an opportunity to one-dart finish after 40 or lower
      if (vm.currentTeam.remaining - returnValue <= 40) {
        if (_TWODARTFINISH.indexOf(vm.currentTeam.remaining - returnValue) > -1) {
          returnValue--;
        }
      }
    }

    return returnValue;
  }

  GetRandomScore(rand: boolean): number {
    const vm = this;

    const random = vm.rnd() + 1;
    const percentage = (100 * random) / 2;
    let returnValue: number;

    if (rand == true || percentage <= (vm.currentPlayer.possibilities.low + vm.currentPlayer.possibilities.rand)) {
      returnValue = Math.round(vm.currentPlayer.cpuLevel * random);
    } else if (percentage <= vm.currentPlayer.possibilities.low) {
      returnValue = vm.GetScore(false);
    } else {
      returnValue = vm.GetScore(true);
    }

    while (returnValue > (vm.currentTeam.remaining - 2)) {
      returnValue = Math.floor(returnValue / 2);
    }
    while (_LOGICTHROWS.indexOf(returnValue) < 0 && vm.currentTeam.remaining > 100) {
      returnValue++;
    }
    return returnValue;
  }

  GetScore(isHigh: boolean): number {
    const vm = this;

    const random = vm.rnd() + 1;
    const percentage = (100 * random) / 2;
    //Pick the high or low numbers
    let getFrom = _LOW_THROWS;
    if (isHigh) {
      getFrom = _HIGH_THROWS;
    }
    let range = null;
    if (percentage <= getFrom[0].possibility) {
      range = getFrom[0];
    } else if (percentage <= (getFrom[0].possibility + getFrom[1].possibility)) {
      range = getFrom[1];
    } else {
      range = getFrom[2];
    }
    //Gegooide aantal kiezen 
    let amount = Math.floor(Math.random() * (range.max - range.min)) + range.min;
    //Als het getal niet mogelijk is om te gooien
    while (_IMPOSSIBLETHROWS.indexOf(amount) >= 0) {
      amount = Math.floor(amount / 2);
    }
    while (amount > (vm.currentTeam.remaining - 2)) {
      amount = Math.floor(amount / 2);
    }
    while (_LOGICTHROWS.indexOf(amount) < 0 && vm.currentTeam.remaining > 100) {
      amount++;
    }
    return amount;
  }

  rnd(): number {
    return ((Math.random() + Math.random() + Math.random() + Math.random() + Math.random() + Math.random()) - 3) / 3;
  }

  calcPossibilities(x: number) {
    const diff = x - 40;
    return {
      low: 40 - (diff / 4),
      rand: 55 - (diff / 3),
      high: 5 + (diff / 1.5)
    };
  }

  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  GetPossibleThrowout(team: any): string {
    let returnString = null;

    if (_THROWOUTS[team.remaining] != null && _THROWOUTS[team.remaining]['default'] != null) {
      returnString = _THROWOUTS[team.remaining]['default'];
    }
    else if (_IMPOSSIBLEOUTS.indexOf(team.remaining) >= 0) {
      this.translate.get('no possible out', {}).subscribe((res: string) => {
        returnString = (res);
        //=> 'Translated String if found'
      });
    }
    if (returnString == null && team.subtext != null) {
      this.gameState.subtexts--;
    }
    else if (team.subtext == null && returnString != null) {
      this.gameState.subtexts++;
    }

    return returnString;
  }

  TrySave() {
    const vm = this;

    vm.storage.RemoveItemFromStorageListByID(vm.unfinishedKey, vm.gameState.ID);

    if (vm.currentTeam.legsWon == 1) {
      vm.currentTeam.avgDarts = vm.currentLeg.darts[vm.currentTeam.ID];
    } else {
      const darts = (vm.currentTeam.avgDarts * (vm.currentTeam.legsWon - 1)) + vm.currentLeg.darts[vm.currentTeam.ID];
      vm.currentTeam.avgDarts = darts / vm.currentTeam.legsWon;
    }

    if (vm.auth.userData) {
      vm.currentTeam.hasWon = true;

      if (vm.gameState.checkoutRate) {
        vm.teams.forEach((team) => {
          team.checkoutRate = (team.legsWon / team.legDarts) * 100;
        });
      }

      const gameData = vm.gameState;
      gameData.finished = true;

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Teams: vm.teams,
        Game: gameData
      }, 'complete', 'match').subscribe(result => {
        vm.mobileAPI.hideLoader();
        vm.ShowEnd();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.unsavedKey).then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.unsavedKey, items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.unsavedKey, [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    vm.gameState.finished = true;
    this.nav.pop();
    vm.ShowStatistics();
  }

}
