import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Content, Platform, ModalController, PopoverController, IonicPage, ViewController, NavController, ToastController, NavParams, LoadingController, MenuController } from 'ionic-angular';
import { Settings } from '../../../models/settings';

import { SimpleStatisticsDialog, TutorialDialog, SwipeForMenu } from '../../../dialogs';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { IonNumericKeyboardOptions, IngameMenuComponent } from '../../../components';
import { Auth, MobileApiService, StorageProvider, PreferenceService } from '../../../providers';
 

@IonicPage()
@Component({
  selector: 'page-singles-training',
  templateUrl: 'singles-training.html',
})
export class SinglesTrainingPage {
  @ViewChild(Content) content: Content;
  settings: Settings;
  storageKey: string = 'singles';
  ID: any = Date.now();

  title: string;
  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;
  score: any = '';
  players: any[] = [];
  currentPlayer: any;

  finishedPeople: number = 0;
  dartboardValues: any[];
  entryIndex: number = 0;
  currentEntry: any;
  currentOrientation: any;
  
  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController, 
    public modal: ModalController,
    public menu: MenuController,
    public inGameMenu: IngameMenuComponent,    
    public auth: Auth,
    public platform: Platform,
    public chRef: ChangeDetectorRef,
    public screenOrientation: ScreenOrientation,
    public viewController: ViewController,
    public toast: ToastController,
    public mobileAPI: MobileApiService,
    public preferenceService: PreferenceService,
    public storage: StorageProvider,
    public loading: LoadingController,
     ) {

    const vm = this;
    this.settings = navParams.get('settings');
    this.title = 'page singles';

    this.menu.enable(false, 'mainmenu');
    this.menu.enable(true, 'ingamemenu');

    for (const team of this.settings.teams) {
      //Concat the players to the playerlist
      for (const player of team.players) {
        player.scores = [];
        player.hits = 0;

        vm.players.push(player);
      }
    }
    this.dartboardValues = [
      { val: 1, title: 1 },
      { val: 2, title: 2 },
      { val: 3, title: 3 },
      { val: 4, title: 4 },
      { val: 5, title: 5 },
      { val: 6, title: 6 },
      { val: 7, title: 7 },
      { val: 8, title: 8 },
      { val: 9, title: 9 },
      { val: 10, title: 10 },
      { val: 11, title: 11 },
      { val: 12, title: 12 },
      { val: 13, title: 13 },
      { val: 14, title: 14 },
      { val: 15, title: 15 },
      { val: 16, title: 16 },
      { val: 17, title: 17 },
      { val: 18, title: 18 },
      { val: 19, title: 19 },
      { val: 20, title: 20 },
      { val: 25, title: 'BULL' }
    ];
    if (this.settings.sequence == 'desc') {
      this.dartboardValues.reverse();
    }
    else if (this.settings.sequence == 'random') {
      this.dartboardValues = this.ShuffleArray(this.dartboardValues);
    }

    this.NextValue(0);
    this.NextPlayer(0);
  }

  ionViewWillEnter() {
    const vm = this;

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });

    vm.CheckOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        if (vm.screenOrientation.type.indexOf('portrait') >= 0) {
          vm.currentOrientation = 'portrait';
        }
        else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
          vm.currentOrientation = 'landscape';
        }

        setTimeout(() => {
          vm.CheckOrientation();
        }, 500);
      }
    );
  }

  CheckOrientation() {
    const vm = this;

    let totalHeight = this.platform.height();
    let totalWidth = this.platform.width();
    if (totalHeight < totalWidth) {
      const tempHeight = totalHeight;
      totalHeight = totalWidth;
      totalWidth = tempHeight;
    }

    if (vm.platform.is('core') || vm.platform.is('mobileweb') ||
      vm.screenOrientation.type.indexOf('portrait') >= 0) {
        vm.currentOrientation = 'portrait';
        if (document.querySelector('.playerssection')) {
        const playersHeight = document.querySelector('.playerssection').clientHeight;
        vm.preferenceService.Preferences.keySize = Math.floor((totalHeight - 115 - playersHeight) / 4);
      }
    }
    else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
      vm.currentOrientation = 'landscape';
      vm.preferenceService.Preferences.keySize = Math.floor((totalWidth - 115) / 4);
    }
    vm.chRef.detectChanges();    
  }

  // -> Fisher–Yates shuffle algorithm
  ShuffleArray(array) {
    let m = array.length, t, i;

    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }

    return array;
  }

  SwipeForMenu() {
    const popover = this.popoverCtrl.create(SwipeForMenu, {}, {
      cssClass: 'swipePopover'
    });
    popover.present();
    this.storage.PutItemInStorage('swipeForMenu', true);
    
    setTimeout(() => {
      this.OpenInGameMenu();
    }, 2000);
  }

  OpenInGameMenu() {
    this.menu.open('ingamemenu');
  }

  Shuffle(array: any[]): any[] {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      hideBottom: true,
      showExtraButton: true,
      ExtraButtonText: 'miss',
    };
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = '';
    }
    else if (event.source === 'NUMERIC_KEY') {
      //Score can be only a single digit with SinglesTraining (0 - 9)
      this.score = event.key;
    }
    else if (event.source === 'extra') {
      this.SubmitScore(null);
    }
    this.chRef.detectChanges();  
  }

  SubmitScore(_score) {
    const vm = this;

    if (_score == null) {
      _score = 0;
    }
    _score = parseInt(_score);

    if (vm.currentEntry.title != 'BULL' && _score >= 0 && _score <= 9 && vm.IsInt(_score)) {
      vm.currentPlayer.scores[vm.currentEntry.val] = _score;
      vm.currentPlayer.hits += _score;
      vm.NextPlayer();
    } else if (vm.currentEntry.title == 'BULL' && _score >= 0 && _score <= 6 && vm.IsInt(_score)) {
      vm.currentPlayer.scores[vm.currentEntry.val] = _score;
      vm.currentPlayer.hits += _score;
      vm.NextPlayer();
    } else {

      let maxValue = 9;
      if (vm.currentEntry.title == 'BULL') {
        maxValue = 6;
      }

      //Show score error
      vm.mobileAPI.translate.get('enter between', { val1: 0, val2: maxValue }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });

      vm.score = '';
      return;
    }
  }

  UndoScore(): void {
    const vm = this;
    const currentPlayerIndex = vm.players.indexOf(vm.currentPlayer);

    //It's the first player
    if (currentPlayerIndex == 0) {
      if (vm.dartboardValues.indexOf(vm.currentEntry) > 0) {
        //Go to last player if it wasn't the first score of the game
        vm.currentPlayer = vm.players[(vm.players.length - 1)];
        vm.PreviousValue();
      }
    } else {
      vm.currentPlayer = vm.players[currentPlayerIndex - 1];
      vm.currentPlayer.hits -= vm.currentPlayer.scores[vm.currentEntry.val];
      vm.currentPlayer.scores[vm.currentEntry.val] = null;
    }
  }

  NextPlayer(playerIndex: number = null) {
    const vm = this;

    if (playerIndex != null) {
      vm.currentPlayer = vm.players[playerIndex];
    } else {
      if (vm.players.length > 1) {
        const newIndex = vm.players.indexOf(vm.currentPlayer) + 1;
        //If we are at the last item of the array
        if (!vm.players[newIndex]) {
          //Go to first player
          vm.currentPlayer = vm.players[0];
          //Go to the next value on the board
          vm.NextValue();
        } else {
          vm.currentPlayer = vm.players[newIndex];
        }
      } else {
        vm.NextValue();
      }
    }

    vm.score = '';
  }

  NextValue(entryIndex: number = null) {
    const vm = this;
    if (entryIndex != null) {
      vm.entryIndex = entryIndex;
    } else {
      vm.entryIndex++;
    }
    vm.currentEntry = vm.dartboardValues[vm.entryIndex];
    if (vm.currentEntry == undefined) {
      vm.TrySave();
    }
  }

  PreviousValue = function () {
    const vm = this;

    vm.entryIndex--;
    vm.currentEntry = vm.dartboardValues[vm.entryIndex];
    if (vm.entryIndex == 0) {
      vm.currentPlayer.scores = [];
      vm.currentPlayer.hits = 0;
    } else {
      vm.currentPlayer.hits -= vm.currentPlayer.scores[vm.currentEntry.val];
      vm.currentPlayer.scores[vm.currentEntry.val] = null;
    }
  };

  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  TrySave() {
    const vm = this;

    if (vm.auth.userData) {
      const gameData = {
        ID: vm.ID,
        singlePlayers: vm.players
      };

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({ 
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Game: gameData 
      }, 'complete', vm.storageKey).subscribe(result => {
        vm.ShowEnd();
        vm.mobileAPI.hideLoader();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.storageKey + 'unsaved').then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    /* set the custom stats */
    const totals = { title: 'total points', values: [] };
    
    for (const player of vm.players) {
      totals.values.push(player.hits);
    }

    const statisticsModal = this.modal.create(SimpleStatisticsDialog,
      {
        ignoreTotal: true,
        statistics: [
          totals
        ],
        players: vm.players,
      });
    statisticsModal.onDidDismiss(data => {
    });

    this.nav.pop();
    statisticsModal.present();
  }

  ShowHelp() {
    const vm = this;

    const tutorial = this.modal.create(TutorialDialog,
      {
        slides: [
          {
            title: vm.storageKey + ' slide1 title',
            description: vm.storageKey + ' slide1 content',
          }
        ]
      });
      tutorial.onDidDismiss(data => {
    });

    tutorial.present();
  }
}
