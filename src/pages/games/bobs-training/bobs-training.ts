import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import {
  Content, ModalController, PopoverController, ViewController, MenuController,
  IonicPage, NavController, NavParams, ToastController, LoadingController
} from 'ionic-angular';
import { Settings } from '../../../models/settings';
import { Platform } from 'ionic-angular/platform/platform';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { SimpleStatisticsDialog, TutorialDialog, SwipeForMenu } from '../../../dialogs';

import { IonNumericKeyboardOptions, IngameMenuComponent } from '../../../components';
import { MobileApiService, StorageProvider, Auth, PreferenceService } from '../../../providers';


const _DARTBOARD_VALUES = [
  { val: 1, title: 1 },
  { val: 2, title: 2 },
  { val: 3, title: 3 },
  { val: 4, title: 4 },
  { val: 5, title: 5 },
  { val: 6, title: 6 },
  { val: 7, title: 7 },
  { val: 8, title: 8 },
  { val: 9, title: 9 },
  { val: 10, title: 10 },
  { val: 11, title: 11 },
  { val: 12, title: 12 },
  { val: 13, title: 13 },
  { val: 14, title: 14 },
  { val: 15, title: 15 },
  { val: 16, title: 16 },
  { val: 17, title: 17 },
  { val: 18, title: 18 },
  { val: 19, title: 19 },
  { val: 20, title: 20 },
  { val: 25, title: 'BULL' }
];


@IonicPage()
@Component({
  selector: 'page-bobs-training',
  templateUrl: 'bobs-training.html',
})
export class BobsTraining {
  @ViewChild(Content) content: Content;
  settings: Settings;
  storageKey: string = 'bobs';
  ID: any = Date.now();

  title: string;
  keyboardOptions: IonNumericKeyboardOptions;
  isKeyboardVisible: boolean = true;
  score: any = '';
  players: any[] = [];
  currentPlayer: any;

  finishedPeople: number = 0;
  dartboardValues: any[];
  entryIndex: number = 0;
  currentEntry: any;
  currentOrientation: any;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public modal: ModalController,
    public menu: MenuController,
    public inGameMenu: IngameMenuComponent,
    public auth: Auth,
    public toast: ToastController,
    public platform: Platform,
    public chRef: ChangeDetectorRef,
    public screenOrientation: ScreenOrientation,
    public storage: StorageProvider,
    public preferenceService: PreferenceService,
    public viewController: ViewController,
    public mobileAPI: MobileApiService,
    public loading: LoadingController,
  ) {

    const vm = this;
    this.settings = navParams.get('settings');
    this.title = 'page bobs';

    this.menu.enable(false, 'mainmenu');
    this.menu.enable(true, 'ingamemenu');

    // this.inGameMenu.SetOptions({
    //   showKeySize: true,
    //   showSound: false
    // });

    for (const team of this.settings.teams) {
      //Concat the players to the playerlist
      for (const player of team.players) {
        player.scores = [];
        player.dartsThrown = 0;
        player.done = false;
        player.atIndex = 0;
        player.finishes = 0;
        player.turns = [];
        player.total = 27; //for bobs 27

        vm.players.push(player);
      }
    }

    this.dartboardValues = _DARTBOARD_VALUES;
    this.NextPlayer(0);
  }

  ionViewWillEnter() {
    const vm = this;

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });

    vm.CheckOrientation();
    this.screenOrientation.onChange().subscribe(
      () => {
        if (vm.screenOrientation.type.indexOf('portrait') >= 0) {
          vm.currentOrientation = 'portrait';
        }
        else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
          vm.currentOrientation = 'landscape';
        }

        setTimeout(() => {
          vm.CheckOrientation();
        }, 500);
      }
    );
  }

  CheckOrientation() {
    const vm = this;

    let totalHeight = this.platform.height();
    let totalWidth = this.platform.width();
    if (totalHeight < totalWidth) {
      const tempHeight = totalHeight;
      totalHeight = totalWidth;
      totalWidth = tempHeight;
    }

    if (vm.platform.is('core') || vm.platform.is('mobileweb') ||
      vm.screenOrientation.type.indexOf('portrait') >= 0) {
        vm.currentOrientation = 'portrait';
        if (document.querySelector('.playerssection')) {
        const playersHeight = document.querySelector('.playerssection').clientHeight;
        vm.preferenceService.Preferences.keySize = Math.floor((totalHeight - 115 - playersHeight) / 2);
      }
    }
    else if (vm.screenOrientation.type.indexOf('landscape') >= 0) {
      vm.currentOrientation = 'landscape';
      vm.preferenceService.Preferences.keySize = Math.floor((totalWidth - 115) / 2);
    }
    vm.chRef.detectChanges();
  }

  SwipeForMenu() {
    const popover = this.popoverCtrl.create(SwipeForMenu, {}, {
      cssClass: 'swipePopover'
    });
    popover.present();
    this.storage.PutItemInStorage('swipeForMenu', true);

    setTimeout(() => {
      this.OpenInGameMenu();
    }, 2000);
  }

  OpenInGameMenu() {
    this.menu.open('ingamemenu');
  }

  Shuffle(array: any[]): any[] {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }

  ngOnInit() {
    this.keyboardOptions = {
      contentComponent: this.content,
      onlyThreeOptions: true,
      showExtraButton: true,
      ExtraButtonText: 'miss',
      hideBottom: true,
      rightControlKey: {
        type: 'icon',
        value: 'backspace'
      }
    };
  }

  onClick(event) {
    if (event.source === 'RIGHT_CONTROL') {
      this.score = this.score.substr(0, this.score.length - 1);
    }
    else if (event.source === 'NUMERIC_KEY') {
      //Score can be only a single digit with DoublesTraining (0 - 9)
      this.score = event.key;
    }
    else if (event.source === 'extra') {
      this.SubmitScore(0);
    }
    this.chRef.detectChanges();
  }

  SubmitScore(_score: number = 0) {
    const vm = this;
    const currentDartboardValue = vm.dartboardValues[vm.currentPlayer.atIndex];

    if (!vm.currentPlayer.scores[currentDartboardValue.val]) {
      vm.currentPlayer.scores[currentDartboardValue.val] = { at: _score, finished: true, gaveup: false };
    }

    if (_score == 0) {
      vm.currentPlayer.total -= (currentDartboardValue.val * 2);
    } else {
      vm.currentPlayer.total += (currentDartboardValue.val * 2) * _score;
    }

    //Push the turn and go to the next index for the next turn
    vm.currentPlayer.turns.push({
      number: currentDartboardValue.val,
      finished: true,
      gaveUp: false,
      darts: _score
    });
    vm.currentPlayer.atIndex++;

    if ((vm.currentPlayer.total <= 0 && vm.settings.difficulty == 'hard') || !vm.dartboardValues[vm.currentPlayer.atIndex]) {
      if (vm.currentPlayer.total <= 0) {
        vm.currentPlayer.total = 0;
      }
      vm.finishedPeople++;
      vm.currentPlayer.done = true;

      if (vm.players.length == vm.finishedPeople) {
        vm.TrySave();
      } else {
        vm.NextPlayer();
      }
    }
    else {
      vm.NextPlayer();
    }
  }

  UndoScore(): void {
    const vm = this;
    const currentPlayerIndex = vm.players.indexOf(vm.currentPlayer);

    //First player & no turns played yet
    if (currentPlayerIndex == 0 && !vm.currentPlayer.turns.length) {
      return;
    }

    //Current player is the first player
    if (currentPlayerIndex == 0) {
      //Go to last player
      vm.currentPlayer = vm.players[vm.players.length - 1];
    } else {
      //CurrentPlayerIndex - 1
      vm.currentPlayer = vm.players[currentPlayerIndex - 1];
    }

    const last_turn = vm.currentPlayer.turns.pop();
    if (last_turn.darts == 0) {
      vm.currentPlayer.total += (last_turn.number * 2);
    }
    else {
      vm.currentPlayer.total -= ((last_turn.number * 2) * last_turn.darts);
    }

    if (vm.currentPlayer.done && vm.currentPlayer.total > 0) {
      vm.currentPlayer.done = false;
      vm.finishedPeople--;
    }

    vm.currentPlayer.scores.pop();
    vm.currentPlayer.atIndex--;
    vm.currentPlayer.scores[last_turn.number] = null;
  }

  NextPlayer(playerIndex: number = null) {
    const vm = this;

    if (playerIndex != null) {
      vm.currentPlayer = vm.players[playerIndex];
    } else {
      if (vm.players.length > 1) {
        const newIndex = vm.players.indexOf(vm.currentPlayer) + 1;
        //If we are at the last item of the array
        if (!vm.players[newIndex]) {
          //Go to first player
          vm.currentPlayer = vm.players[0];
        } else {
          vm.currentPlayer = vm.players[newIndex];
        }

        if (vm.currentPlayer.done) {
          vm.NextPlayer();
        }
      }
    }

    vm.score = '';
  }

  IsInt(n: any): boolean {
    return Math.round(parseInt(n)) == n;
  }

  TrySave() {
    const vm = this;

    if (vm.auth.userData) {
      const gameData = {
        ID: vm.ID,
        singlePlayers: vm.players
      };

      vm.mobileAPI.showLoader();
      vm.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
        Game: gameData
      }, 'complete', vm.storageKey).subscribe(result => {
        vm.ShowEnd();
        vm.mobileAPI.hideLoader();
      }, (err) => {
        //Add to unsaved-collection
        vm.mobileAPI.hideLoader();
        vm.storage.GetItemFromStorage(vm.storageKey + 'unsaved').then(
          (items) => {
            //There's a local item, we add this one
            items.push(gameData);
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', items);
          },
          () => {
            //No local items yet, create an array with this one
            vm.storage.PutItemInStorage(vm.storageKey + 'unsaved', [gameData]);
          });
        vm.ShowEnd();
      });
    }
    else {
      vm.ShowEnd();
    }

  }

  ShowEnd() {
    const vm = this;

    const statisticsModal = this.modal.create(SimpleStatisticsDialog,
      {
        statistics: [],
        players: vm.players,
      });
    statisticsModal.onDidDismiss(data => {
    });

    this.nav.pop();
    statisticsModal.present();
  }

  ShowHelp() {
    const vm = this;

    const tutorial = this.modal.create(TutorialDialog,
      {
        slides: [
          {
            title: vm.storageKey + ' slide1 title',
            description: vm.storageKey + ' slide1 content',
          },
          {
            title: vm.storageKey + ' slide2 title',
            description: vm.storageKey + ' slide2 content',
            image: 'assets/img/ica-slidebox-img-2.png',
          },
          {
            title: vm.storageKey + ' slide3 title',
            description: vm.storageKey + ' slide3 content',
            image: 'assets/img/ica-slidebox-img-3.png',
          }
        ]
      });
    tutorial.onDidDismiss(data => {
    });

    tutorial.present();
  }
}
