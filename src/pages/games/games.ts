import { Component } from '@angular/core';
import { App, Platform, IonicPage, NavController, MenuController, ToastController, ModalController } from 'ionic-angular';
import { AppRate } from '@ionic-native/app-rate';

import { Auth } from '../../providers';
import { GameInterface } from '../../interfaces/GameInterface';
import { SettingsView } from '../';
import { MobileApiService, StorageProvider, PreferenceService } from '../../providers';

import { ActionCodeDialog, LoginRequiredDialog } from '../../dialogs';
import { MatchPage, SinglesTrainingPage, DoublesTrainingPage, BobsTraining, ScoreTrainingPage, TacticsPage } from '../';
import { ViewController } from 'ionic-angular/navigation/view-controller';
 

/**
 * Generated class for the Games page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-games',
  templateUrl: 'games.html',
})
export class Games {
  private games: GameInterface[];
  private products: any[];
  private showCode: boolean;

  constructor(public app: App, private nav: NavController,
    private auth: Auth,
    public storage: StorageProvider,
    public menu: MenuController,
    public viewController: ViewController,
    public platform: Platform,
    public appRate: AppRate,
    private modal: ModalController,
    private preferenceService: PreferenceService,
    public mobileAPI: MobileApiService, public toast: ToastController,
     ) {

    this.app.setTitle('Home');
    const vm = this;

    if (vm.platform.is('core') || vm.platform.is('mobileweb')) {
    }
    else {
      if (vm.preferenceService.Preferences.lang == 'nl') {
        vm.appRate.preferences.customLocale = {
          title: 'Beoordeel %@',
          message: 'Zou je onze app willen beoordelen? Bedankt voor je steun!',
          cancelButtonLabel: 'Nee, bedankt',
          laterButtonLabel: 'Herinner me later',
          rateButtonLabel: 'Ja!',
          yesButtonLabel: 'Ja',
          noButtonLabel: 'Nee',
          appRatePromptTitle: 'Ben je tevreden met %@?',
          feedbackPromptTitle: 'Zou je ons feedback willen geven?',
        };
      }
      else if (vm.preferenceService.Preferences.lang == 'de') {
        vm.appRate.preferences.customLocale = {
          title: 'Bewerte %@',
          message: 'Würdest Du uns bitte bewerten? Danke für die Unterstützung!',
          cancelButtonLabel: 'Nein, danke',
          laterButtonLabel: 'Später erinnern',
          rateButtonLabel: 'Ja!',
          yesButtonLabel: 'Ja',
          noButtonLabel: 'Nein',
          appRatePromptTitle: 'Bist du zufrieden mit %@?',
          feedbackPromptTitle: 'Geben Sie uns eine Rückmeldung',
        };
      }
      else if (vm.preferenceService.Preferences.lang == 'en') {
        vm.appRate.preferences.customLocale = {
          title: 'Rate %@?',
          message: 'Would you please rate our app? Thank you for your support!',
          cancelButtonLabel: 'No, Thanks',
          laterButtonLabel: 'Remind Me Later',
          rateButtonLabel: 'Yes!',
          yesButtonLabel: 'Yes',
          noButtonLabel: 'No',
          appRatePromptTitle: 'Do you like using %@?',
          feedbackPromptTitle: 'Would you like to give us some feedback?',
        };
      }
      vm.appRate.promptForRating(false);
    }

    vm.mobileAPI.GetData({}, 'code', 'show').subscribe(res => {
      vm.showCode = res.showCode;
    });

    // Configure all games for the overview
    this.games = [
      {
        title: 'page match',
        component: SettingsView,
        gameplay: MatchPage,
        settings: {
          key: 'matchSettings',
          allowDartbot: true,
          dartbotMin: 20,
          dartbotMax: 120,
          dartbotDefault: 50,
          cpuTitle: 'dartbot average',
          allowCheckoutRate: true,
          checkoutRate: true,
          allowTeams: true,
          showStartScores: true,
          selectedStartScore: 501,
          customScore: 170,
          setsMode: true,
          goalAmount: 5,
          isBestOf: 'true',
          isSets: 'true',
          unsavedKey: 'matchunsaved',
          unfinishedKey: 'matchunfinished',
        },
        players: [],
        icon: 'arrow-forward'
      },
      {
        title: 'page tactics',
        component: SettingsView,
        gameplay: TacticsPage,
        settings: {
          key: 'tacticsSettings',
          allowDartbot: true,
          dartbotMin: 1,
          dartbotMax: 10,
          dartbotDefault: 5,
          cpuTitle: 'dartbot level',
          allowTeams: true,
          showGameModes: true,
          gameMode: 'Cricket',
          unsavedKey: 'tacticsunsaved',
          unfinishedKey: 'tacticsunfinished',
        },
        players: [],
        icon: 'arrow-forward'
      },
      {
        title: 'page singles',
        component: SettingsView,
        gameplay: SinglesTrainingPage,
        settings: {
          key: 'singlesSettings',
          showSequences: true,
          unsavedKey: 'singlesunsaved',
          unfinishedKey: 'singlesunfinished',
          sequence: 'asc',
        },
        players: [],
        icon: 'arrow-forward'
      },
      {
        title: 'page doubles',
        component: SettingsView,
        gameplay: DoublesTrainingPage,
        settings: {
          key: 'doublesSettings',
          showSequences: true,
          sequence: 'asc',
          unsavedKey: 'doublesunsaved',
          unfinishedKey: 'doublesunfinished',
        },
        players: [],
        icon: 'arrow-forward'
      },
      {
        title: 'page bobs',
        component: SettingsView,
        gameplay: BobsTraining,
        settings: {
          key: 'bobsSettings',
          unsavedKey: 'bobsunsaved',
          unfinishedKey: 'bobsunfinished',
          difficulties: true,
          difficulty: 'easy',
        },
        players: [],
        icon: 'arrow-forward'
      },
      {
        title: 'page scores',
        component: SettingsView,
        gameplay: ScoreTrainingPage,
        settings: {
          key: 'scoresSettings',
          showAmountOfTurns: true,
          unsavedKey: 'scoresunsaved',
          unfinishedKey: 'scoresunfinished',
          amountOfTurns: 10
        },
        players: [],
        icon: 'arrow-forward'
      }
    ];
  }

  EnterCode(showInfo: boolean) {
    const vm = this;

    if (vm.auth.guest) {
      const loginRequiredDialog = vm.modal.create(LoginRequiredDialog);
      loginRequiredDialog.present();
    }
    else {
      const actionCodeDialog = this.modal.create(ActionCodeDialog, { info: showInfo });
      actionCodeDialog.present();
    }
  }

  SetUnfinishedAmount(game: GameInterface) {
    const vm = this;

    vm.storage.GetItemFromStorage(game.settings.unfinishedKey).then(
      (items) => {
      }, () => { });
  }

  ionViewWillEnter() {
    const vm = this;

    vm.games.forEach(function (game) {
      vm.SetUnfinishedAmount(game);
    });

    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  // DartboardPOC(){
  //   let vm = this;
  //   //send throwout with amount of darts
  //   let dartboardModal = this.modal.create(DartboardDialog);
  //   dartboardModal.onDidDismiss(data => {
  //     console.log(data)
  //   });

  //   dartboardModal.present();
  // }

  openSettings(game: GameInterface) {
    this.nav.push(game.component, game);
  }
}
