import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';


 

/**
 * Generated class for the Register page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {
  role: string;
  email: string;
  password: string;
  loader: any;

  constructor( ) {
  }

  ionViewDidLoad() {
    const vm = this;
  }

}
