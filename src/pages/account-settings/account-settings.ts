import { Component } from '@angular/core';
import { ModalController, LoadingController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { FacebookLoginResponse } from '@ionic-native/facebook';

import { Auth, MobileApiService, SocialMediaService } from '../../providers';
import { ForgotPassword } from '../';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@IonicPage()
@Component({
  selector: 'page-account-settings',
  templateUrl: 'account-settings.html',
})

export class AccountSettingsView {
  public title: string;
  public account: any;
  public Username: any;
  public Password: any;
  public link: any;

  constructor(public nav: NavController,
    public auth: Auth,
    public navCtrl: NavController,
    public loading: LoadingController,
    public social: SocialMediaService,
    public mobileAPI: MobileApiService,
    public toast: ToastController,
    public modal: ModalController,
    public navParams: NavParams) {
    const vm = this;

    vm.title = 'account settings';
    vm.link = 'new';

    if (vm.navParams.get('account')) {
      vm.account = vm.navParams.get('account');
    }

    //Check if authenticated
    vm.GetProfile();
  }

  GetProfile() {
    const vm = this;

    const loader = this.loading.create({
      showBackdrop: false
    });

    loader.present();
    return new Promise((resolve, reject) => {
      this.mobileAPI.PostData({
        AccountID: this.auth.userData.AccountID,
        UniqueKey: this.auth.userData.UniqueKey,
      }, 'backend', 'AccountData').subscribe(profile => {
        vm.account = profile;
        loader.dismiss();
        resolve(profile);
      }, (err) => {
        loader.dismiss();
        reject(err);
      });
    });
  }

  LinkFacebook() {
    const vm = this;
    vm.social.FacebookLogin().then((res: FacebookLoginResponse) => {
      vm.social.GetAccount(res.authResponse.userID)
        .then((fbAccount) => {
          vm.LinkToCurrentAccount(fbAccount);
        }).catch(e => { });
    }).catch(e => {
      console.log('Error logging into Facebook', e);
    });
  }

  LinkToCurrentAccount(fbAccount) {
    const vm = this;

    vm.mobileAPI.PostData({
      account: {
        ID: vm.auth.userData.ID,
        FacebookID: fbAccount.id,
        Birthday: null,
        Locale: fbAccount.locale,
        Email: fbAccount.email,
        Gender: fbAccount.gender,
      }
    }, 'backend', 'linkToExisting').subscribe(profile => {
      vm.auth.saveAuthentication(profile);
      vm.account = profile;

      //Account successfully linked to existing user: profile.Username
      vm.mobileAPI.translate.get('account linked to', { value: profile.Username }).subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
      
    }, (err) => {
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  LinkExistingToFacebook() {
    const vm = this;

    const loader = this.loading.create();
    loader.present();
    vm.mobileAPI.PostData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
      account: {
        Username: vm.Username,
        Password: vm.Password,
      },
    }, 'backend', 'linkFacebook').subscribe(profile => {
      loader.dismiss();
      vm.auth.saveAuthentication(profile);
      vm.account = profile;

      //Account successfully linked to existing user: profile.Username
      vm.toast.create({
        cssClass: 'success',
        message: 'Account linked to ' + profile.Username,
        duration: 3000
      }).present();
    }, (err) => {
      loader.dismiss();
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  AddLoginToAccount() {
    const vm = this;

    const userRegex = new RegExp('^[a-zA-Z0-9\-]+$');
    if (vm.Username.length < 5 || vm.Username.length > 20 || !userRegex.test(vm.Username)) {
      vm.mobileAPI.translate.get('username_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else if (vm.Password.length < 6) {
      vm.mobileAPI.translate.get('pass_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else {
      const loader = this.loading.create();
      loader.present();
      vm.mobileAPI.PostData({
        AccountID: vm.auth.userData.AccountID,
        UniqueKey: vm.auth.userData.UniqueKey,
        account: {
          Username: vm.Username,
          Password: vm.Password,
        },
      }, 'backend', 'addLogin').subscribe(profile => {
        loader.dismiss();
        vm.auth.saveAuthentication(profile);
        vm.account = profile;
      }, (err) => {
        loader.dismiss();
        this.toast.create({
          cssClass: 'error',
          message: err,
          duration: 3000
        }).present();
      });
    }
  }

  ForgotPassword() {
    this.navCtrl.push(ForgotPassword);
  }
}
