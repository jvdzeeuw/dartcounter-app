// Include all pages here
export * from './home/home';
export * from './dashboard/dashboard';
export * from './login/login';
export * from './friends/friends';
export * from './register/register';
export * from './account/account';
export * from './account-settings/account-settings';
export * from './forgot-password/forgot-password';
export * from './profile/profile';
export * from './app-settings/app-settings';
export * from './remote-control/remote-control';

export * from './statistics/statistics';
export * from './statistics/achievements/achievements';
export * from './statistics/itemlist/itemlist';
export * from './statistics/itemDetail/itemDetail';
export * from './statistics/itemDetail/Details/singlesDetail';
export * from './statistics/itemDetail/Details/doublesDetail';
export * from './statistics/itemDetail/Details/bobsDetail';
export * from './statistics/itemDetail/Details/scoresDetail';
export * from './statistics/matchlist/matchlist';
export * from './statistics/matchDetail/matchDetail';
export * from './statistics/tacticslist/tacticslist';
export * from './statistics/tacticsDetail/tacticsDetail';

export * from './games/games';
export * from './games/settings/settings';
export * from './games/match/match';
export * from './games/tactics/tactics';
export * from './games/doubles-training/doubles-training';
export * from './games/bobs-training/bobs-training';
export * from './games/singles-training/singles-training';
export * from './games/score-training/score-training';
