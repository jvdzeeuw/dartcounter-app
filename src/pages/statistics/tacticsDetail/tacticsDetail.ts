import { Component } from '@angular/core';
import moment from 'moment';
import { IonicPage, LoadingController, ViewController, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../providers';
import { _DetailPage } from '../../_DetailPage';
 

@IonicPage()
@Component({
  selector: 'page-tacticsDetail',
  templateUrl: 'tacticsDetail.html',
})

export class TacticsDetail {
  public title: string;
  public item: any = null;
  public teams: any;
  gameValues: any[] = [];

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,
    public navParams: NavParams,
     ) {
    const vm = this;

    const data = navParams.data;
    this.item = data.item;

    this.GetItemDetail();
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  GetItemDetail() {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.GetData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
    }, 'expanded', 'tactics', vm.item.ID).subscribe(result => {
      vm.mobileAPI.hideLoader();
      result.CreatedDate = moment(result.CreatedDate).toISOString();
      vm.item = result;

      vm.GetTeams(vm.item.ID);

    }, (err) => {
      vm.mobileAPI.hideLoader();
      //Error go back
      this.nav.pop();
    });
  }

  GetTeams(itemID) {
    const vm = this;

    vm.mobileAPI.GetData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
    }, 'expanded', 'teams', vm.item.ID).subscribe(result => {
      vm.teams = result;

      vm.teams.forEach(team => {
        if (team.hasWon == '1') {
          //winner didn't hit 10 so it's Tactics
          if (!team.turns[10]) {
            vm.gameValues = [
              { val: 20, title: 20 },
              { val: 19, title: 19 },
              { val: 18, title: 18 },
              { val: 17, title: 17 },
              { val: 16, title: 16 },
              { val: 15, title: 15 },
              { val: 25, title: 25 },
            ];
          }
          else {
            vm.gameValues = [
              { val: 20, title: 20 },
              { val: 19, title: 19 },
              { val: 18, title: 18 },
              { val: 17, title: 17 },
              { val: 16, title: 16 },
              { val: 15, title: 15 },
              { val: 14, title: 14 },
              { val: 13, title: 13 },
              { val: 12, title: 12 },
              { val: 11, title: 11 },
              { val: 10, title: 10 },
              { val: 25, title: 25 },
            ];
          }
        }
      });
    }, (err) => {
      //Error
    });
  }

  GetPlayerByID(playerID) {
    const vm = this;

    let playername = '';
    playerID = parseInt(playerID);
    vm.teams.forEach((team: any) => {
      team.players.forEach((player: any) => {
        if (player.ID == playerID) {
          playername = player.FirstName;
        }
      });
    });

    return playername;
  }
}
