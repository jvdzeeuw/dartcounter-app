import { Component } from '@angular/core';
import { IonicPage, LoadingController, ViewController, ModalController, NavController, NavParams } from 'ionic-angular';

import { UpgradeSubscriptionDialog } from '../../../../dialogs';
import { Auth, MobileApiService } from '../../../../providers';
import { MatchDetail } from '../../matchDetail/matchDetail';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-x01average',
  templateUrl: 'x01average.html',
})

export class X01Average {
  public items: {
    avg: Array<any>,
    best: Array<any>,
    worst: Array<any>
  } = {
      avg: [],
      best: [],
      worst: []
    };
  public segment;

  constructor(public nav: NavController,
    public auth: Auth,
    public modal: ModalController,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,
    public navParams: NavParams) {

    const vm = this;

    vm.segment = vm.navParams.get('segment');
    if (!vm.segment) {
      vm.segment = 'best';
    }

    const loader = this.loading.create({
      showBackdrop: false
    });
    loader.present();

    vm.GetItems().then(() => {
      loader.dismiss();
    }, () => {
      loader.dismiss();
    });
  }

  swipeEvent(e) {
    if (e.direction == 4 && this.segment != 'best') {
      //direction 4 = left to right swipe.
      this.segment = 'best';
    }
    else if (e.direction == 2 && this.segment != 'worst') {
      //direction 2 = right to left swipe.
      this.segment = 'worst';
    }
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  GetDate(date) {
    moment(date).toISOString();
  }

  OpenSubscriptionDialog() {
    const subscriptionDialog = this.modal.create(UpgradeSubscriptionDialog);
    subscriptionDialog.present();
  }

  GetItems(): Promise<any> {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    return new Promise((resolve) => {

      vm.mobileAPI.PostDataR(data, 'calculate', 'averages', 1, 'avg').subscribe(result => {
        vm.items.avg = result[0].Value;
      });

      vm.mobileAPI.PostDataR(data, 'calculate', 'averages', 10, 'desc').subscribe(result => {
        vm.items.best = result;
      });

      vm.mobileAPI.PostDataR(data, 'calculate', 'averages', 10, 'asc').subscribe(result => {
        vm.items.worst = result;
        resolve();
      });
    });
  }

  ShowDetail(item) {
    this.nav.push(MatchDetail, {
      data: {
        item: {
          ID: item.GameID
        }
      }
    });
  }

}
