import { Component } from '@angular/core';
import moment from 'moment';
import { IonicPage, LoadingController, ViewController, ModalController, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../providers';
import { UpgradeSubscriptionDialog } from '../../../dialogs';
import { MatchDetail } from '../../';


@IonicPage()
@Component({
  selector: 'page-matchlist',
  templateUrl: 'matchlist.html',
})

export class MatchList {
  public title: string;
  public items: Array<any> = [];
  public typeIDs: Array<number>;
  public startDate: Date;
  public endDate: Date;
  public skip: number = 0;
  public take: number = 5;
  public total: number = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public modal: ModalController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,   
    public navParams: NavParams) {

    const vm = this;

    vm.title = vm.navParams.get('title');
    vm.typeIDs = vm.navParams.get('typeIDs');
    vm.startDate = null;
    vm.endDate = moment().toDate();

    const loader = this.loading.create({
      showBackdrop: false
    });
    loader.present();

    vm.GetItems().then(() => {
      loader.dismiss();
    }, () => {
      loader.dismiss();
    });
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  OnDateChanged(){
    const vm = this;

    vm.items = [];
    vm.skip = 0;
    vm.take = 5;
    vm.total = null;
    
    const loader = this.loading.create({
      showBackdrop: false
    });
    loader.present();

    vm.GetItems().then(() => {
      loader.dismiss();
    }, () => {
      loader.dismiss();
    });
  }

  OpenSubscriptionDialog(){
    const subscriptionDialog = this.modal.create(UpgradeSubscriptionDialog);
    subscriptionDialog.present();
  }

  GetItems(event = null): Promise<any> {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      TypeIDs: vm.typeIDs,
      From: vm.startDate,
      To: vm.endDate,
      Skip: vm.skip,
      Take: vm.take,
    };

    return new Promise((resolve) => {

      if (vm.skip <= vm.total || vm.total == null) {
        vm.mobileAPI.PostData(data, 'statistics', 'overview').subscribe(result => {
          vm.skip += vm.take;
          vm.total = result.total;
          
          result.items.forEach((item) => {
            item.CreatedDate = moment(item.CreatedDate).toISOString();
            vm.items.push(item);
          });

          if (event) { event.complete(); }
          resolve();
        }, (err) => {
          //Add to unsaved-collection
          if (event) { event.complete(); }
          resolve();
        });
      }
      else {
        if (event) { event.complete(); }
        resolve();
      }
    });
  }

  GetPlayers(team): string {
    const playerNames = team.players.map(function (e) {
      return e.FirstName;
    });

    return playerNames.join(', ');
  }

  ShowDetail(item) {
    if (item.Game){
      item.Game.CreatedDate = moment(item.Game.CreatedDate).toISOString();
    }

    this.nav.push(MatchDetail, {
      item: item
    });
  }

}
