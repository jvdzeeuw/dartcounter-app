import { Component } from '@angular/core';
import moment from 'moment';
import { IonicPage, LoadingController, ViewController, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../providers';
import { SinglesDetail, DoublesDetail, BobsDetail, ScoresDetail } from '../../';
 

@IonicPage()
@Component({
  selector: 'page-itemDetail',
  templateUrl: 'itemDetail.html',
})

export class ItemDetail {
  public title: string;
  public item: any = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,   
    public navParams: NavParams,
     ) {
      
    const vm = this;

    const data = navParams.data;
    this.item = data.item;

    this.GetItemDetail(data.routeParam);
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  GetItemDetail(routeParam) {
    const vm = this;

    vm.mobileAPI.showCircleLoader();
    vm.mobileAPI.GetData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
    }, 'expanded', routeParam, vm.item.ID).subscribe(result => {
      vm.mobileAPI.hideLoader();      
      result.CreatedDate = moment(result.CreatedDate).toISOString(); 

      if (result.Game){
        result.Game.CreatedDate = moment(result.Game.CreatedDate).toISOString();
      }

      //Go to the correct detailPage
      switch (routeParam) {
        case 'singles': this.nav.push(SinglesDetail, result);
          break;
        case 'doubles': this.nav.push(DoublesDetail, result);
          break;
        case 'bobs': this.nav.push(BobsDetail, result);
          break;
        case 'scores': this.nav.push(ScoresDetail, result);
          break;
      }

    }, (err) => {
      vm.mobileAPI.hideLoader();            
      //Error go back
      this.nav.pop();
    });
  }

}
