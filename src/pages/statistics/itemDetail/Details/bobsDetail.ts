import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../../providers';
 

@IonicPage()
@Component({
  selector: 'page-bobsDetail',
  templateUrl: 'bobsDetail.html',
})

export class BobsDetail {
  public title: string;
  public item: any = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public navParams: NavParams,
     ) {
    const vm = this;

    const data = navParams.data;
    this.item = data;

  }

  ionViewDidLoad(){
    const vm = this;

    const active = this.nav.getActive(true);
    this.nav.remove((active.index - 1) , 1);
  }

}
