import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../../providers';
import { _DetailPage } from '../../../_DetailPage';


@IonicPage()
@Component({
  selector: 'page-scoresDetail',
  templateUrl: 'scoresDetail.html',
})

export class ScoresDetail {
  public title: string;
  public item: any = null;

  constructor(public nav: NavController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public navParams: NavParams,
  ) {
    const data = navParams.data;
    this.item = data;
  }

  ionViewDidLoad() {
    const active = this.nav.getActive(true);
    this.nav.remove((active.index - 1), 1);
  }

  getAverage(player) {
    const total = parseInt(player.TotalScore);
    const darts = parseInt(player.DartsThrown);

    return (total / darts) * 3;
  }

}
