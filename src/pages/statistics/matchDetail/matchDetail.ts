import { Component } from '@angular/core';
import moment from 'moment';
import { IonicPage, LoadingController, ViewController, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../providers';
import { _DetailPage } from '../../_DetailPage';
 

@IonicPage()
@Component({
  selector: 'page-matchDetail',
  templateUrl: 'matchDetail.html',
})

export class MatchDetail {
  public title: string;
  public item: any = null;
  public teams: any;

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,   
    public navParams: NavParams,
     ) {
    const data = navParams.get('data');
    this.item = data.item;

    this.GetItemDetail();
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  GetItemDetail() {
    const vm = this;

    vm.mobileAPI.showCircleLoader();          
    vm.mobileAPI.GetData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
    }, 'game', 'details', vm.item.ID).subscribe(result => {
      vm.mobileAPI.hideLoader();      
      result.CreatedDate = moment(result.CreatedDate).toISOString();      
      vm.item = result;

      vm.GetTeams(vm.item.ID);

    }, (err) => {
      vm.mobileAPI.hideLoader();      
      //Error go back
      this.nav.pop();
    });
  }

  GetTeams(itemID) {
    const vm = this;
    
    vm.mobileAPI.GetData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
    }, 'game', 'teams', vm.item.ID).subscribe(result => {
      vm.teams = result;

    }, (err) => {
      //Error
    });
  }

  GetPlayers(team): string {
    const playerNames = team.players.map(function (e) {
      return e.FirstName;
    });

    return playerNames.join(', ');
  }
}
