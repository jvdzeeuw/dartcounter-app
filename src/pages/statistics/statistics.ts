import { Component } from '@angular/core';
import { ModalController, IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../providers';
import { ItemList, MatchList, TacticsList } from '../../pages';
import { X01Average } from './alltime/x01average/x01average';
import { CheckoutRate } from './alltime/checkoutRate/checkoutRate';
import { Checkouts } from './alltime/checkouts/checkouts';
import { FirstNines } from './alltime/firstNine/firstNine';
import { UltimateSubscriptionDialog } from '../../dialogs';


@IonicPage()
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})

export class Statistics {
  public title: string;
  public stats: {
    matchAvg: { avg: null, best: null, worst: null },
    firstNine: { avg: null, best: null, worst: null },
    checkout: { avg: null, best: null, worst: null },
    checkoutRate: { avg: null, best: null, worst: null },
  } = <any>{};

  constructor(public nav: NavController, public auth: Auth,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,
    public modal: ModalController, public navParams: NavParams,
  ) {
    const vm = this;

    vm.stats.matchAvg = <any>{};
    vm.stats.firstNine = <any>{};
    vm.stats.checkout = <any>{};
    vm.stats.checkoutRate = <any>{};
    this.title = 'statistics';

    vm.initAverages();
    vm.initCheckoutRates();
    vm.initCheckouts();
    vm.initFirstNines();
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  initAverages() {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    this.mobileAPI.PostDataR(data, 'calculate', 'averages', 1, 'avg').subscribe(response => {
      vm.stats.matchAvg.avg = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'averages', 1, 'desc').subscribe(response => {
      vm.stats.matchAvg.best = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'averages', 1, 'asc').subscribe(response => {
      vm.stats.matchAvg.worst = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
  }

  initCheckouts() {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    this.mobileAPI.PostDataR(data, 'calculate', 'checkouts', 1, 'avg').subscribe(response => {
      vm.stats.checkout.avg = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'checkouts', 1, 'desc').subscribe(response => {
      vm.stats.checkout.best = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'checkouts', 1, 'asc').subscribe(response => {
      vm.stats.checkout.worst = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
  }

  initCheckoutRates() {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    this.mobileAPI.PostDataR(data, 'calculate', 'checkoutRates', 1, 'avg').subscribe(response => {
      vm.stats.checkoutRate.avg = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'checkoutRates', 1, 'desc').subscribe(response => {
      vm.stats.checkoutRate.best = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'checkoutRates', 1, 'asc').subscribe(response => {
      vm.stats.checkoutRate.worst = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
  }

  initFirstNines() {
    const vm = this;

    const data = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    this.mobileAPI.PostDataR(data, 'calculate', 'firstNines', 1, 'avg').subscribe(response => {
      vm.stats.firstNine.avg = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'firstNines', 1, 'desc').subscribe(response => {
      vm.stats.firstNine.best = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
    this.mobileAPI.PostDataR(data, 'calculate', 'firstNines', 1, 'asc').subscribe(response => {
      vm.stats.firstNine.worst = response.length ? response[0].Value : 0;
    }, (err) => {
      console.error(err);
    });
  }

  ShowMatches() {
    this.nav.push(MatchList, {
      title: 'page match',
      typeIDs: [1]
    });
  }

  ShowTactics() {
    this.nav.push(TacticsList, {
      title: 'page tactics',
      typeIDs: [6]
    });
  }

  ShowSingles() {
    this.nav.push(ItemList, {
      title: 'page singles',
      routeParam: 'singles',
      typeIDs: [2]
    });
  }

  ShowDoubles() {
    this.nav.push(ItemList, {
      title: 'page doubles',
      routeParam: 'doubles',
      typeIDs: [3]
    });
  }

  ShowBobs() {
    this.nav.push(ItemList, {
      title: 'page bobs',
      routeParam: 'bobs',
      typeIDs: [4]
    });
  }

  ShowScores() {
    this.nav.push(ItemList, {
      title: 'page scores',
      routeParam: 'scores',
      typeIDs: [5]
    });
  }

  OpenUltimateSubscriptionDialog() {
    const subscriptionDialog = this.modal.create(UltimateSubscriptionDialog);
    subscriptionDialog.present();
  }

  alltime(key, segment) {
    switch (key) {
      case 'avg':
        this.nav.push(X01Average, {
          segment: segment
        });
        break;
      case 'checkout':
        this.nav.push(Checkouts, {
          segment: segment
        });
        break;
      case 'checkoutrate':
        this.nav.push(CheckoutRate, {
          segment: segment
        });
        break;
        case 'firstnine':
          this.nav.push(FirstNines, {
            segment: segment
          });
          break;
    }
  }
}
