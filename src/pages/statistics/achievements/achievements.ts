import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, LoadingController, NavParams } from 'ionic-angular';

import { Auth, MobileApiService } from '../../../providers';
 
/**
 * Generated class for the Settings page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-achievements',
  templateUrl: 'achievements.html',
})

export class Achievements {
  public title: string;
  public thrownScores: Array<any> = [];

  constructor(public nav: NavController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public viewController: ViewController,    
    public navParams: NavParams,
     ) {
    const vm = this;
    vm.title = 'my achievements';

    vm.GetStatistics();
  }

  ionViewWillEnter() {
    this.mobileAPI.translate.get('backbutton').subscribe((res: string) => {
      this.viewController.setBackButtonText(res);
    });
  }

  GetStatistics() {
    const vm = this;

    vm.mobileAPI.GetDataR({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
    }, 'statistics', 'scoreCount').subscribe(result => {
      vm.thrownScores = result;
    }, (err) => {
    });
  }


}
