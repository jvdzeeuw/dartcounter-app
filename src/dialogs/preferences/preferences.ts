import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { PreferenceService, MobileApiService, Auth } from '../../providers';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@IonicPage()
@Component({
  selector: 'dialog-preferences',
  templateUrl: 'preferences.html',
})
export class PreferencesDialog {
  public lang: string = 'en';

  constructor(
    public view: ViewController,
    public mobileAPI: MobileApiService,
    public auth: Auth,
    public preferenceService: PreferenceService,
  ) { }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  LanguageChanged(){
    this.preferenceService.Preferences.lang = this.lang;
    this.preferenceService.LanguageChanged();
  }
}
