import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, NavController } from 'ionic-angular';
import { Register } from '../../pages';

@IonicPage()
@Component({
  selector: 'dialog-login-required',
  templateUrl: 'login-required.html',
})
export class LoginRequiredDialog {
  constructor(public view: ViewController, public navParams: NavParams,
    public navCtrl: NavController) {
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

  GoToRegistration() {
    this.navCtrl.push(Register);
    this.view.dismiss();
  }

}
