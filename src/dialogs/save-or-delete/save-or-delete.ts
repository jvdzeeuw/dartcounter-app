import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@IonicPage()
@Component({
  selector: 'dialog-save-or-delete',
  templateUrl: 'save-or-delete.html',
})
export class SaveOrDeleteDialog {

  constructor(
    public view: ViewController,
  ) { }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

}
