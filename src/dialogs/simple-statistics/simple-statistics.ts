import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'dialog-simple-statistics',
  templateUrl: 'simple-statistics.html',
})
export class SimpleStatisticsDialog {
  teams: Array<any> = new Array();
  players: Array<any> = new Array();
  statistics: Array<any> = new Array();
  ignoreTotal: boolean = false;
  
  constructor(public view: ViewController, public navParams: NavParams) {
    //navParams.get('param') to get a given value
    this.teams = navParams.get('teams');
    this.players = navParams.get('players');
    this.statistics = navParams.get('statistics');   
    this.ignoreTotal = navParams.get('ignoreTotal');   
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

}
