import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController, ModalController } from 'ionic-angular';
import { Auth, MobileApiService, PurchaseService, PreferenceService } from '../../providers';
import { PolicyDialog, AgreementDialog } from '../../dialogs';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';

@IonicPage()
@Component({
  selector: 'dialog-pro-subscription',
  templateUrl: 'pro-subscription.html',
})
export class UpgradeSubscriptionDialog {

  constructor(
    public view: ViewController,
    public mobileAPI: MobileApiService,
    public auth: Auth,
    public modal: ModalController,
    public toast: ToastController,
    private store: InAppPurchase2,
    private themeableBrowser: ThemeableBrowser,
    private preferences: PreferenceService,
    public purchases: PurchaseService,
  ) {
    const vm = this;


  }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  public BuyUltimate() {
    const vm = this;

    const options: ThemeableBrowserOptions = {
      statusbar: {
        color: '#ffffffff',
      },
      toolbar: {
        height: 44,
        color: '#ff6600'
      },
      title: {
        color: '#3a3a3a',
        showPageTitle: true
      },
      backButton: {
        image: 'back',
        imagePressed: 'back',
        align: 'left',
        event: 'backPressed'
      },
      backButtonCanClose: true
    };

    let url = 'http://dartcounter.net/v2/#/' + vm.preferences.Preferences.lang + '/login';
    url += '/' + this.auth.userData.AccountID;
    url += '/' + this.auth.userData.UniqueKey;
    url += '/upgrade';

    const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', options);
  }

  public BuyPro(){
    const vm = this;
    
    /*
    vm.purchases.SetPro("lalala").subscribe(profile => {
      vm.auth.saveAuthentication(profile);
      vm.DismissModal();
    }, (err) => {
      vm.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
    */

    this.store.when('annual_pro').approved(function (p) {
      //globalService.toast('success', "Verify the subscription!", 3000);
      p.verify();
      vm.purchases.SetPro(p).subscribe(profile => {
        vm.auth.saveAuthentication(profile);
        
        vm.DismissModal();
      }, (err) => {
        vm.toast.create({
          cssClass: 'error',
          message: err,
          duration: 3000
        }).present();
      });
    });
    
    vm.store.order(vm.purchases.proSubscription);
  }

  OpenAgreementDialog() {
    const modal = this.modal.create(AgreementDialog);
    modal.present();
  }

  OpenPolicyDialog() {
    const modal = this.modal.create(PolicyDialog);
    modal.present();
  }
}
