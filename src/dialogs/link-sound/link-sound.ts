
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { ModalController, ViewController, NavParams } from 'ionic-angular';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/catch';

import { Auth, MobileApiService, SmartAudioProvider } from '../../providers';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { UltimateSubscriptionDialog } from '../ultimate-subscription/ultimate-subscription';
import { LoginRequiredDialog } from '../login-required/login-required';

export interface DialogData {
  player: any;
}

@Component({
  selector: 'dialog-link-sound',
  templateUrl: 'link-sound.html'
})
export class LinkSoundDialog {
  player: any;
  Sounds: any[] = [];
  searchTerm$ = new Subject<string>();
  selectedSound: any;

  constructor(
    public view: ViewController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public smartAudio: SmartAudioProvider,
    public modal: ModalController,
    public navParams: NavParams,
    public toast: ToastController
  ) {
    //navParams.get('param') to get a given value
    const vm = this;
    this.player = navParams.get('player');

    this.search(this.searchTerm$)
      .subscribe(
        sounds => {
          if (sounds.length) {
            vm.Sounds = sounds;
          }
        },
        (error) => {
          vm.toast.create({
            cssClass: 'error',
            message: error,
            duration: 3000
          }).present();
      });
  }

  public dismiss(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  search(terms) {
    return terms.debounceTime(400)
      .switchMap(term => this.SearchSounds(term))
      .catch((e) => {
        // handle e and return a safe value or rethrow
        return Observable.throw(e);
      });
  }

  SearchSounds(searchString: string): Observable<any[]> {
    const vm = this;

    if (searchString.length >= 2) {
      const apiCall = vm.mobileAPI.GetDataR({}, 'sounds', 'search', searchString);
      return apiCall;
    }
    else{
      const apiCall = vm.mobileAPI.GetDataR({}, 'sounds', 'search', 'notextgiven');
      return apiCall;
    }
  }

  PlaySound(sound) {
    this.smartAudio.playHTML('https://legacyapi.dartcounter.net/sounds/names/' + sound.File);
  }

  LinkSound(sound): void {
    if (sound
      && this.player.isUltimate !== '1'
      && (this.auth.userData && this.auth.userData.isAdmin != '1')
      && !(this.player.Account == this.auth.userData.ID && this.auth.userData.isUltimate == '1')) {
      const subscriptionDialog = this.modal.create(UltimateSubscriptionDialog);
      subscriptionDialog.present();
      this.dismiss();
    }
    else if (this.auth.guest) {
      const loginRequiredDialog = this.modal.create(LoginRequiredDialog);
      loginRequiredDialog.present();
      this.dismiss();
    }
    else {
      this.dismiss(sound);
    }
  }
}
