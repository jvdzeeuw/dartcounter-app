import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ThrowoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'dialog-throwout',
  templateUrl: 'throwout.html',
})
export class ThrowoutDialog {
  private possibleDarts: number[];
  private selectedDart: number;
  private isInvalid: boolean;
  
  private possibleDartsAtDouble: number[];
  private selectedDartAtDouble: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController) {
    this.possibleDarts = navParams.get('possibleDarts');
    this.possibleDartsAtDouble = navParams.get('possibleDartsAtDouble');

    this.selectedDart = this.possibleDarts[0];
    this.selectedDartAtDouble = this.possibleDartsAtDouble[0];
  }

  /**
   * Returns the amount of thrown darts to the masterView
   * 
   * @param {number} _dartsThrown 
   * @memberof ThrowoutDialog
   */
  public ReturnDarts(): void {
    const vm = this;
    this.view.dismiss({
      selectedDartAtDouble: vm.selectedDartAtDouble,
      selectedDart: vm.selectedDart
    });
  }

  public CheckDoubles() {
    const vm = this;

    if (vm.selectedDartAtDouble != null && vm.selectedDart != null) {
      //it's one or two darts at the double (with finish)
      if ((vm.possibleDartsAtDouble.length == 2 && vm.selectedDart <= vm.selectedDartAtDouble) || 
        (vm.possibleDartsAtDouble.length > 2 && vm.selectedDart < vm.selectedDartAtDouble)) {
          vm.isInvalid = true;        
      }
      else{
        vm.isInvalid = false;        
      }

    }
    else {
      vm.isInvalid = false;
    }
  }

}
