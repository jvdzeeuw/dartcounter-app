import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'dialog-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialDialog {
  slides: any;

  constructor(public view: ViewController, public navParams: NavParams) {
    //navParams.get('param') to get a given value
    this.slides = navParams.get('slides');   
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

}
