import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, ModalController } from 'ionic-angular';
import { LoginOrCreateDialog } from '../login-or-create/login-or-create';

@IonicPage()
@Component({
  selector: 'dialog-link-account-warning',
  templateUrl: 'link-account-warning.html',
})
export class LinkAccountWarningDialog {
  constructor(
    public view: ViewController, 
    public navParams: NavParams,
    public modal: ModalController) {
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

  ShowLogin(){
    const loginDialog = this.modal.create(LoginOrCreateDialog);
    loginDialog.present();
  }
}
