import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { PreferenceService } from '../../providers';

@IonicPage()
@Component({
  selector: 'dialog-agreement',
  templateUrl: 'agreement.html',
})
export class AgreementDialog {
  scorings: any;
  teams: any;

  constructor(
    public view: ViewController, 
    public preferenceService: PreferenceService, 
    public navParams: NavParams) {
    
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

}
