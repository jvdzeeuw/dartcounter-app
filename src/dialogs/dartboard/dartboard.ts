import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'dialog-dartboard',
  templateUrl: 'dartboard.html',
})
export class DartboardDialog {
  public values: Array<any> = new Array();
  public totalScore: number = 0;

  constructor(public view: ViewController, public navParams: NavParams) {
    //navParams.get('param') to get a given value
  }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  AddValue(name: string, value: number = null) {
    const vm = this;
    if (vm.values.length < 3) {

      if (value == null) {
        const firstChar = name.slice(0, 1);
        value = parseInt(name.slice(1));

        if (firstChar == 'd') { value *= 2; }
        if (firstChar == 't') { value *= 3; }

      }

      vm.values.push({
        name: name, 
        value: value
      });
      vm.totalScore += value;
    }
  }


  DeleteItemFromArray(item, array: any[]) {
    if (array.indexOf(item) >= 0) {
      this.totalScore -= item.value;
      array.splice(array.indexOf(item), 1);
    }
  }
}
