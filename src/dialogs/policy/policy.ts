import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'dialog-policy',
  templateUrl: 'policy.html',
})
export class PolicyDialog {
  scorings: any;
  teams: any;

  constructor(public view: ViewController, public navParams: NavParams) {
    
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

}
