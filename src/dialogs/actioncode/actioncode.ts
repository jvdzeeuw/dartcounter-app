import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController, ModalController } from 'ionic-angular';
import { Auth, MobileApiService } from '../../providers';
import { NavParams } from 'ionic-angular/navigation/nav-params';


@IonicPage()
@Component({
  selector: 'dialog-actioncode',
  templateUrl: 'actioncode.html',
})
export class ActionCodeDialog {
  public code: string;  
  public info: boolean = false;

  constructor(
    public view: ViewController,  
    public mobileAPI: MobileApiService, 
    public navParams: NavParams,
    public auth: Auth, 
    public modal: ModalController, 
    public toast: ToastController, 
  ) {
    this.info = navParams.get('info');
    const vm = this;
  }

  public DismissModal(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

  SubmitCode(){
    const vm = this;

    if (vm.code.length != 8){
      vm.toast.create({
        cssClass: 'error',
        message: 'De wincode bestaat uit 8 tekens!',
        duration: 3000
      }).present();
      return false;
    }

    vm.mobileAPI.showLoader();
    
    vm.mobileAPI.PostData({
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey,
      code: vm.code
    }, 'code', 'insert').subscribe(result => {
      vm.mobileAPI.hideLoader();
      this.toast.create({
        cssClass: 'success',
        message: result.data,
        duration: 3000
      }).present();

      vm.DismissModal();
    }, (err) => {
      //Add to unsaved-collection
      vm.mobileAPI.hideLoader();
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

}
