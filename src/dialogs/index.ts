//Include all Dialogs for the exports
export * from './link-people/link-people';
export * from './link-sound/link-sound';
export * from './policy/policy';
export * from './agreement/agreement';
export * from './throwout/throwout';
export * from './tutorial/tutorial';
export * from './dartboard/dartboard';
export * from './match-statistics/match-statistics';
export * from './complete-account/complete-account';
export * from './simple-statistics/simple-statistics';
export * from './preferences/preferences';
export * from './save-or-delete/save-or-delete';
export * from './login-required/login-required';
export * from './actioncode/actioncode';

export * from './pro-subscription/pro-subscription';
export * from './ultimate-subscription/ultimate-subscription';
export * from './acceptTerms/acceptTerms';

export * from './swipe-for-menu/swipe-for-menu';
