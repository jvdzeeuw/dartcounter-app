import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'swipe-for-menu',
  templateUrl: 'swipe-for-menu.html',
})
export class SwipeForMenu {
  constructor(public view: ViewController, public navParams: NavParams) {
    
  }

  public Dismiss(returnValue: any = null): void{
    this.view.dismiss(returnValue);
  }

}
