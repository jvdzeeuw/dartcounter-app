import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, ToastController } from 'ionic-angular';
import { Auth, MobileApiService } from '../../providers';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@IonicPage()
@Component({
  selector: 'dialog-link-people',
  templateUrl: 'link-people.html',
})
export class LinkPeopleDialog {
  Username: any;
  Password: any;
  link: string = 'friend';
  Friends: any[] = [];
  searchTerm$ = new Subject<string>();

  constructor(
    public view: ViewController,
    public auth: Auth,
    public mobileAPI: MobileApiService,
    public navParams: NavParams,
    public toast: ToastController
  ) {
    //navParams.get('param') to get a given value
    const vm = this;

    this.search(this.searchTerm$)
      .subscribe(
      friends => vm.Friends = friends,
      error => vm.toast.create({
        cssClass: 'error',
        message: error,
        duration: 3000
      }).present()
      );
  }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  search(terms) {
    return terms.debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.SearchFriends(term))
      .catch((e) => {
        // handle e and return a safe value or rethrow
        return Observable.throw(e);
      });
  }

  SearchFriends(searchString: string): Observable<any[]> {
    const vm = this;

    const authBody = {
      AccountID: this.auth.userData.AccountID,
      UniqueKey: this.auth.userData.UniqueKey
    };

    if (searchString.length >= 2) {
      return vm.mobileAPI.GetDataR(authBody, 'friends', 'search', searchString);
    }
    else {
      return Observable.of<any>([]);
    }
  }

  TryLogin(): void {
    const vm = this;
    const credentials = {
      Username: this.Username,
      Password: this.Password
    };

    this.mobileAPI.PostData({account: credentials}, 'backend', 'login').subscribe(result => {
      //this.loader.dismiss().catch(() => console.log('ERROR CATCH: LoadingController dismiss'));
      //this.loader = undefined;
      vm.LinkFriend(result);
    }, (err) => {
      vm.mobileAPI.hideLoader();
      vm.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  LinkOwnAccount(): void {
    this.DismissModal({
      DisplayName: this.auth.userData.FirstName,
      ProfilePic: this.auth.userData.ProfilePic,
      AccountID: this.auth.userData.AccountID,
      isUltimate: this.auth.userData.isUltimate,
    });
  }

  LinkFriend(friend): void {
    this.DismissModal({
      DisplayName: friend.FirstName,
      ProfilePic: friend.ProfilePic,
      isUltimate: friend.isUltimate,
      AccountID: friend.ID,
    });
  }
}
