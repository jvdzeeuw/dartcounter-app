import { Component } from '@angular/core';
import { IonicPage, ViewController, LoadingController, NavParams, ToastController } from 'ionic-angular';
import { Auth, MobileApiService } from '../../providers';
import { FacebookLoginResponse } from '@ionic-native/facebook';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@IonicPage()
@Component({
  selector: 'dialog-complete-account',
  templateUrl: 'complete-account.html',
})
export class CompleteAccountDialog {
  account: FacebookLoginResponse;
  Username: any;
  Password: any;

  constructor(
    public view: ViewController,
    public auth: Auth,
    public loading: LoadingController,
    public mobileAPI: MobileApiService,
    public navParams: NavParams,
    public toast: ToastController
  ) {
    //navParams.get('param') to get a given value
    const vm = this;
  }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  LinkExistingToFacebook() {
    const vm = this;

    const loader = this.loading.create();
    loader.present();
    vm.mobileAPI.PostData({
      AccountID: vm.auth.userData.AccountID,
      UniqueKey: vm.auth.userData.UniqueKey,
      account: {
        Username: vm.Username,
        Password: vm.Password,
      },
    }, 'backend', 'linkFacebook').subscribe(profile => {
      loader.dismiss();
      vm.auth.saveAuthentication(profile);

      //Account successfully linked to existing user: profile.Username
      vm.toast.create({
        cssClass: 'success',
        message: 'Account linked to ' + profile.Username,
        duration: 3000
      }).present();

      vm.DismissModal();
    }, (err) => {
      loader.dismiss();
      this.toast.create({
        cssClass: 'error',
        message: err,
        duration: 3000
      }).present();
    });
  }

  AddLoginToAccount() {
    const vm = this;

    const userRegex = new RegExp('^[a-zA-Z0-9\-]+$');
    if (vm.Username.length < 5 || vm.Username.length > 20 || !userRegex.test(vm.Username)) {
      vm.mobileAPI.translate.get('username_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else if (vm.Password.length < 6) {
      vm.mobileAPI.translate.get('pass_error').subscribe((res: string) => {
        vm.toast.create({
          cssClass: 'error',
          message: res,
          duration: 3000
        }).present();
      });
    }
    else {
      const loader = this.loading.create();
      loader.present();
      vm.mobileAPI.PostData({
        AccountID: vm.auth.userData.AccountID,
        UniqueKey: vm.auth.userData.UniqueKey,
        account: {
          Username: vm.Username,
          Password: vm.Password,
        },
      }, 'backend', 'addLogin').subscribe(profile => {
        loader.dismiss();
        vm.auth.saveAuthentication(profile);
        vm.DismissModal();
      }, (err) => {
        loader.dismiss();
        this.toast.create({
          cssClass: 'error',
          message: err,
          duration: 3000
        }).present();
      });
    }
  }
}
