import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PreferenceService } from '../../providers';

/**
 * Generated class for the ThrowoutPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'dialog-acceptTerms',
  templateUrl: 'acceptTerms.html',
})
export class AcceptTermsDialog {
  possibleDarts: number[];

  constructor(public navCtrl: NavController, 
    public preferenceService: PreferenceService, 
    public navParams: NavParams, 
    public view: ViewController) {
     this.possibleDarts = navParams.get('possibleDarts');
  }

  public AgreeWithTerms(): void{
    this.view.dismiss(true);
  }

}
