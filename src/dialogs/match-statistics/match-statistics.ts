import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, ModalController } from 'ionic-angular';
import { UltimateSubscriptionDialog } from '../ultimate-subscription/ultimate-subscription';
import { Auth } from '../../providers';
import { LoginOrCreateDialog } from '../login-or-create/login-or-create';

@IonicPage()
@Component({
  selector: 'dialog-match-statistics',
  templateUrl: 'match-statistics.html',
})
export class MatchStatisticsDialog {
  scorings: any;
  title: any;
  teams: any;
  checkoutRate: any;

  constructor(public view: ViewController,
    public navParams: NavParams,
    public auth: Auth,
    public modal: ModalController) {
    //navParams.get('param') to get a given value
    this.scorings = navParams.get('scorings');
    this.title = navParams.get('title');
    this.teams = navParams.get('teams');
    this.checkoutRate = navParams.get('checkoutRate');
  }

  public DismissModal(returnValue: any = null): void {
    this.view.dismiss(returnValue);
  }

  OpenUltimateSubscriptionDialog() {
    const subscriptionDialog = this.modal.create(UltimateSubscriptionDialog);
    subscriptionDialog.present();
  }

  ShowLogin() {
    const loginDialog = this.modal.create(LoginOrCreateDialog);
    loginDialog.present();
  }
}
